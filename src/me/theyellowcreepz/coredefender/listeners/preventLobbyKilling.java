package me.theyellowcreepz.coredefender.listeners;

import me.theyellowcreepz.coredefender.GameState;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;

/**
 * All code inside this plugin is the
 * property of Andrew Petersen.
 * Decompiling, copying, or claiming any
 * code from this plugin as your own
 * voids any legal rights you have to
 * this plugin, unless express permission
 * is given.
 */
public class preventLobbyKilling implements Listener {

    @EventHandler
    public void onDamage(EntityDamageEvent evt){
        if(!GameState.getState().equals(GameState.INGAME)){
            if(evt.getEntity() instanceof Player){
                evt.setCancelled(true);
            }
        }
    }
}
