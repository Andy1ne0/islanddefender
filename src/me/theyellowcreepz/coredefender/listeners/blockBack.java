package me.theyellowcreepz.coredefender.listeners;

import me.theyellowcreepz.coredefender.messageStandards;
import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;

/**
 * All code inside this plugin is the
 * property of Andrew Petersen.
 * Decompiling, copying, or claiming any
 * code from this plugin as your own
 * voids any legal rights you have to
 * this plugin, unless express permission
 * is given.
 */
public class blockBack implements Listener {

    @EventHandler
    public void onCommand(PlayerCommandPreprocessEvent evt){
        if(evt.getMessage().equals("/back")){
            evt.setCancelled(true);
            evt.getPlayer().sendMessage(messageStandards.alertPrefix()+ChatColor.RESET+ ChatColor.RED+"You can't use that command!");
        }
    }

}
