package me.theyellowcreepz.coredefender.listeners;

import me.theyellowcreepz.coredefender.Main;
import me.theyellowcreepz.coredefender.inventories.kitManager;
import me.theyellowcreepz.coredefender.messageStandards;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerQuitEvent;

/**
 * All code inside this plugin is the
 * property of Andrew Petersen.
 * Decompiling, copying, or claiming any
 * code from this plugin as your own
 * voids any legal rights you have to
 * this plugin, unless express permission
 * is given.
 */
public class playerQuitOrKick implements Listener {

    public void removeFromArrays(Player pl){

        Main.redTeam.remove(pl);
        Main.playerKits.remove(pl);
        Main.playerRespawnTimes.remove(pl);
        Main.blueTeam.remove(pl);
        Main.playersOnline.remove(pl);
        Main.spectatorTeam.remove(pl);
        kitManager.playerKits.remove(pl);
        Main.mapVotes.remove(pl);

    }


    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent evt){

        evt.getPlayer().getActivePotionEffects().clear();
        evt.setQuitMessage("");
        removeFromArrays(evt.getPlayer());

        for(Player pl : Bukkit.getOnlinePlayers()){
            pl.sendMessage(messageStandards.pluginPrefixMessage()+ ChatColor.AQUA +evt.getPlayer().getName()+ChatColor.RED+" has left the game!");
        }
    }


    @EventHandler
    public void onPlayerKick(PlayerKickEvent evt){

        evt.getPlayer().getActivePotionEffects().clear();
        evt.setLeaveMessage("");
        removeFromArrays(evt.getPlayer());

        for(Player pl : Bukkit.getOnlinePlayers()){
            pl.sendMessage(messageStandards.pluginPrefixMessage()+ ChatColor.AQUA +evt.getPlayer().getName()+ChatColor.RED+" was kicked from the game!");
        }
    }

}
