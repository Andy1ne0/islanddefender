package me.theyellowcreepz.coredefender.listeners;

import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityExplodeEvent;

/**
 * All code inside this plugin is the
 * property of Andrew Petersen.
 * Decompiling, copying, or claiming any
 * code from this plugin as your own
 * voids any legal rights you have to
 * this plugin, unless express permission
 * is given.
 */
public class onExplodeCancelDamage implements Listener {

    @EventHandler
    public void onExplode(EntityExplodeEvent evt){
        evt.blockList().clear();
        // Bukkit.getServer().getLogger().info(">>> An explosion was created at ("+evt.getLocation()+"). All block damage was cancelled.");
    }
}
