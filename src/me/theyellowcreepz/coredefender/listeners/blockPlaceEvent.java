package me.theyellowcreepz.coredefender.listeners;

import me.theyellowcreepz.coredefender.GameState;
import me.theyellowcreepz.coredefender.Main;
import me.theyellowcreepz.coredefender.messageStandards;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPlaceEvent;

/**
 * All code inside this plugin is the
 * property of Andrew Petersen.
 * Decompiling, copying, or claiming any
 * code from this plugin as your own
 * voids any legal rights you have to
 * this plugin, unless express permission
 * is given.
 */
public class blockPlaceEvent implements Listener {

    @EventHandler
    public void onBlockPlace(BlockPlaceEvent evt) {
        if (GameState.getState() == GameState.INGAME) {
            if (!(evt.getBlockPlaced().getType() == Material.LEAVES || evt.getBlockPlaced().getType() == Material.LEAVES_2)) {
                evt.setCancelled(true);
                Player pl = evt.getPlayer();
                pl.sendMessage(messageStandards.alertPrefix() + ChatColor.RESET + ChatColor.RED + "You can only place leaves!");
            } else if (evt.getBlockPlaced().getType() == Material.LEAVES || evt.getBlockPlaced().getType() == Material.LEAVES_2) {
                Main.blocksPlaced.add(evt.getBlock());
            } else {
                evt.getPlayer().sendMessage(messageStandards.alertPrefix() + "Critical Error: ");
                evt.getPlayer().sendMessage(messageStandards.alertPrefix() + "Ask the server admin to check the server logs.");
                throw new IllegalArgumentException(
                        "Block was placed, but was not captured by either if statement in blockPlaceEvent class. Contact Yellow with this error, plus the following info: " + evt.getBlockPlaced().getType());
            }
        } else {
            evt.setCancelled(true);
            evt.getPlayer().sendMessage(messageStandards.alertPrefix() + ChatColor.RESET + ChatColor.GRAY + ChatColor.BOLD + "You can't place blocks now!");
        }
    }
}