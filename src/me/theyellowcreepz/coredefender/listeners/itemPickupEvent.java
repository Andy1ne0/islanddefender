package me.theyellowcreepz.coredefender.listeners;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerPickupItemEvent;

/**
 * All code inside this plugin is the
 * property of Andrew Petersen.
 * Decompiling, copying, or claiming any
 * code from this plugin as your own
 * voids any legal rights you have to
 * this plugin, unless express permission
 * is given.
 */
public class itemPickupEvent implements Listener {

    @EventHandler
    public void onEntityPickup(PlayerPickupItemEvent evt){
            evt.setCancelled(true);
    }
}