package me.theyellowcreepz.coredefender.listeners;

import me.theyellowcreepz.coredefender.GameState;
import me.theyellowcreepz.coredefender.inventories.kitManager;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

/**
 * All code inside this plugin is the
 * property of Andrew Petersen.
 * Decompiling, copying, or claiming any
 * code from this plugin as your own
 * voids any legal rights you have to
 * this plugin, unless express permission
 * is given.
 */
public class warmupKitSelectionRightClick implements Listener {

    @EventHandler
    public void onRightClick(PlayerInteractEvent evt){
        if(GameState.getState().equals(GameState.WARMUP)){
            Player pl = evt.getPlayer();
            if(pl.getItemInHand().getType().equals(Material.PAPER)){
                if(evt.getAction().equals(Action.RIGHT_CLICK_AIR) || evt.getAction().equals(Action.RIGHT_CLICK_BLOCK)) {
                    pl.openInventory(kitManager.kitSelMenu);
                }
            }
        }
    }

}
