package me.theyellowcreepz.coredefender.listeners;

import me.theyellowcreepz.coredefender.GameState;
import me.theyellowcreepz.coredefender.Main;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

/**
 * All code inside this plugin is the
 * property of Andrew Petersen.
 * Decompiling, copying, or claiming any
 * code from this plugin as your own
 * voids any legal rights you have to
 * this plugin, unless express permission
 * is given.
 */
public class playerChatEvent implements Listener {


    public String getTeam(Player pl) {
        if (Main.redTeam.contains(pl)) {
            return "red";
        } else if (Main.blueTeam.contains(pl)) {
            return "blue";
        } else {
            return "spec";
        }
    }


    @EventHandler
    public void onChat(AsyncPlayerChatEvent evt) {
        String msg = evt.getMessage();
        Player pl = evt.getPlayer();
        String newMsg = null;
        evt.setCancelled(true);
        char firstChar = evt.getMessage().charAt(0);

        if (GameState.getState().equals(GameState.INGAME) || GameState.getState().equals(GameState.WARMUP) || GameState.getState().equals(GameState.ENDGAME)) {

            if (Main.redTeam.contains(pl)) {
                newMsg = (ChatColor.BOLD + "" + ChatColor.DARK_RED + "[" + ChatColor.RED + "RED" + ChatColor.DARK_RED + "] " + ChatColor.RESET + ChatColor.GREEN + pl.getName() + ChatColor.DARK_GRAY + ": " + ChatColor.WHITE + msg);

            } else if (Main.blueTeam.contains(pl)) {
                newMsg = (ChatColor.BOLD + "" + ChatColor.DARK_BLUE + "[" + ChatColor.BLUE + "BLUE" + ChatColor.DARK_BLUE + "] " + ChatColor.RESET + ChatColor.GREEN + pl.getName() + ChatColor.DARK_GRAY + ": " + ChatColor.WHITE + msg);

            } else if (Main.spectatorTeam.contains(pl)) {
                newMsg = (ChatColor.BOLD + "" + ChatColor.DARK_GRAY + "[" + ChatColor.GRAY + "SPECTATOR" + ChatColor.DARK_GRAY + "] " + ChatColor.RESET + ChatColor.GREEN + pl.getName() + ChatColor.DARK_GRAY + ": " + ChatColor.WHITE + msg);
                
            }
            String plStr = newMsg;
            for (Player pltomsg : Bukkit.getOnlinePlayers()) {
                if (Character.toString(firstChar).equals("!")) {
                    String editedShout = newMsg.replaceFirst("!", "");
                    String newNewString = (ChatColor.DARK_GRAY + "[" + ChatColor.GOLD + "SHOUT" + ChatColor.DARK_GRAY + "]" + editedShout);
                    pltomsg.sendMessage(newNewString);
                } else if (getTeam(pltomsg).equals(getTeam(pl)) || getTeam(pltomsg).equals("spec")) {
                    pltomsg.sendMessage(newMsg);
                }
            }
            ConsoleCommandSender console = Bukkit.getServer().getConsoleSender();
            if (Character.toString(firstChar).equals("!")) {
                String editedShout = newMsg.replaceFirst("!", "");
                String newNewString = (ChatColor.DARK_GRAY + "[" + ChatColor.GOLD + "SHOUT" + ChatColor.DARK_GRAY + "]" + editedShout);
                console.sendMessage(newNewString);
            } else {
                console.sendMessage(newMsg);
            }
        } else {
            newMsg = (ChatColor.GREEN + pl.getName() + ChatColor.DARK_GRAY + ": " + ChatColor.WHITE + msg);
            for (Player pltomsg : Bukkit.getOnlinePlayers()) {
                pltomsg.sendMessage(newMsg);
            }
            ConsoleCommandSender console = Bukkit.getServer().getConsoleSender();
            console.sendMessage(newMsg);
        }

    }
}