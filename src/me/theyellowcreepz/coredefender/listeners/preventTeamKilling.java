package me.theyellowcreepz.coredefender.listeners;

import me.theyellowcreepz.coredefender.GameState;
import me.theyellowcreepz.coredefender.Main;
import me.theyellowcreepz.coredefender.messageStandards;
import org.bukkit.ChatColor;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Player;
import org.bukkit.entity.Snowball;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

/**
 * All code inside this plugin is the
 * property of Andrew Petersen.
 * Decompiling, copying, or claiming any
 * code from this plugin as your own
 * voids any legal rights you have to
 * this plugin, unless express permission
 * is given.
 */
public class preventTeamKilling implements Listener {

    @EventHandler
    public void onPlayerDamage(EntityDamageByEntityEvent evt) {

        String damagedPlayerTeam = null;
        String DamagerTeam = null;

        if (GameState.getState() == GameState.INGAME) {
            if (evt.getEntity() instanceof Player) {
                Player pl = (Player) evt.getEntity();

                // Player Damage (Direct).
                if (evt.getDamager() instanceof Player) {
                    Player damager = (Player) evt.getDamager();
                    if(Main.redTeam.contains(pl)){
                        damagedPlayerTeam = "red";
                    } else if(Main.blueTeam.contains(pl)){
                        damagedPlayerTeam = "blue";
                    }
                    if(Main.redTeam.contains(damager)){
                        DamagerTeam = "red";
                    } else if(Main.blueTeam.contains(damager)){
                        DamagerTeam = "blue";
                    }
                    if(damagedPlayerTeam.equals(DamagerTeam)){
                        evt.setCancelled(true);
                        damager.sendMessage(messageStandards.alertPrefix()+ ChatColor.RESET+ChatColor.RED+ChatColor.ITALIC+"You can't damage your own team members!");
                    }
                }

                // Arrow Damage.
                else if(evt.getDamager() instanceof Arrow){
                    Arrow arrowShot = (Arrow) evt.getDamager();
                    if(arrowShot.getShooter() instanceof Player){
                        Player plShooter = (Player) arrowShot.getShooter();
                        if(Main.redTeam.contains(pl)){
                            damagedPlayerTeam = "red";
                        } else if(Main.blueTeam.contains(pl)){
                            damagedPlayerTeam = "blue";
                        }
                        if(Main.redTeam.contains(plShooter)){
                            DamagerTeam = "red";
                        } else if(Main.blueTeam.contains(plShooter)){
                            DamagerTeam = "blue";
                        }
                        if(damagedPlayerTeam.equals(DamagerTeam)){
                            evt.setCancelled(true);
                            plShooter.sendMessage(messageStandards.alertPrefix()+ ChatColor.RESET+ChatColor.RED+ChatColor.ITALIC+"You can't damage your own team members!");

                        }
                    }
                } else if(evt.getDamager() instanceof Snowball){
                    Snowball sb = (Snowball) evt.getDamager();
                    if(sb.getShooter() instanceof Player){
                        Player sbShooter = (Player) sb.getShooter();
                        if(Main.redTeam.contains(pl)){
                            damagedPlayerTeam = "red";
                        } else if(Main.blueTeam.contains(pl)){
                            damagedPlayerTeam = "blue";
                        }
                        if(Main.redTeam.contains(sbShooter)){
                            DamagerTeam = "red";
                        } else if(Main.blueTeam.contains(sbShooter)){
                            DamagerTeam = "blue";
                        }
                        if(damagedPlayerTeam.equals(DamagerTeam)){
                            evt.setCancelled(true);
                            sbShooter.sendMessage(messageStandards.alertPrefix()+ ChatColor.RESET+ChatColor.RED+ChatColor.ITALIC+"You can't damage your own team members!");

                        }

                    }
                }
            }
        }
    }
}
