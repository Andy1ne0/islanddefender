package me.theyellowcreepz.coredefender.listeners;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.events.PacketContainer;
import com.comphenix.protocol.wrappers.EnumWrappers;
import me.theyellowcreepz.coredefender.GameState;
import me.theyellowcreepz.coredefender.Main;
import me.theyellowcreepz.coredefender.messageStandards;
import me.theyellowcreepz.coredefender.util.packetwrappers.clientCommandWrapper;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.scheduler.BukkitRunnable;

import java.lang.reflect.InvocationTargetException;

/**
 * All code inside this plugin is the
 * property of Andrew Petersen.
 * Decompiling, copying, or claiming any
 * code from this plugin as your own
 * voids any legal rights you have to
 * this plugin, unless express permission
 * is given.
 */
public class playerDeathEvt implements Listener {

    @EventHandler
    public void onDeath(final PlayerDeathEvent evt) {
        String deathMSG = evt.getDeathMessage();
        deathMSG = deathMSG.replace("(Right-click)", "");
        evt.setDeathMessage("");
        if (evt.getEntity() instanceof Player) {
            if (!(Main.playerRespawnTimes.containsKey(evt.getEntity())) && GameState.getState() == GameState.INGAME) {
                Player pl = evt.getEntity();
                new BukkitRunnable()
                {
                    public void run()
                    {
                        PacketContainer respawnPacket = new PacketContainer(PacketType.Play.Client.CLIENT_COMMAND);
                        clientCommandWrapper respawnWrap = new clientCommandWrapper(respawnPacket);
                        respawnWrap.setCommand(EnumWrappers.ClientCommand.PERFORM_RESPAWN);
                        try {
                            Main.protocolManager.recieveClientPacket(evt.getEntity(), respawnPacket);
                        } catch (IllegalAccessException e) {
                            e.printStackTrace();
                        } catch (InvocationTargetException e) {
                            e.printStackTrace();
                        }
                    }
                }.runTaskLater(Main.getInstance() , 2);
                pl.teleport(Main.genSpawnExact);
                if(!Main.spectatorTeam.contains(pl)) {
                    if (Main.redTeam.contains(pl)) {
                        pl.teleport(Main.redSpawnExact);
                    } else if (Main.blueTeam.contains(pl)) {
                        pl.teleport(Main.blueSpawnExact);
                    }
                    pl.sendMessage(messageStandards.alertPrefix() + ChatColor.RESET + "" + ChatColor.RED + "YOU DIED!");
                    pl.sendMessage(messageStandards.grayArrows() + ChatColor.ITALIC + "You will respawn shortly. ");
                    evt.getDrops().clear();
                    evt.setKeepInventory(false);
                    Main.playerRespawnTimes.put(pl, 10);
                    pl.setMaxHealth(20);
                    for(PotionEffect effect : pl.getActivePotionEffects()) {     pl.removePotionEffect(effect.getType()); }
                    pl.setGameMode(GameMode.SPECTATOR);
                    String newDeathMSG = (ChatColor.BOLD+""+ChatColor.RED+">>> "+ChatColor.RESET+ChatColor.GRAY+deathMSG);
                    evt.setDeathMessage(newDeathMSG);
                } else if(Main.spectatorTeam.contains(pl)){
                    pl.sendMessage(messageStandards.alertPrefix() + ChatColor.RESET + "" + ChatColor.RED + "You died! However, you're a spectator, so no action has been taken. ");
                } else {
                    pl.kickPlayer(messageStandards.alertPrefix() + ChatColor.RED + "Internal error, please contact an admin. \nAsk them to check the server logs.");
                    Bukkit.getServer().getLogger().severe("!!! Player " + pl.getName() + " was not in any team at all. Kicked to maintain stability.");
                }
            }
        }
    }
}