package me.theyellowcreepz.coredefender.listeners;

import me.theyellowcreepz.coredefender.GameState;
import me.theyellowcreepz.coredefender.Main;
import me.theyellowcreepz.coredefender.messageStandards;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.server.ServerListPingEvent;

/**
 * All code inside this plugin is the
 * property of Andrew Petersen.
 * Decompiling, copying, or claiming any
 * code from this plugin as your own
 * voids any legal rights you have to
 * this plugin, unless express permission
 * is given.
 */
public class serverPing implements Listener {

    @EventHandler
    public void onServerPing(ServerListPingEvent evt){

        String MOTD = (ChatColor.GRAY+"["+ChatColor.DARK_AQUA+"Core Defender"+ChatColor.GRAY+"] "+ChatColor.DARK_GREEN+"\n"
                +ChatColor.UNDERLINE+ChatColor.RED+"Current Mode: "+ChatColor.RESET+" "+ messageStandards.getGameStateColor()+GameState.getState());
        evt.setMotd(MOTD);
        evt.setMaxPlayers(Main.maximumPlayers);
    }

}
