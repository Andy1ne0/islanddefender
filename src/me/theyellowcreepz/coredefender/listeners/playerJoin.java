package me.theyellowcreepz.coredefender.listeners;

import me.theyellowcreepz.coredefender.GameState;
import me.theyellowcreepz.coredefender.Main;
import me.theyellowcreepz.coredefender.messageStandards;
import me.theyellowcreepz.coredefender.util.sendTitle;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.potion.PotionEffect;

/**
 * All code inside this plugin is the
 * property of Andrew Petersen.
 * Decompiling, copying, or claiming any
 * code from this plugin as your own
 * voids any legal rights you have to
 * this plugin, unless express permission
 * is given.
 */
public class playerJoin implements Listener {

    @EventHandler
    public static void onPlayerJoin(PlayerJoinEvent evt){
        evt.getPlayer().teleport(Main.lobbySpawnLocExact);
        evt.setJoinMessage("");
        Main.playersOnline.add(evt.getPlayer());
        Player pl = evt.getPlayer();
        pl.getInventory().clear();
        pl.getInventory().setArmorContents(null);
        pl.setMaxHealth(20);
        pl.setDisplayName(pl.getName());
        for(PotionEffect p : pl.getActivePotionEffects()){
            pl.removePotionEffect(p.getType());
        }
        if(Bukkit.getOnlinePlayers().size() >= Main.maximumPlayers){
            pl.kickPlayer(messageStandards.alertPrefix()+ChatColor.RESET+ChatColor.DARK_RED+ChatColor.BOLD
                    +"This server is full! \n"+ChatColor.DARK_AQUA+"Please wait until someone disconnects. ");
        }
        pl.setPlayerListName(pl.getName());
        for(PotionEffect effect : pl.getActivePotionEffects()) {     pl.removePotionEffect(effect.getType()); }
        pl.setGameMode(GameMode.SPECTATOR);
        pl.setPlayerListName(ChatColor.GRAY+pl.getName());
        for(int i=0; i < 100; i ++)
        {
            evt.getPlayer().sendMessage("");
        }
        for(Player onlinePlayers : Bukkit.getOnlinePlayers()){
            onlinePlayers.sendMessage(messageStandards.pluginPrefixMessage()+ ChatColor.AQUA+pl.getName()+ChatColor.GREEN+" has joined the game!");
        }

        if(GameState.getState().equals(GameState.LOBBY)) {
            if(Bukkit.getOnlinePlayers().size() == Main.minimumPlayers){
                for(Player plMSG : Bukkit.getOnlinePlayers()){
                    plMSG.sendMessage(messageStandards.pluginPrefixMessage()+ChatColor.RESET+ChatColor.DARK_GREEN+"The minimum amount of players has been reached!");
                    plMSG.sendMessage(messageStandards.pluginPrefixMessage()+ChatColor.RESET+ChatColor.DARK_GREEN+"The game will start shortly. ");
                    sendTitle.sendTitle(plMSG, 20, 100, 20, (" "), (ChatColor.GRAY+">>> "+ChatColor.GOLD+""+ChatColor.BOLD+"The game will start shortly. "));
                }
            }
                pl.sendMessage(messageStandards.pluginPrefixMessage() + ChatColor.RESET + ChatColor.YELLOW + "The game has not started yet! ");
            pl.sendMessage(messageStandards.pluginPrefixMessage()+ChatColor.RESET+ChatColor.YELLOW+"It will start in "+ChatColor.AQUA+Main.lobbyTicks+ChatColor.YELLOW+" seconds!");
            pl.sendMessage(messageStandards.pluginPrefixMessage()+ChatColor.RESET+ChatColor.YELLOW+"Core Defender requires "+ChatColor.AQUA+Main.minimumPlayers+ChatColor.YELLOW+" players to start. ");
            pl.sendMessage(messageStandards.pluginPrefixMessage()+ChatColor.RESET+ChatColor.YELLOW+"There are currently "+ChatColor.AQUA+Bukkit.getOnlinePlayers().size()+ChatColor.YELLOW+" players online!");

            if(pl.isOp() || pl.hasPermission("CoreDefender.NextState")){
                pl.sendMessage(messageStandards.grayArrows()+ChatColor.RESET+ChatColor.DARK_GRAY+"Alternatively, you are able to use the command "+ChatColor.GRAY+"/nextState"+ChatColor.DARK_GRAY+".");
            }
        } else if(GameState.getState().equals(GameState.INGAME)){

                Main.spectatorTeam.add(pl);
                pl.sendMessage(messageStandards.grayArrows() + ChatColor.RESET + ChatColor.ITALIC + ChatColor.DARK_GRAY
                        + "You are now a spectator!");

        } else if(GameState.getState().equals(GameState.WARMUP) || GameState.getState().equals(GameState.ENDGAME) || GameState.getState().equals(GameState.RESTART)){

                pl.kickPlayer(messageStandards.alertPrefix()+ChatColor.RESET+ChatColor.DARK_RED+ChatColor.BOLD
                        +"You cannot join right now! \n"+ChatColor.DARK_AQUA+"Please try again in a few moments.");

            }
        }
    }
