package me.theyellowcreepz.coredefender.listeners;

import me.theyellowcreepz.coredefender.Main;
import me.theyellowcreepz.coredefender.messageStandards;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;

/**
 * All code inside this plugin is the
 * property of Andrew Petersen.
 * Decompiling, copying, or claiming any
 * code from this plugin as your own
 * voids any legal rights you have to
 * this plugin, unless express permission
 * is given.
 */
public class breakBlockEvent implements Listener {
    @EventHandler
    public void onBlockBreak(BlockBreakEvent evt){
        if(evt.getPlayer() instanceof Player){
            if (!Main.blocksPlaced.contains(evt.getBlock())) {
                evt.setCancelled(true);
                Player pl = evt.getPlayer();
                pl.sendMessage(messageStandards.alertPrefix()+ ChatColor.RESET + ChatColor.RED + "You can't break blocks that weren't placed!");
            } else if(evt.getBlock().getType() == Material.SIGN_POST){
                evt.setCancelled(true);
                evt.getPlayer().sendBlockChange(evt.getBlock().getLocation(), Material.AIR, (byte) 0);
                evt.getPlayer().sendMessage(messageStandards.alertPrefix()+ ChatColor.RESET + ChatColor.RED + "You can't break blocks that weren't placed!");
            }
        }
    }
}