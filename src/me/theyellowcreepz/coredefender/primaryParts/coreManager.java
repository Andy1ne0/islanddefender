package me.theyellowcreepz.coredefender.primaryParts;

import me.theyellowcreepz.coredefender.GameState;
import me.theyellowcreepz.coredefender.Main;
import me.theyellowcreepz.coredefender.messageStandards;
import me.theyellowcreepz.coredefender.returnWinners;
import me.theyellowcreepz.coredefender.util.sendTitle;
import org.bukkit.*;
import org.bukkit.entity.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.inventory.meta.FireworkMeta;

/**
 * All code inside this plugin is the
 * property of Andrew Petersen.
 * Decompiling, copying, or claiming any
 * code from this plugin as your own
 * voids any legal rights you have to
 * this plugin, unless express permission
 * is given.
 */
public class coreManager implements Listener {

    // declare ints for entity health
    public static int redCoreHealth = 10;
    public static int blueCoreHealth = 10;

    public void blueCoreDamaged(){
        if (blueCoreHealth == 1) {
            blueCoreHealth--;
            returnWinners.winners = "Red";
            Main.ingameTicks = 0;
        } else {
            blueCoreHealth--;
            Bukkit.getServer().getLogger().info("[ Core Defender ] The Blue core was attacked. New health: "+blueCoreHealth);
            Main.blueCore.setCustomName(ChatColor.BLUE+""+ChatColor.BOLD+"Blue Core "+ChatColor.DARK_BLUE+"("+ChatColor.GRAY+blueCoreHealth+ChatColor.DARK_BLUE+"/"+ChatColor.GRAY+"10"+ChatColor.DARK_BLUE+")");
            for(Player pl : Bukkit.getOnlinePlayers()){

                pl.playSound(pl.getLocation(), Sound.WITHER_SHOOT, 5, 5);
                pl.sendMessage(messageStandards.alertPrefix()+ChatColor.YELLOW+"Team "+ChatColor.BLUE+"Blue's "+ChatColor.YELLOW+"core was damaged!");
                pl.sendMessage(messageStandards.alertPrefix()+ChatColor.GOLD+"It now has "+ChatColor.AQUA+blueCoreHealth+ChatColor.GOLD+" health points remaining!");
                sendTitle.sendTitle(pl, 10, 60, 10, (ChatColor.AQUA+"Blue Core Damaged!"), (ChatColor.DARK_GRAY+""+ChatColor.BOLD+">> "+ChatColor.RESET+ChatColor.GRAY+"New Health: "+ChatColor.GOLD+blueCoreHealth+"hp"));

            }
            Main.mainWorld.playEffect(Main.blueCore.getLocation(), Effect.MOBSPAWNER_FLAMES, 5);
            Main.mainWorld.createExplosion(Main.blueCoreExact, 0f);
            Main.coreDamageCD.put("blue", 10);
            Firework fw = (Firework) Main.mainWorld.spawnEntity(Main.blueCoreExact, EntityType.FIREWORK);
            FireworkMeta meta = fw.getFireworkMeta();
            meta.setPower(1);
            FireworkEffect eft = FireworkEffect.builder().with(FireworkEffect.Type.BALL_LARGE).withColor(Color.BLUE).trail(true).flicker(true).withFade(Color.BLUE).build();
            meta.addEffect(eft);
        }
    }

    public void redCoreDamaged(){
        if (redCoreHealth == 1) {
            redCoreHealth--;
            returnWinners.winners = "Blue";
            Main.ingameTicks = 0;
        } else {
            redCoreHealth--;
            Bukkit.getServer().getLogger().info("[ Core Defender ] The Red core was attacked. New health: "+redCoreHealth);
            Main.redCore.setCustomName(ChatColor.RED+""+ChatColor.BOLD+"Red Core "+ChatColor.DARK_RED+"("+ChatColor.GRAY+redCoreHealth+ChatColor.DARK_RED+"/"+ChatColor.GRAY+"10"+ChatColor.DARK_RED+")");
            for(Player pl : Bukkit.getOnlinePlayers()){

                pl.playSound(pl.getLocation(), Sound.WITHER_SHOOT, 5, 5);
                pl.sendMessage(messageStandards.alertPrefix()+ChatColor.YELLOW+"Team "+ChatColor.RED+"Red's "+ChatColor.YELLOW+"core was damaged!");
                pl.sendMessage(messageStandards.alertPrefix()+ChatColor.GOLD+"It now has "+ChatColor.AQUA+redCoreHealth+ChatColor.GOLD+" health points remaining!");
                sendTitle.sendTitle(pl, 10, 60, 10, (ChatColor.RED+"Red Core Damaged!"), (ChatColor.DARK_GRAY+""+ChatColor.BOLD+">> "+ChatColor.RESET+ChatColor.GRAY+"New Health: "+ChatColor.GOLD+redCoreHealth+"hp"));
            }
            Main.mainWorld.playEffect(Main.redCore.getLocation(), Effect.MOBSPAWNER_FLAMES, 5);
            Main.mainWorld.createExplosion(Main.redCoreExact, 0f);
            Main.coreDamageCD.put("red", 10);
            Firework fw = (Firework) Main.mainWorld.spawnEntity(Main.redCoreExact, EntityType.FIREWORK);
            FireworkMeta meta = fw.getFireworkMeta();
            meta.setPower(1);
            FireworkEffect eft = FireworkEffect.builder().with(FireworkEffect.Type.BALL_LARGE).withColor(Color.RED).trail(true).flicker(true).withFade(Color.RED).build();
            meta.addEffect(eft);
        }
    }

    @EventHandler
    public void onHit(EntityDamageByEntityEvent evt) {
        if(evt.getEntity() != null && GameState.getState() == GameState.INGAME) {
            if (evt.getEntity().getCustomName() != null && evt.getEntity().getCustomName().equalsIgnoreCase(Main.redCore.getCustomName())) {
                evt.setCancelled(true);
                if (!Main.coreDamageCD.containsKey("red")) {
                    if (evt.getCause() == EntityDamageEvent.DamageCause.PROJECTILE || evt.getCause() == EntityDamageEvent.DamageCause.ENTITY_ATTACK) { // evt.getDamager() instanceof Player
                        if (evt.getDamager() instanceof Arrow) { //  || evt.getDamager() instanceof Player
                            Arrow shotArrow = (Arrow) evt.getDamager();
                            if (shotArrow.getShooter() instanceof Player) {
                                if (!(Main.redTeam.contains(shotArrow.getShooter()))) {
                                    redCoreDamaged();
                                } else if(Main.redTeam.contains(shotArrow.getShooter())){
                                    ((Player) shotArrow.getShooter()).sendMessage(messageStandards.alertPrefix()+ChatColor.RED+"You can't damage your own core! ");
                                }
                            }
                        } else if (evt.getDamager() instanceof Player) {
                            if (!(Main.redTeam.contains(evt.getDamager()))) {
                                redCoreDamaged();
                            } else if(Main.redTeam.contains(evt.getDamager())){
                                ((Player)evt.getDamager()).sendMessage(messageStandards.alertPrefix()+ChatColor.RED+"You can't damage your own core! ");
                            }
                        }
                    }
                } else if (Main.coreDamageCD.containsKey("red")) {
                    evt.setCancelled(true);
                    if (evt.getDamager() instanceof Player) {
                        Player pl = (Player) evt.getDamager();
                        pl.sendMessage(messageStandards.alertPrefix() + ChatColor.RESET + ChatColor.RED + "You can't damage this core for another " + Main.coreDamageCD.get("red") + " seconds!");
                    } else if(evt.getDamager() instanceof Arrow){
                        Arrow arrow = (Arrow) evt.getDamager();
                        if(arrow.getShooter() instanceof Player){
                            Player pl = (Player)arrow.getShooter();
                            pl.sendMessage(messageStandards.alertPrefix() + ChatColor.RESET + ChatColor.RED + "You can't damage this core for another " + Main.coreDamageCD.get("red") + " seconds!");
                        }
                    }

                }
            } else

            if (evt.getEntity().getCustomName() != null && evt.getEntity().getCustomName().equalsIgnoreCase(Main.blueCore.getCustomName())) {
                evt.setCancelled(true);
                if (!Main.coreDamageCD.containsKey("blue")) {
                    if (evt.getCause() == EntityDamageEvent.DamageCause.PROJECTILE || evt.getCause() == EntityDamageEvent.DamageCause.ENTITY_ATTACK) { // evt.getDamager() instanceof Player
                        if (evt.getDamager() instanceof Arrow) { //  || evt.getDamager() instanceof Player
                            Arrow shotArrow = (Arrow) evt.getDamager();
                            if (shotArrow.getShooter() instanceof Player) {
                                if (!(Main.blueTeam.contains(evt.getDamager()))) {
                                    blueCoreDamaged();
                                } else if(Main.blueTeam.contains(shotArrow.getShooter())){
                                    ((Player) shotArrow.getShooter()).sendMessage(messageStandards.alertPrefix()+ChatColor.RED+"You can't damage your own core! ");
                                }
                            }
                        } else if (evt.getDamager() instanceof Player) {
                            if (!(Main.blueTeam.contains(evt.getDamager()))) {
                                blueCoreDamaged();
                            } else if(Main.blueTeam.contains(evt.getDamager())){
                                ((Player)evt.getDamager()).sendMessage(messageStandards.alertPrefix()+ChatColor.RED+"You can't damage your own core! ");
                            }
                        }
                    }
                } else if (Main.coreDamageCD.containsKey("blue")) {
                    evt.setCancelled(true);
                    if (evt.getDamager() instanceof Player) {
                        Player pl = (Player) evt.getDamager();
                        pl.sendMessage(messageStandards.alertPrefix() + ChatColor.RESET + ChatColor.RED + "You can't damage this core for another " + Main.coreDamageCD.get("blue") + " seconds!");
                    } else if(evt.getDamager() instanceof Arrow){
                        Arrow arrow = (Arrow) evt.getDamager();
                        if(arrow.getShooter() instanceof Player){
                            Player pl = (Player)arrow.getShooter();
                            pl.sendMessage(messageStandards.alertPrefix() + ChatColor.RESET + ChatColor.RED + "You can't damage this core for another " + Main.coreDamageCD.get("blue") + " seconds!");
                        }
                    }
                }
            }
            else {

            }
        } else {

            if(GameState.getState() != GameState.INGAME && evt.getEntity() instanceof EnderCrystal){ //  && (evt.getEntity().getCustomName().equals(Main.redCore.getCustomName()) || evt.getEntity().getCustomName().equals(Main.blueCore.getCustomName()))
                evt.setCancelled(true);
                if(evt.getDamager() instanceof Player){
                    ((Player) evt.getDamager()).sendMessage(messageStandards.alertPrefix() + ChatColor.RESET + ChatColor.RED + "You can't damage this core right now!");
                }
            }

        }
    }

    public static Entity summonCore(String team){
        if(team == "Red"){
            Entity redCrystal = (EnderCrystal) Main.mainWorld.spawnEntity(Main.redCoreExact, EntityType.ENDER_CRYSTAL);
            redCrystal.setCustomName(ChatColor.RED+""+ChatColor.BOLD+"Red Core "+ChatColor.DARK_RED+"("+ChatColor.GRAY+redCoreHealth+ChatColor.DARK_RED+"/"+ChatColor.GRAY+"10"+ChatColor.DARK_RED+")");
            redCrystal.setCustomNameVisible(true);
            return redCrystal;
        } else if(team == "Blue"){
            Entity blueCrystal = (EnderCrystal) Main.mainWorld.spawnEntity(Main.blueCoreExact, EntityType.ENDER_CRYSTAL);
            blueCrystal.setCustomName(ChatColor.BLUE+""+ChatColor.BOLD+"Blue Core "+ChatColor.DARK_BLUE+"("+ChatColor.GRAY+blueCoreHealth+ChatColor.DARK_BLUE+"/"+ChatColor.GRAY+"10"+ChatColor.DARK_BLUE+")");
            blueCrystal.setCustomNameVisible(true);
            return blueCrystal;
        }
        return null;
    }
}
