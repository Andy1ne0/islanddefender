package me.theyellowcreepz.coredefender;

/**
 * All code inside this plugin is the
 * property of Andrew Petersen.
 * Decompiling, copying, or claiming any
 * code from this plugin as your own
 * voids any legal rights you have to
 * this plugin, unless express permission
 * is given.
 */
public enum GameState {

    LOBBY, LOADINGMAP, WARMUP, INGAME, ENDGAME, RESTART;

    private static GameState state;

    public static void setState(GameState state) {
        GameState.state = state;
    }

    public static boolean isState(GameState state)
    {
        return GameState.state == state;
    }

    public static GameState getState()
    {
        return state;
    }

}
