package me.theyellowcreepz.coredefender;

import org.bukkit.ChatColor;

/**
 * All code inside this plugin is the
 * property of Andrew Petersen.
 * Decompiling, copying, or claiming any
 * code from this plugin as your own
 * voids any legal rights you have to
 * this plugin, unless express permission
 * is given.
 */
public class returnWinners {
    public static String winners;
    public static String getWinningTeam(){
        if(winners == "Red"){
            return (ChatColor.DARK_RED+""+ChatColor.BOLD+"Red Team");
        } else if(winners == "Blue"){
            return (ChatColor.BLUE+""+ChatColor.BOLD+"Blue Team");
        } else {
            return (ChatColor.YELLOW+""+ChatColor.BOLD+"No Winner");
        }
    }
}
