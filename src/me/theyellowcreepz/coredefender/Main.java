package me.theyellowcreepz.coredefender;

import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.ProtocolManager;
import me.theyellowcreepz.coredefender.commands.*;
import me.theyellowcreepz.coredefender.inventories.effectCooldowns;
import me.theyellowcreepz.coredefender.inventories.kitCooldowns;
import me.theyellowcreepz.coredefender.inventories.kitManager;
import me.theyellowcreepz.coredefender.listeners.*;
import me.theyellowcreepz.coredefender.primaryParts.coreManager;
import me.theyellowcreepz.coredefender.scoreboards.scoreboardManager;
import me.theyellowcreepz.coredefender.util.*;
import me.theyellowcreepz.coredefender.worldloaders.cycleCores;
import me.theyellowcreepz.coredefender.worldloaders.cycleSpawns;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.EnderCrystal;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.potion.PotionEffect;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitScheduler;

import java.io.File;
import java.util.*;

/**
 * All code inside this plugin is the
 * property of Andrew Petersen.
 * Decompiling, copying, or claiming any
 * code from this plugin as your own
 * voids any legal rights you have to
 * this plugin, unless express permission
 * is given.
 */
public class Main extends JavaPlugin {
    // Declarations
    public static int warmupTicks = 30;
    public static int lobbyTicks = 45;
    public static int ingameTicks = 900;
    public static int endgameTicks = 10;
    public static String ingameTimeIndicator = null;
    public final static int minimumPlayers = 4;
    public final static int maximumPlayers = 16;
    public static boolean lobbyRunning = false;
    public static boolean warmupRunning = false;
    public static boolean ingameRunning = false;
    public static boolean endgameRunning = false;
    public static boolean loadingmapRunning = false;
    public static Map<String, Location> spawnLocations = new HashMap<String, Location>();
    public static Map<String, Location> teamCores = new HashMap<String, Location>();
    public static HashSet<Player> playersOnline = new HashSet<Player>();
    public static HashSet<Player> redTeam = new HashSet<Player>();
    public static HashSet<Player> blueTeam = new HashSet<Player>();
    public static HashSet<Player> spectatorTeam = new HashSet<Player>();
    public static Map<Player, Integer> playerKits = new HashMap<Player, Integer>();
    public static Map<Player, Integer> playerRespawnTimes = new HashMap<Player, Integer>();
    public static HashSet<Block> blocksPlaced = new HashSet<Block>();

    // Core damage cooldowns.
    public static Map<String, Integer> coreDamageCD = new HashMap<String, Integer>();

    // Spawn Locations, public.

    public static Location redSpawnExact;
    public static Location blueSpawnExact;
    public static Location genSpawnExact;

    public static Location redSpawnApprox;
    public static Location blueSpawnApprox;
    public static Location generalSpawnApprox;

    public static Location redCoreLoc;
    public static Location blueCoreLoc;

    public static Location redCoreExact;
    public static Location blueCoreExact;

    public static Location lobbySpawnLocExact;

    // Core declarations.
    public static Entity redCore;
    public static Entity blueCore;

    // World Declarations.
    public static World mainWorld;
    public static MapInstance mainWorldMapInstance;
    public static World lobbyWorld;

    // ProtocolLib
    public static ProtocolManager protocolManager;

    // SQL manager instance.
    sqlmanager sql;

    // Map voting.
    public static HashMap<Player, Integer> mapVotes = new HashMap<>();
    public static HashMap<Integer, MapInstance> mapsToVoteOn = new HashMap<>();

    // TODO implement a debug mode feature.

    public static Plugin getInstance(){
        return Bukkit.getServer().getPluginManager().getPlugin("CoreDefender");
    }

    // All kit schedulers
    public void enableKitSchedulers(){
        BukkitScheduler scheduler = Bukkit.getScheduler();
        scheduler.scheduleSyncRepeatingTask(this, new kitCooldowns(), 20l, 20l);
        scheduler.scheduleSyncRepeatingTask(this, new effectCooldowns(), 20l, 20l);
    }

    public void registerConsoleAnalytics(){
        BukkitScheduler sched = Bukkit.getScheduler();
        sched.scheduleSyncRepeatingTask(this, new Runnable() {
            @Override
            public void run() {
                ConsoleCommandSender server = Bukkit.getServer().getConsoleSender();
                server.sendMessage("");
                server.sendMessage(ChatColor.AQUA + "[ Core Defender ] Game statistics: ");
                server.sendMessage(ChatColor.GOLD + "Players online: " + ChatColor.GREEN+ Bukkit.getOnlinePlayers().size());
                server.sendMessage(ChatColor.GOLD + "Game State: " +ChatColor.GREEN +GameState.getState());
                server.sendMessage(ChatColor.GOLD + "System Time (Millis): " +ChatColor.GREEN+ System.currentTimeMillis());
                server.sendMessage(ChatColor.GOLD + "Server TPS: " +ChatColor.GREEN+ getTPS.getTPS());
                if(GameState.getState().equals(GameState.INGAME)){
                    server.sendMessage(ChatColor.AQUA+"In-Game Statistics: ");
                    server.sendMessage(ChatColor.GOLD+"Red Core Health: "+ChatColor.GREEN+coreManager.redCoreHealth+"hp");
                    server.sendMessage(ChatColor.GOLD+"Blue Core Health: "+ChatColor.GREEN+coreManager.blueCoreHealth+"hp");
                    server.sendMessage(ChatColor.GOLD+"Players online: ");
                    server.sendMessage(ChatColor.GREEN+"Red Team: "+Main.redTeam.size());
                    server.sendMessage(ChatColor.GREEN+"Blue Team: "+Main.blueTeam.size());
                    server.sendMessage(ChatColor.GOLD+"Time Remaining (InGame): "+Main.ingameTicks+" seconds");
                }
                server.sendMessage("");
            }
        }, 20l, 1200l);
    }

    public void registerCoreDamageCooldown(){
        BukkitScheduler sched = Bukkit.getScheduler();
        sched.scheduleSyncRepeatingTask(this, new Runnable() {
            @Override
            public void run() {
                if(coreDamageCD.containsKey("red")){
                    if(coreDamageCD.get("red").equals(1)){
                        coreDamageCD.remove("red");
                        for(Player pl: Bukkit.getOnlinePlayers()){
                            pl.sendMessage(messageStandards.grayArrows()+ChatColor.GREEN+"The red core is damageable again!");
                        }
                    } else {
                        int updTime = coreDamageCD.get("red") -1;
                        coreDamageCD.remove("red");
                        coreDamageCD.put("red", updTime);
                    }
                } else if(coreDamageCD.containsKey("blue")){
                    if(coreDamageCD.get("blue").equals(1)){
                        coreDamageCD.remove("blue");
                        for(Player pl: Bukkit.getOnlinePlayers()){
                            pl.sendMessage(messageStandards.grayArrows()+ChatColor.GREEN+"The blue core is damageable again!");
                        }
                    } else {
                        int updTime = coreDamageCD.get("blue") -1;
                        coreDamageCD.remove("blue");
                        coreDamageCD.put("blue", updTime);
                    }
                }
            }
        }, 20l, 20l);
    }

    public void loopPlayerHunger(){
        Bukkit.getScheduler().scheduleSyncRepeatingTask(this, new Runnable() {
            @Override
            public void run() {
                for(Player pl : Bukkit.getOnlinePlayers()){
                    pl.setFoodLevel(20);
                    pl.setSaturation(20);
                }
            }
        }, 20l, 100l);
    }

    public static void sendMapVotingInfo(Player pl){

        // 【 】➤╔╚║▂▃▅▆▇═

        pl.sendMessage(ChatColor.DARK_GREEN+""+ChatColor.BOLD+"╔════════"+messageStandards.pluginPrefixMessage()+ChatColor.AQUA+"/vote <ID> "+ChatColor.DARK_GREEN+""+ChatColor.BOLD+"════════");
        for(int i = 0; i < Main.mapsToVoteOn.size(); i++){
            int mapVotes = 0;
            for(Player player : Main.mapVotes.keySet()){
                int thatPlayersVote = Main.mapVotes.get(player);
                if(thatPlayersVote == i){
                    mapVotes++;
                }
            }
            pl.sendMessage(ChatColor.DARK_GREEN+""+ChatColor.BOLD+"║ "+
                    ChatColor.DARK_GRAY+"["+ChatColor.YELLOW+(i+1)+ChatColor.DARK_GRAY+"]"+ChatColor.GOLD +Main.mapsToVoteOn.get(i).getPublicTitle() +ChatColor.DARK_GRAY+" (" +ChatColor.GOLD +mapVotes + ChatColor.GOLD+" votes"+ChatColor.DARK_GRAY+")");
        }
        pl.sendMessage(ChatColor.DARK_GREEN+""+ChatColor.BOLD+"╚═══════════════════════════════");

    }

    public void lobbyPluginTicks(){
        Bukkit.getScheduler().scheduleSyncRepeatingTask(this, new Runnable() {
            @Override
            public void run() {
                if (lobbyRunning) {
                    if (!GameState.isState(GameState.LOBBY)) {
                        GameState.setState(GameState.LOBBY);
                        logGameState.logState();
                    }
                    if (playersOnline.size() >= minimumPlayers) {
                        lobbyTicks--;
                        if (lobbyTicks <= 0) {
                            lobbyRunning = false;

                            if(getConfig().getBoolean("use_map_voting")) {
                                loadingmapRunning = true;
                            } else {
                                warmupRunning = true;
                            }

                        } else if (lobbyTicks == 30 || lobbyTicks == 20 || lobbyTicks <= 10) {
                            for (Player pl : Bukkit.getOnlinePlayers()) {
                                pl.sendMessage(messageStandards.pluginPrefixMessage() + ChatColor.GREEN + "The game starts in " + ChatColor.AQUA + warmupTicks + ChatColor.GREEN + " seconds!");
                            }
                        }

                    } else {
                        if (lobbyTicks != 45) {
                            lobbyTicks = 45;
                        }
                    }
                    for(Player pl : Bukkit.getOnlinePlayers()){
                        sendActionBar.sendTitle(pl, ChatColor.GRAY+">>> "+ChatColor.GOLD+""+ChatColor.BOLD+"The game will start shortly. ");
                    }
                }
            }
        }, 20l, 20l);
    }

    public void loadingmapTicks(){
        BukkitScheduler sched = Bukkit.getScheduler();
        sched.scheduleSyncRepeatingTask(this, new Runnable() {
            @Override
            public void run() {
                if(loadingmapRunning){
                    if(!GameState.getState().equals(GameState.LOADINGMAP)){
                        GameState.setState(GameState.LOADINGMAP);
                        logGameState.logState();
                    }

                    HashMap<MapInstance, Integer> finalVotes = new HashMap<MapInstance, Integer>();

                    if(mapVotes.size() < 1){
                        mapVotes.put((Player)Bukkit.getOnlinePlayers().toArray()[0], (int)mapsToVoteOn.keySet().toArray()[0]);
                    }

                    for(Player pl : mapVotes.keySet()){
                        int playerVote = mapVotes.get(pl);
                        MapInstance votedMap = mapsToVoteOn.get(playerVote);
                        if(!finalVotes.containsKey(votedMap)){
                            finalVotes.put(votedMap, 1);
                        } else {
                            int newVotesCount = finalVotes.get(votedMap);
                            finalVotes.remove(votedMap);
                            finalVotes.put(votedMap, newVotesCount + 1);
                        }
                    }

                    MapInstance temp = null;

                    for(MapInstance m : finalVotes.keySet()){
                        if(temp == null || finalVotes.get(m) > finalVotes.get(temp)) {
                            temp = m;
                        }
                    }

                    MapInstance winningMap = temp;
                    mainWorldMapInstance = winningMap;

                    for(Player pl : Bukkit.getOnlinePlayers()){
                        pl.sendMessage(messageStandards.pluginPrefixMessage()+ChatColor.GREEN+"Voting has ended! ");
                        pl.sendMessage(messageStandards.pluginPlayerInputPrefix()+"The winning map was: "+ChatColor.YELLOW+winningMap.getPublicTitle()+ChatColor.GREEN+"! ");
                        pl.sendMessage(messageStandards.pluginPrefixMessage()+ChatColor.AQUA+"Loading map now. This may take a while. ");
                    }

                    new BukkitRunnable() {
                        @Override
                        public void run() {
                            if(GameState.getState().equals(GameState.LOADINGMAP)){
                                for(Player pl : Bukkit.getOnlinePlayers()){
                                    pl.sendMessage(messageStandards.alertPrefix()+ChatColor.RED+"The map loading is taking a bit longer than usual. Please be patient! ");
                                }
                                Bukkit.getServer().getLogger().info("[ Core Defender ] Map "+mainWorld.getName()+" is taking too long to load! ");
                                new BukkitRunnable() {
                                    @Override
                                    public void run() {
                                        if(GameState.getState().equals(GameState.LOADINGMAP)){
                                            Bukkit.getServer().getLogger().severe("Map "+mainWorld.getName()+" took too long to load. Server terminated, make sure it is not corrupted. ");
                                            Bukkit.getServer().shutdown();
                                        }
                                    }
                                }.runTaskLater(getInstance(), 400l);
                            }
                        }
                    }.runTaskLater(getInstance(), 200l);

                    mainWorld = winningMap.getWorld();

                    // Setting spawn locations. Defined above.
                    redSpawnApprox = new Location(Main.mainWorld, mainWorldMapInstance.getConfig().getDouble("teamSpawns.red.x"), mainWorldMapInstance.getConfig().getDouble("teamSpawns.red.y"), mainWorldMapInstance.getConfig().getDouble("teamSpawns.red.z"));
                    blueSpawnApprox = new Location(Main.mainWorld, mainWorldMapInstance.getConfig().getDouble("teamSpawns.blue.x"), mainWorldMapInstance.getConfig().getDouble("teamSpawns.blue.y"), mainWorldMapInstance.getConfig().getDouble("teamSpawns.blue.z"));
                    generalSpawnApprox = new Location(Main.mainWorld, mainWorldMapInstance.getConfig().getDouble("generalSpawn.x"), mainWorldMapInstance.getConfig().getDouble("generalSpawn.y"), mainWorldMapInstance.getConfig().getDouble("generalSpawn.z"));

                    redCoreLoc = new Location(Main.mainWorld, mainWorldMapInstance.getConfig().getDouble("coreLocations.redCore.x"), mainWorldMapInstance.getConfig().getDouble("coreLocations.redCore.y"), mainWorldMapInstance.getConfig().getDouble("coreLocations.redCore.z"));
                    blueCoreLoc = new Location(Main.mainWorld, mainWorldMapInstance.getConfig().getDouble("coreLocations.blueCore.x"), mainWorldMapInstance.getConfig().getDouble("coreLocations.blueCore.y"), mainWorldMapInstance.getConfig().getDouble("coreLocations.blueCore.z"));

                    loadingmapRunning = false;
                    warmupRunning = true;
                }
            }
        }, 20l, 20l);
    }

    public void WarmupPluginTicks(){
        BukkitScheduler scheduler = Bukkit.getServer().getScheduler();
        scheduler.scheduleSyncRepeatingTask(this, new Runnable() {
            @Override
            public void run() {
                if (warmupRunning) {
                    if(!GameState.isState(GameState.WARMUP)){
                        GameState.setState(GameState.WARMUP);
                        logGameState.logState();
                        enableKitSchedulers();
                        registerCoreDamageCooldown();

                    }
                    if(warmupTicks == 30){
                        try {

                            if(!getConfig().getBoolean("use_map_voting")){
                                mainWorld = Bukkit.getWorlds().get(0);
                                redSpawnApprox = new Location(Main.mainWorld, getConfig().getDouble("teamSpawns.red.x"), getConfig().getDouble("teamSpawns.red.y"), getConfig().getDouble("teamSpawns.red.z"));
                                blueSpawnApprox = new Location(Main.mainWorld, getConfig().getDouble("teamSpawns.blue.x"), getConfig().getDouble("teamSpawns.blue.y"), getConfig().getDouble("teamSpawns.blue.z"));
                                generalSpawnApprox = new Location(Main.mainWorld, getConfig().getDouble("generalSpawn.x"), getConfig().getDouble("generalSpawn.y"), getConfig().getDouble("generalSpawn.z"));

                                redCoreLoc = new Location(Main.mainWorld, getConfig().getDouble("coreLocations.redCore.x"), getConfig().getDouble("coreLocations.redCore.y"), getConfig().getDouble("coreLocations.redCore.z"));
                                blueCoreLoc = new Location(Main.mainWorld, getConfig().getDouble("coreLocations.blueCore.x"), getConfig().getDouble("coreLocations.blueCore.y"), getConfig().getDouble("coreLocations.blueCore.z"));

                            }
                            configureWorldSettings();
                            blueSpawnExact = cycleSpawns.setSpawns("Blue");
                            redSpawnExact = cycleSpawns.setSpawns("Red");
                            redCoreExact = cycleCores.cycleCores("Red");
                            blueCoreExact = cycleCores.cycleCores("Blue");
                            genSpawnExact = cycleSpawns.setSpawns("General");
                            if(blueSpawnExact == null){
                                Bukkit.getServer().getLogger().severe("CRITICAL: Blue Spawn not configured properly. ");
                                throw new NullPointerException();
                            }
                            if(redSpawnExact == null){
                                Bukkit.getServer().getLogger().severe("CRITICAL: Red Spawn not configured properly. ");
                                throw new NullPointerException();
                            }
                            if(redCoreExact == null){
                                Bukkit.getServer().getLogger().severe("CRITICAL: Red Core not configured properly. ");
                                throw new NullPointerException();
                            }
                            if(blueCoreExact == null){
                                Bukkit.getServer().getLogger().severe("CRITICAL: Blue Core not configured properly. ");
                                throw new NullPointerException();
                            }
                            if(genSpawnExact == null){
                                Bukkit.getServer().getLogger().severe("CRITICAL: General Spawn not configured properly. ");
                                throw new NullPointerException();
                            }
                            Entity redCore = (EnderCrystal) coreManager.summonCore("Red");
                            Entity blueCore = (EnderCrystal) coreManager.summonCore("Blue");
                        } catch(NullPointerException e){
                            Bukkit.getServer().getLogger().severe("Check your config, you've made an error! ");
                            Bukkit.getServer().getLogger().severe("StackTrace printed for debugging purposes, the server has also terminated for stability. ");
                            Bukkit.getServer().getLogger().info("World name: "+mainWorld.getName());
                            e.printStackTrace();
                            Bukkit.getServer().shutdown();
                        } catch(Exception e){
                            Bukkit.getServer().getLogger().severe("CRITICAL: Error when setting spawns and/or cores. ");
                            Bukkit.getServer().getLogger().severe("StackTrace printed for debugging purposes - SEND TO YELLOW - the server has also terminated for stability. ");
                            e.printStackTrace();
                            Bukkit.getServer().shutdown();
                    }


                        for (Player pl : playersOnline){
                            pl.setHealth(20);
                            if (redTeam.size() > blueTeam.size()){
                                if(!blueTeam.contains(pl) && !(redTeam.contains(pl))) {
                                    blueTeam.add(pl);
                                    pl.sendMessage(messageStandards.pluginPlayerInputPrefix() + "You joined the " + ChatColor.AQUA + "Blue " + ChatColor.GREEN + "team!");
                                    pl.setPlayerListName(ChatColor.AQUA + "" + ChatColor.BOLD+"BLUE "+ChatColor.AQUA+ pl.getName());
                                    pl.setDisplayName(ChatColor.AQUA + pl.getName());
                                    pl.teleport(blueSpawnExact);
                                }
                            } else {
                                if(!redTeam.contains(pl) && !(blueTeam.contains(pl))) {
                                    redTeam.add(pl);
                                    pl.sendMessage(messageStandards.pluginPlayerInputPrefix() + "You joined the " + ChatColor.RED + "Red " + ChatColor.GREEN + "team!");
                                    pl.setPlayerListName(ChatColor.RED + "" + ChatColor.BOLD + "RED " + ChatColor.RED + pl.getName());
                                    pl.setDisplayName(ChatColor.RED + pl.getName());
                                    pl.teleport(redSpawnExact);
                                }
                            }
                            if(redTeam.contains(pl)){
                                sendTitle.sendTitle(pl, 1, 80, 1, (ChatColor.GOLD+""+"You joined "+ChatColor.RED+""+ChatColor.BOLD+"RED"+ChatColor.GOLD+"!"), (ChatColor.RESET+""+ChatColor.DARK_GRAY+""+ChatColor.BOLD+">> "+ChatColor.GRAY+"The game will begin shortly. "));
                            } else if(blueTeam.contains(pl)){
                                sendTitle.sendTitle(pl, 1, 80, 1, (ChatColor.GOLD+""+"You joined "+ChatColor.AQUA+""+ChatColor.BOLD+"BLUE"+ChatColor.GOLD+"!"), (ChatColor.RESET+""+ChatColor.DARK_GRAY+""+ChatColor.BOLD+">> "+ChatColor.GRAY+"The game will begin shortly. "));
                            }
                        }

                        int entitiesKilled = 0;
                        if(Main.mainWorld.getEntities().size() >= 1) {
                            for (Entity e : Main.mainWorld.getEntities()) {
                                if (!(e instanceof Player)) {
                                    e.remove();
                                    entitiesKilled = entitiesKilled + 1;
                                }
                            }
                        }

                        for(Player pl : playersOnline){
                            pl.setGameMode(GameMode.ADVENTURE);
                            pl.openInventory(kitManager.kitSelMenu);
                            // pl.openInventory(kitSelector.kitSelectorMenu);
                            pl.sendMessage(messageStandards.pluginPlayerInputPrefix()+"Use /kit to change your kit!");
                            pl.getInventory().clear();
                            ItemStack kitSelectorPaper = new ItemStack(Material.PAPER, 1);
                            ItemMeta kitSelectorMeta = kitSelectorPaper.getItemMeta();
                            kitSelectorMeta.setDisplayName(ChatColor.GOLD+""+ChatColor.BOLD+"Select your kit! "+messageStandards.rightClickText());
                            kitSelectorPaper.setItemMeta(kitSelectorMeta);
                            pl.getInventory().addItem(kitSelectorPaper);
                            pl.getInventory().setHeldItemSlot(0);
                        }
                    }
                    warmupTicks--;
                    if (warmupTicks <= 0) {
                        warmupRunning = false;
                        ingameRunning = true;
                    } else if (warmupTicks == 20 || warmupTicks == 15 || warmupTicks <= 10) {
                        Bukkit.getServer().getLogger().info("[ Core Defender ] "+warmupTicks+" seconds remaining in WARMUP mode. ");
                        for (Player pl : Bukkit.getOnlinePlayers()) {
                            pl.playSound(pl.getLocation(), Sound.NOTE_PLING, 10, 5);
                            pl.sendMessage(messageStandards.pluginPrefixMessage()+ ChatColor.AQUA + warmupTicks + ChatColor.GREEN + " seconds!");
                            sendTitle.sendTitle(pl, 0, 60, 0, (" "), (ChatColor.RESET+""+ChatColor.DARK_GRAY+""+ChatColor.BOLD+">> "+ChatColor.GRAY+"The game will begin in "+ChatColor.GOLD+warmupTicks+ChatColor.GRAY+" seconds. "));
                        }
                    }
                    for(Player pl : Bukkit.getOnlinePlayers()){
                        if(redTeam.contains(pl)){
                            sendActionBar.sendTitle(pl, ChatColor.GRAY+">>> "+ChatColor.WHITE+""+ChatColor.BOLD+"You're on the "+ChatColor.RED+ChatColor.BOLD+"RED "+ChatColor.RESET+ChatColor.WHITE+ChatColor.BOLD+"team!");
                        } else if(blueTeam.contains(pl)){
                            sendActionBar.sendTitle(pl, ChatColor.GRAY+">>> "+ChatColor.WHITE+""+ChatColor.BOLD+"You're on the "+ChatColor.AQUA+ChatColor.BOLD+"BLUE "+ChatColor.RESET+ChatColor.WHITE+ChatColor.BOLD+"team!");
                        }
                    }
                    if(warmupTicks == 15){
                        for(Player pl : Bukkit.getOnlinePlayers()){
                            pl.playSound(pl.getLocation(), Sound.FUSE, 10, 5);
                            pl.sendMessage(messageStandards.questionPref()+"In Core Defender, you must protect your core. ");
                            pl.sendMessage(messageStandards.questionPref()+"You must also attack the other team's core. ");
                            pl.sendMessage(messageStandards.questionPref()+"Each core has "+ChatColor.GOLD+"10 "+ChatColor.LIGHT_PURPLE+"lives. ");
                            pl.sendMessage(messageStandards.questionPref()+"Each core can only be attacked once every 10 seconds. ");
                            pl.sendMessage(messageStandards.questionPref()+"Once a core loses its' 10 lives, the other team wins. ");
                            pl.sendMessage(messageStandards.questionPref()+"Have fun!");
                        }
                    }
                }
            }
        }, 20L, 20L);

    }

    public void registerLobbyAlerts(){
        BukkitScheduler sched = Bukkit.getScheduler();
        sched.scheduleSyncRepeatingTask(this, new Runnable() {
            @Override
            public void run() {
                if (GameState.getState().equals(GameState.LOBBY)) {

                    for (Player pl : Bukkit.getOnlinePlayers()) {
                        pl.sendMessage(messageStandards.pluginPrefixMessage() + ChatColor.RESET + ChatColor.YELLOW + "Core Defender requires " + ChatColor.AQUA + "4 " + ChatColor.YELLOW + "players for the game to start. ");
                        pl.sendMessage(messageStandards.pluginPrefixMessage() + ChatColor.RESET + ChatColor.YELLOW + "There are currently " + ChatColor.AQUA + Bukkit.getOnlinePlayers().size() + ChatColor.YELLOW + " players online!");

                        if (pl.isOp() || pl.hasPermission("CoreDefender.NextState")) {
                            pl.sendMessage(messageStandards.grayArrows() + ChatColor.RESET + ChatColor.DARK_GRAY + "Alternatively, you are able to use the command " + ChatColor.GRAY + "/nextState" + ChatColor.DARK_GRAY + ".");
                        }
                        sendMapVotingInfo(pl);
                    }
                }
            }
        }, 20l, 900l);
    }

    public void inGameTicks(){
        BukkitScheduler scheduler = Bukkit.getScheduler();
        scheduler.scheduleSyncRepeatingTask(this, new Runnable() {
            @Override
            public void run() {
                if(ingameRunning){
                    if(!GameState.isState(GameState.INGAME)){
                        GameState.setState(GameState.INGAME);
                        logGameState.logState();
                        int entitiesKilled = 0;
                        if(Main.mainWorld.getEntities().size() >= 1) {
                            for (Entity e : Main.mainWorld.getEntities()) {
                                if (!(e instanceof Player)) {
                                    e.remove();
                                    entitiesKilled = entitiesKilled + 1;
                                }
                            }
                        }
                        for(Player pl: Bukkit.getOnlinePlayers()){
                            pl.setGameMode(GameMode.SURVIVAL);
                            pl.setHealth(20);
                            pl.getInventory().clear();
                            pl.getInventory().setArmorContents(null);
                            kitManager.givePlayerKit(pl);
                        }
                    }
                    if(!(Main.mainWorld.getEntities().contains(blueCore))){
                        blueCore = coreManager.summonCore("Blue");
                    }
                    if(!Main.mainWorld.getEntities().contains(redCore)){
                        redCore = coreManager.summonCore("Red");
                    }
                    if(ingameTicks == 900){

                        for(Player pl : Bukkit.getOnlinePlayers()){
                            pl.sendMessage(messageStandards.pluginPrefixMessage()+ChatColor.DARK_GREEN+"The game has started!");
                            sendTitle.sendTitle(pl, 10, 80, 10, (ChatColor.GREEN+"The game has begun! "), (ChatColor.DARK_GRAY+""+ChatColor.BOLD+">> "+ChatColor.RESET+ChatColor.GRAY+"Good luck! "));
                        }
                        for(Player pl : Bukkit.getOnlinePlayers()) {
                            if (redTeam.contains(pl)){
                                pl.teleport(redSpawnExact);
                                pl.sendMessage(messageStandards.grayArrows()+"You were teleported to the "+ChatColor.RED+"Red Team"+ChatColor.GRAY+ChatColor.BOLD+"'s base!");
                            } else if(blueTeam.contains(pl)){
                                pl.teleport(blueSpawnExact);
                                pl.sendMessage(messageStandards.grayArrows()+"You were teleported to the "+ChatColor.BLUE+"Blue Team"+ChatColor.GRAY+ChatColor.BOLD+"'s base!");
                            }
                        }
                    }

                    if(ingameTicks <= 0){
                        if(returnWinners.winners != "Red" && returnWinners.winners != "Blue"){
                                returnWinners.winners = null;
                        }
                        ingameRunning = false;
                        endgameRunning = true;
                    } else if(ingameTicks == 60
                            || ingameTicks == 45 || ingameTicks == 30 || ingameTicks == 20 || ingameTicks <= 15){
                        Bukkit.getServer().getLogger().info("[ Core Defender ] "+ingameTicks+" seconds remaining in INGAME state. ");
                        for(Player pl : Bukkit.getOnlinePlayers()){
                            pl.sendMessage(messageStandards.pluginPrefixMessage() + ChatColor.GOLD + (ingameTicks) + " seconds remaining!");
                        }
                    } else if(ingameTicks == 900 || ingameTicks == 840 || ingameTicks == 780 || ingameTicks == 720
                            || ingameTicks == 660 || ingameTicks == 600 || ingameTicks == 540 || ingameTicks == 480
                            || ingameTicks == 420 || ingameTicks == 360 || ingameTicks == 300 || ingameTicks == 240
                            || ingameTicks == 180 || ingameTicks == 120 || ingameTicks == 90){
                        Bukkit.getServer().getLogger().info("[CoreDefenderTimer] "+ingameTicks+" ticks remaining. ");
                        ingameTimeIndicator = ((ingameTicks / 60 )+" minutes");
                        for(Player pl : Bukkit.getOnlinePlayers()){
                            pl.sendMessage(messageStandards.pluginPrefixMessage()+ChatColor.GOLD+(ingameTicks / 60)+" minutes remaining!");

                        }
                    }
                    if (ingameTicks <= 60) {
                        ingameTimeIndicator = (+ingameTicks+" seconds");
                    }
                    ingameTicks = ingameTicks - 1;
                    for(Player pl : Bukkit.getOnlinePlayers()){
                        pl.setFoodLevel(30);
                        sendActionBar.sendTitle(pl, (ChatColor.GRAY+">>> "+ChatColor.WHITE+""+ChatColor.BOLD+"Time Remaining: "+ChatColor.GREEN+""+ChatColor.BOLD+ingameTimeIndicator));
                    }
                }

            }
        }, 20l, 20l);

    }

    public void endGamePluginTicks(){
        BukkitScheduler scheduler = Bukkit.getScheduler();
        scheduler.scheduleSyncRepeatingTask(this, new Runnable() {
            @Override
            public void run() {
                if(endgameRunning){
                    if (endgameTicks == 0){
                        playersOnline.clear();
                        redTeam.clear();
                        blueTeam.clear();
                        if(Bukkit.getOnlinePlayers().size()>=1) {
                            for (Player pl : Bukkit.getOnlinePlayers()) {
                                pl.kickPlayer(ChatColor.RED + "" + ChatColor.BOLD + ">> " + ChatColor.AQUA + "Thank you for playing, " + pl.getName() + "!\n The game has ENDED!");
                            }
                        }
                        endgameRunning = false;
                        Bukkit.dispatchCommand(getServer().getConsoleSender(), "reload");
                    } else {
                        if (endgameTicks == 10){

                            GameState.setState(GameState.ENDGAME);

                            if(blocksPlaced != null && blocksPlaced.size() >= 1) {
                                for (Block block : blocksPlaced) {
                                    block.setType(Material.AIR);
                                }
                            }


                            int entitiesKilled = 0;
                            if(Main.mainWorld.getEntities().size() >= 1) {
                                for (Entity e : Main.mainWorld.getEntities()) {
                                    if (!(e instanceof Player)) {
                                        e.remove();
                                        entitiesKilled = entitiesKilled + 1;
                                    }
                                }
                            }
                            Bukkit.getServer().getLogger().info("[ Core Defender ] Killed "+entitiesKilled+" entities.");
                            blocksPlaced = null;

                            for(Player pl : Bukkit.getOnlinePlayers()){
                                pl.setMaxHealth(20);
                                for(PotionEffect effect : pl.getActivePotionEffects()) {     pl.removePotionEffect(effect.getType()); }
                                pl.setGameMode(GameMode.SPECTATOR);
                                pl.sendMessage(ChatColor.RED+""+ChatColor.BOLD+"==========");
                                pl.sendMessage(ChatColor.RED+""+ChatColor.BOLD+"|| ");
                                pl.sendMessage(ChatColor.RED+""+ChatColor.BOLD+"|| "+ChatColor.GREEN+"WINNING TEAM: ");
                                pl.sendMessage(ChatColor.RED+""+ChatColor.BOLD+"|| ");
                                pl.sendMessage(ChatColor.RED+""+ChatColor.BOLD+"|| "+returnWinners.getWinningTeam()+ChatColor.GREEN+"!");
                                pl.sendMessage(ChatColor.RED+""+ChatColor.BOLD+"|| ");
                                pl.sendMessage(ChatColor.RED+""+ChatColor.BOLD+"==========");
                                pl.sendMessage(messageStandards.pluginPrefixMessage()+ChatColor.DARK_AQUA+""+ChatColor.BOLD+"The server will restart shortly. ");
                                sendTitle.sendTitle(pl, 10, 180, 10, (ChatColor.BOLD+""+returnWinners.getWinningTeam()), (ChatColor.GOLD+""+ChatColor.BOLD+"WINNERS"));
                            }
                            ConsoleCommandSender cs = Bukkit.getConsoleSender();
                            cs.sendMessage(ChatColor.RED+""+ChatColor.BOLD+"==========");
                            cs.sendMessage(ChatColor.RED+""+ChatColor.BOLD+"|| ");
                            cs.sendMessage(ChatColor.RED+""+ChatColor.BOLD+"|| "+ChatColor.GREEN+"WINNING TEAM: ");
                            cs.sendMessage(ChatColor.RED+""+ChatColor.BOLD+"|| ");
                            cs.sendMessage(ChatColor.RED+""+ChatColor.BOLD+"|| "+returnWinners.getWinningTeam()+ChatColor.GREEN+"!");
                            cs.sendMessage(ChatColor.RED+""+ChatColor.BOLD+"|| ");
                            cs.sendMessage(ChatColor.RED+""+ChatColor.BOLD+"==========");
                        }
                        if(Bukkit.getOnlinePlayers().size()>=1) {
                            for (Player pl : Bukkit.getOnlinePlayers()) {
                                // pl.sendMessage(messageStandards.pluginPrefixMessage() + ChatColor.AQUA + endgameTicks + ChatColor.GREEN + " seconds until the server restarts!");
                                sendActionBar.sendTitle(pl, ChatColor.GRAY+">>> "+ChatColor.WHITE+""+ChatColor.BOLD+"WINNING TEAM: "+returnWinners.getWinningTeam());
                            }
                        }
                    }
                    Bukkit.getServer().getLogger().info("[CoreDefenderTimer] "+endgameTicks+ " seconds until the server reloads.");
                    endgameTicks--;
                }
            }
        }, 20l, 20l);
    }

    public void regPlayerRespawnTimer(){
        BukkitScheduler Scheduler = Bukkit.getScheduler();
        Scheduler.scheduleSyncRepeatingTask(this, new Runnable() {
            @Override
            public void run() {
                if (GameState.getState() == GameState.INGAME) {
                    for (Player pl : Bukkit.getOnlinePlayers()) {
                        if (playerRespawnTimes.containsKey(pl)) {
                            boolean isKicked = false;
                            if (playerRespawnTimes.get(pl) == 1) {
                                playerRespawnTimes.remove(pl);
                                pl.setGameMode(GameMode.SURVIVAL);
                                pl.getInventory().clear();
                                kitManager.givePlayerKit(pl);
                                // givePlayerKits.giveKit(pl);
                                pl.sendMessage(messageStandards.pluginPlayerInputPrefix() + ChatColor.GREEN + "You have respawned at your team's base!");
                                if (redTeam.contains(pl)) {
                                    pl.teleport(redSpawnExact);
                                } else if (blueTeam.contains(pl)) {
                                    pl.teleport(blueSpawnExact);
                                } else {
                                    pl.kickPlayer(messageStandards.alertPrefix() + ChatColor.RED + "Internal error, please contact an admin. \nAsk them to check the server logs.");
                                    isKicked = true;
                                    Bukkit.getServer().getLogger().severe("!!! Player " + pl.getName() + " was placed on respawn cooldown with no assigned team. Kicked for plugin stability. ");
                                }
                            } else {
                                int respTimeLocal = playerRespawnTimes.get(pl) - 1;
                                playerRespawnTimes.remove(pl);
                                playerRespawnTimes.put(pl, respTimeLocal);
                            }
                            try {
                                if (playerRespawnTimes.containsKey(pl) && isKicked == false && playerRespawnTimes.get(pl) == 5 || playerRespawnTimes.get(pl) == 10 || playerRespawnTimes.get(pl) <= 3) {
                                    pl.sendMessage(messageStandards.pluginPlayerInputPrefix() + ChatColor.GOLD + "You'll respawn in " + ChatColor.AQUA + playerRespawnTimes.get(pl) + ChatColor.GOLD + " seconds!");
                                }
                            } catch (NullPointerException e){

                            }
                        }
                    }
                }
            }
        }, 20l, 20l);
    }

    public void registerPluginInventories(){
    }
    public void loadConfig(){
        saveDefaultConfig();
        getConfig().options().copyDefaults(true);
        saveConfig();
    }

    public void registerExternalSchedulers(){

        BukkitScheduler sched = Bukkit.getScheduler();
        sched.scheduleSyncRepeatingTask(this, new scoreboardManager(), 20l, 3l);

    }

    public void registerPluginListeners(){

        Bukkit.getServer().getPluginManager().registerEvents(new playerJoin(), this);
        Bukkit.getServer().getPluginManager().registerEvents(new playerQuitOrKick(), this);
        Bukkit.getServer().getPluginManager().registerEvents(new coreManager(), this);
        Bukkit.getServer().getPluginManager().registerEvents(new breakBlockEvent(), this);
        Bukkit.getServer().getPluginManager().registerEvents(new playerDeathEvt(), this);
        Bukkit.getServer().getPluginManager().registerEvents(new onExplodeCancelDamage(), this);
        Bukkit.getServer().getPluginManager().registerEvents(new blockPlaceEvent(), this);
        Bukkit.getServer().getPluginManager().registerEvents(new itemPickupEvent(), this);
        Bukkit.getServer().getPluginManager().registerEvents(new cancelInventoryMovements(), this);
        Bukkit.getServer().getPluginManager().registerEvents(new cancelItemDrop(), this);
        Bukkit.getServer().getPluginManager().registerEvents(new preventTeamKilling(), this);
        Bukkit.getServer().getPluginManager().registerEvents(new serverPing(), this);
        Bukkit.getServer().getPluginManager().registerEvents(new playerChatEvent(), this);
        Bukkit.getServer().getPluginManager().registerEvents(new preventLobbyKilling(), this);
        Bukkit.getServer().getPluginManager().registerEvents(new warmupKitSelectionRightClick(), this);

        Bukkit.getScheduler().scheduleSyncRepeatingTask(this, new getTPS(), 100l, 1l);

        // Register cancelled commands.

        Bukkit.getServer().getPluginManager().registerEvents(new reloadOverride(), this);
        Bukkit.getServer().getPluginManager().registerEvents(new blockBack(), this);

        // Register Kit Manager.
        Bukkit.getServer().getPluginManager().registerEvents(new kitManager(), this);

    }
    public void registerPluginCommands(){
        getCommand("nextstate").setExecutor(new nextState());
        getCommand("kit").setExecutor(new kitCommand());
        getCommand("forceticks").setExecutor(new forceIngameTicks());
        getCommand("vote").setExecutor(new votecommand());
    }
    public void registerProtLibInjects(){

        protLibSigns.registerSignIntercepts();
        setServerPingPlayerMsg.registerPingIntercept();

    }

    public void configureWorldSettings(){

        Main.mainWorld.setGameRuleValue("doDaylightCycle", "false");
        Main.mainWorld.setGameRuleValue("doNaturalRegeneration", "true");
        Main.mainWorld.setGameRuleValue("naturalRegeneration", "true");
        Main.mainWorld.setTime(10000);

    }

    @Override
    public void onEnable(){
        protocolManager = ProtocolLibrary.getProtocolManager();
        registerProtLibInjects();
        registerPluginListeners();
        registerPluginCommands();
        registerExternalSchedulers();
        registerPluginInventories();
        registerConsoleAnalytics();
        registerLobbyAlerts();
        loopPlayerHunger();
        loadConfig();
        if(getConfig().getInt("firstRun") == 1) {

            ConsoleCommandSender cs = Bukkit.getServer().getConsoleSender();

            cs.sendMessage(ChatColor.GREEN + "[ Core Defender ] Hey there! ");
            cs.sendMessage(ChatColor.GREEN + "[ Core Defender ] This appears to be your first time running Core Defender.");
            cs.sendMessage(ChatColor.GREEN + "[ Core Defender ] Please edit the \"FirstRun\" property in your config file once you've filled the appropriate settings.");
            cs.sendMessage(ChatColor.GREEN + "[ Core Defender ] Then, the plugin shall launch!");
            cs.sendMessage(ChatColor.GREEN + "[ Core Defender ] The plugin will not start until you've done so. :)");
            cs.sendMessage(ChatColor.GREEN + "[ Core Defender ] The plugin is now terminating. The default config has been copied to your server directory.");
            cs.sendMessage(ChatColor.GREEN + "[ Core Defender ] The server is still running, should you wish to tweak the configuration now. ");
            cs.sendMessage(ChatColor.GREEN + "[ Core Defender ] Edit that, and restart (do NOT /reload) the server. ");
            cs.sendMessage(ChatColor.GOLD + "(Trust me, I tried using /reload. It did NOT go well. ;)");
            cs.sendMessage(ChatColor.GREEN + "[ Core Defender ] Having issues? Contact Yellow directly. :)");
            Bukkit.getPluginManager().disablePlugin(this);

        } else if(getConfig().getInt("firstRun") != 1) {

            lobbyWorld = Bukkit.createWorld(new WorldCreator(getConfig().getString("lobbyworlddata.lobbyworldname")));
            mainWorld = lobbyWorld;
            if(!(getConfig().getDouble("lobbyworlddata.spawnpoint.y") == 0)) {
                lobbySpawnLocExact = new Location(lobbyWorld, getConfig().getDouble("lobbyworlddata.spawnpoint.x"), getConfig().getDouble("lobbyworlddata.spawnpoint.y"), getConfig().getDouble("lobbyworlddata.spawnpoint.z"));
            } else {
                lobbySpawnLocExact = lobbyWorld.getSpawnLocation();
                lobbySpawnLocExact.setY(lobbyWorld.getHighestBlockYAt(lobbySpawnLocExact));
            }
            configureWorldSettings();

            if(getConfig().getBoolean("use_map_voting")){
                Set<String> sections = getConfig().getConfigurationSection("maps").getKeys(false);
                ArrayList<MapInstance> maps = new ArrayList<>();
                for(String s : sections){
                    ConfigurationSection configSection = getConfig().getConfigurationSection("maps."+s);
                    MapInstance map = new MapInstance(configSection.getString("mapfoldername"), configSection.getString("name"), configSection);
                    maps.add(map);
                }

                if(maps.size() <= 4){
                    int i = 0;
                    for(MapInstance m : maps){
                        mapsToVoteOn.put(i, m);
                        i++;
                    }
                } else {
                    for(int i = 0; i <= 4; i++){
                        Random rand = new Random();
                        int x = rand.nextInt(maps.size());
                        MapInstance instance = maps.get(x);
                        mapsToVoteOn.put(i, instance);
                        maps.remove(x);
                    }
                }

                Bukkit.getServer().getLogger().info("[ Core Defender ] The following maps for voting were selected: ");
                for(MapInstance i : mapsToVoteOn.values()){
                    Bukkit.getServer().getLogger().info("[ Core Defender ] "+i.getPublicTitle());
                }
            } else {
                Bukkit.getServer().getLogger().info("[ Core Defender ] Lobby voting is disabled. ");
            }

            lobbyRunning = true;
            lobbyPluginTicks();
            loadingmapTicks();
            WarmupPluginTicks();
            inGameTicks();
            endGamePluginTicks();
            regPlayerRespawnTimer();
            GameState.setState(GameState.LOBBY);
            logGameState.logState();
            kitManager.instantiateKits();
            kitManager.configureKitMenu();
            if(getConfig().getInt("sqlenabled") == 1) {
                sql = new sqlmanager();
                if(sql.openConnection(getConfig())) {
                    Bukkit.getServer().getLogger().info("[ Core Defender ] Database connection established! ");
                }
            } else {
                Bukkit.getServer().getLogger().info("[ Core Defender ] Database usage is disabled. To enable/configure this, edit the configuration. ");
            }

        }
    }

    public void deleteFile(File f){
        if(!f.exists()) return;
        File[] subfiles = f.listFiles();
        for(int i = 0; i < subfiles.length; i++){
            if(subfiles[i].isDirectory()) {
                deleteFile(subfiles[i]);
            } else {
                subfiles[i].delete();
            }
        }
        f.delete();
    }

    @Override
    public void onDisable(){
        GameState.setState(GameState.ENDGAME);
        Bukkit.unloadWorld(mainWorld, false);
        for (Player pl: Bukkit.getOnlinePlayers()){
            pl.kickPlayer(ChatColor.RED+""+ChatColor.BOLD+">> "+ChatColor.AQUA+"Thank you for playing, "+pl.getName()+"!\n The game has ENDED!");
        }
        if(getConfig().getInt("sqlenabled") == 1) {
            sql.terminateConnection();
            Bukkit.getServer().getLogger().info("[ Core Defender ] Database connection terminated. ");
        }
        logGameState.logState();
        spawnLocations = null;
        teamCores = null;
        playersOnline = null;
        blueTeam = null;
        redTeam = null;
        playerKits = null;
        playerRespawnTimes = null;
        /*if(mainWorld != null) {
            File folderOfWorld = mainWorld.getWorldFolder();
            if (Bukkit.unloadWorld(mainWorld, false)) {
                deleteFile(folderOfWorld);

                Bukkit.getServer().getLogger().info("[ Core Defender ] World folder will be deleted. ");

            } else {
                Bukkit.getServer().getLogger().info("[ Core Defender ] The world could not be deleted! ");
            }
        }*/

    }
}
