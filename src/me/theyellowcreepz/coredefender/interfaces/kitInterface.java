package me.theyellowcreepz.coredefender.interfaces;

import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;

import java.util.ArrayList;

/**
 * All code inside this plugin is the
 * property of Andrew Petersen.
 * Decompiling, copying, or claiming any
 * code from this plugin as your own
 * voids any legal rights you have to
 * this plugin, unless express permission
 * is given.
 */
public interface kitInterface {

    ItemStack getDisplayItem();

    String getKitTitle();

    boolean hasListeners();

    int getKitID();

    ArrayList<ItemStack> getKitItems();

    ItemStack getHelmet();
    ItemStack getChestplate();
    ItemStack getLeggings();
    ItemStack getBoots();

    ArrayList<PotionEffect> getPotionEffects();

    int getPlayerMaxHealth();
}
