package me.theyellowcreepz.coredefender.scoreboards;

import me.theyellowcreepz.coredefender.GameState;
import me.theyellowcreepz.coredefender.Main;
import me.theyellowcreepz.coredefender.interfaces.kitInterface;
import me.theyellowcreepz.coredefender.inventories.kitManager;
import me.theyellowcreepz.coredefender.messageStandards;
import me.theyellowcreepz.coredefender.primaryParts.coreManager;
import me.theyellowcreepz.coredefender.returnWinners;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.*;

/**
 * All code inside this plugin is the
 * property of Andrew Petersen.
 * Decompiling, copying, or claiming any
 * code from this plugin as your own
 * voids any legal rights you have to
 * this plugin, unless express permission
 * is given.
 */
public class scoreboardManager implements Runnable {

    public String getPlayerTeam(Player pl){

        if(Main.redTeam.contains(pl)){
            return (ChatColor.RED+""+ChatColor.BOLD+"Red Team");
        } else if(Main.blueTeam.contains(pl)){
            return (ChatColor.AQUA+""+ChatColor.BOLD+"Blue Team");
        } else if(Main.spectatorTeam.contains(pl)){
            return (ChatColor.GRAY+""+ChatColor.BOLD+"Spectator");
        } else {
            return null;
        }
    }

    public String getPlayerKit(Player pl){
        if(kitManager.playerKits.containsKey(pl)){
            kitInterface playerKit = kitManager.kitIdReference.get(kitManager.playerKits.get(pl));
            return playerKit.getKitTitle();
        } else {
            return (ChatColor.GOLD+""+ChatColor.BOLD+"None");
        }
    }


    @Override
    public void run() {
        ScoreboardManager m = Bukkit.getScoreboardManager();
        for(Player pl : Bukkit.getOnlinePlayers()) {

            Scoreboard sb = m.getNewScoreboard();

            if (GameState.getState().equals(GameState.LOBBY)) {

                // Lobby scoreboard.
                Objective objt = sb.registerNewObjective("Scoreboard", "Siedboard"); // Criterias.PLAYER_KILLS
                objt.setDisplaySlot(DisplaySlot.SIDEBAR);
                objt.setDisplayName(ChatColor.DARK_GRAY+ "[ " + ChatColor.BOLD +ChatColor.GREEN + "CORE DEFENDER "+ ChatColor.RESET+ChatColor.DARK_GRAY+"]");

                Score placeholder3 = objt.getScore(ChatColor.RESET.toString());
                placeholder3.setScore(16);

                Score yourNamePref = objt.getScore(ChatColor.DARK_AQUA+""+ChatColor.BOLD+"Your IGN: ");
                yourNamePref.setScore(15);

                Score yourName = objt.getScore(ChatColor.GOLD+pl.getName());
                yourName.setScore(14);

                Score placeholder5 = objt.getScore(ChatColor.RESET.toString()+ChatColor.RESET.toString()+ChatColor.RESET.toString()+ChatColor.RESET.toString()+ChatColor.RESET.toString()+ChatColor.GRAY+"----------");
                placeholder5.setScore(13);

                Score currentStatePref = objt.getScore(ChatColor.DARK_AQUA+""+ChatColor.BOLD+"Current Game State: ");
                currentStatePref.setScore(12);

                Score currentState = objt.getScore(messageStandards.getGameStateColor()+""+GameState.getState());
                currentState.setScore(11);

                Score placeholder1 = objt.getScore(ChatColor.RESET.toString()+ChatColor.RESET.toString()+ChatColor.GRAY+"----------");
                placeholder1.setScore(10);

                Score timeRemPref = objt.getScore(ChatColor.DARK_AQUA+""+ChatColor.BOLD+"Time until Start: ");
                timeRemPref.setScore(9);

                Score timeRem = objt.getScore(ChatColor.GOLD+ "" + Main.lobbyTicks+" seconds");
                timeRem.setScore(8);

                Score placeholder2 = objt.getScore(ChatColor.RESET.toString()+ChatColor.RESET.toString()+ChatColor.RESET.toString()+ChatColor.GRAY+"----------");
                placeholder2.setScore(7);

                Score playersReqPref = objt.getScore(ChatColor.DARK_AQUA+""+ChatColor.BOLD+"Players Required: ");
                playersReqPref.setScore(6);

                Score playersRequired = objt.getScore(ChatColor.GOLD+"4");
                playersRequired.setScore(5);

                Score placeholder4 = objt.getScore(ChatColor.RESET.toString()+ChatColor.RESET.toString()+ChatColor.RESET.toString()+ChatColor.RESET.toString()+ChatColor.GRAY+"----------");
                placeholder4.setScore(4);

                Score playersOnPref = objt.getScore(ChatColor.DARK_AQUA+""+ChatColor.BOLD+"Players Online: ");
                playersOnPref.setScore(3);

                Score playersOn = objt.getScore(ChatColor.GOLD+""+Bukkit.getOnlinePlayers().size());
                playersOn.setScore(2);

            } else if (GameState.getState().equals(GameState.WARMUP)) {

                // Warmup scoreboard.
                Objective objt = sb.registerNewObjective("Scoreboard", "Siedboard"); // Criterias.PLAYER_KILLS
                objt.setDisplaySlot(DisplaySlot.SIDEBAR);
                objt.setDisplayName(ChatColor.DARK_GRAY+ "[ " + ChatColor.BOLD +ChatColor.GREEN + "CORE DEFENDER "+ ChatColor.RESET+ChatColor.DARK_GRAY+"]");

                Score placeholder3 = objt.getScore(ChatColor.RESET.toString());
                placeholder3.setScore(16);

                Score yourNamePref = objt.getScore(ChatColor.DARK_AQUA+""+ChatColor.BOLD+"Your IGN: ");
                yourNamePref.setScore(15);

                Score yourName = objt.getScore(ChatColor.GOLD+pl.getName());
                yourName.setScore(14);

                Score placeholder5 = objt.getScore(ChatColor.RESET.toString()+ChatColor.RESET.toString()+ChatColor.RESET.toString()+ChatColor.RESET.toString()+ChatColor.RESET.toString()+ChatColor.GRAY+"----------");
                placeholder5.setScore(13);

                Score currentStatePref = objt.getScore(ChatColor.DARK_AQUA+""+ChatColor.BOLD+"Current Game State: ");
                currentStatePref.setScore(12);

                Score currentState = objt.getScore(messageStandards.getGameStateColor()+""+GameState.getState());
                currentState.setScore(11);

                Score placeholder1 = objt.getScore(ChatColor.RESET.toString()+ChatColor.RESET.toString()+ChatColor.GRAY+"----------");
                placeholder1.setScore(10);

                Score timeRemPref = objt.getScore(ChatColor.DARK_AQUA+""+ChatColor.BOLD+"Time until Start: ");
                timeRemPref.setScore(9);

                Score timeRem = objt.getScore(ChatColor.GOLD+ "" + Main.warmupTicks+" seconds");
                timeRem.setScore(8);

                Score placeholder2 = objt.getScore(ChatColor.RESET.toString()+ChatColor.RESET.toString()+ChatColor.RESET.toString()+ChatColor.GRAY+"----------");
                placeholder2.setScore(7);

                Score plTeamTitle = objt.getScore(ChatColor.DARK_AQUA+""+ChatColor.BOLD+"Your Team: ");
                plTeamTitle.setScore(6);

                Score plTeam = objt.getScore(getPlayerTeam(pl));
                plTeam.setScore(5);

                Score placeholder6 = objt.getScore(ChatColor.RESET.toString()+ChatColor.RESET.toString()+ChatColor.RESET.toString()+ChatColor.RESET.toString()+ChatColor.RESET.toString()+ChatColor.RESET.toString()+ChatColor.GRAY+"----------");
                placeholder6.setScore(4);

                Score plKitPref = objt.getScore(ChatColor.DARK_AQUA+""+ChatColor.BOLD+"Your Kit: ");
                plKitPref.setScore(3);

                Score plKit = objt.getScore(getPlayerKit(pl));
                plKit.setScore(2);

            } else if (GameState.getState().equals(GameState.INGAME)) {

                // Ingame scoreboard.
                Objective objt = sb.registerNewObjective("Scoreboard", "Siedboard"); // Criterias.PLAYER_KILLS
                objt.setDisplaySlot(DisplaySlot.SIDEBAR);
                objt.setDisplayName(ChatColor.DARK_GRAY + "[ " + ChatColor.BOLD + ChatColor.GREEN + "CORE DEFENDER " + ChatColor.RESET + ChatColor.DARK_GRAY + "]");

                Score placeholder3 = objt.getScore(ChatColor.RESET.toString());
                placeholder3.setScore(20);

                Score yourNamePref = objt.getScore(ChatColor.DARK_AQUA+""+ChatColor.BOLD+"Your IGN: ");
                yourNamePref.setScore(19);

                Score yourName = objt.getScore(ChatColor.GOLD+pl.getName());
                yourName.setScore(18);

                Score placeholder5 = objt.getScore(ChatColor.RESET.toString()+ChatColor.RESET.toString()+ChatColor.RESET.toString()+ChatColor.RESET.toString()+ChatColor.RESET.toString()+ChatColor.GRAY+"----------");
                placeholder5.setScore(17);

                Score currentStatePref = objt.getScore(ChatColor.DARK_AQUA+""+ChatColor.BOLD+"Current Game State: ");
                currentStatePref.setScore(16);

                Score currentState = objt.getScore(messageStandards.getGameStateColor()+""+GameState.getState());
                currentState.setScore(15);

                Score placeholder1 = objt.getScore(ChatColor.RESET.toString()+ChatColor.RESET.toString()+ChatColor.GRAY+"----------");
                placeholder1.setScore(14);

                Score timeRemPref = objt.getScore(ChatColor.DARK_AQUA+""+ChatColor.BOLD+"Time Remaining: ");
                timeRemPref.setScore(13);

                Score timeRem = objt.getScore(ChatColor.GOLD+Main.ingameTimeIndicator);
                timeRem.setScore(12);

                Score placeholder2 = objt.getScore(ChatColor.RESET.toString()+ChatColor.RESET.toString()+ChatColor.RESET.toString()+ChatColor.GRAY+"----------");
                placeholder2.setScore(11);

                Score plTeamTitle = objt.getScore(ChatColor.DARK_AQUA+""+ChatColor.BOLD+"Your Team: "+getPlayerTeam(pl));
                plTeamTitle.setScore(10);

                Score placeholder4 = objt.getScore(ChatColor.RESET.toString()+ChatColor.RESET.toString()+ChatColor.RESET.toString()+ChatColor.RESET.toString()+ChatColor.GRAY+"----------");
                placeholder4.setScore(8);

                Score redHealth = objt.getScore(ChatColor.RED+""+ChatColor.BOLD+"Red Core Health: "+ChatColor.GOLD+ "" + coreManager.redCoreHealth+" HP");
                redHealth.setScore(7);

                Score blueHealth = objt.getScore(ChatColor.AQUA+""+ChatColor.BOLD+"Blue Core Health: "+ChatColor.GOLD+ "" + coreManager.blueCoreHealth+" HP");
                blueHealth.setScore(4);


            } else if (GameState.getState().equals(GameState.ENDGAME)) {

                // Endgame scoreboard.
                Objective objt = sb.registerNewObjective("Scoreboard", "Siedboard"); // Criterias.PLAYER_KILLS
                objt.setDisplaySlot(DisplaySlot.SIDEBAR);
                objt.setDisplayName(ChatColor.DARK_GRAY + "[ " + ChatColor.BOLD + ChatColor.GREEN + "CORE DEFENDER " + ChatColor.RESET + ChatColor.DARK_GRAY + "]");

                Score placeholder3 = objt.getScore(ChatColor.RESET.toString());
                placeholder3.setScore(20);

                Score yourNamePref = objt.getScore(ChatColor.DARK_AQUA+""+ChatColor.BOLD+"Your IGN: ");
                yourNamePref.setScore(19);

                Score yourName = objt.getScore(ChatColor.GOLD+pl.getName());
                yourName.setScore(18);

                Score placeholder5 = objt.getScore(ChatColor.RESET.toString()+ChatColor.RESET.toString()+ChatColor.RESET.toString()+ChatColor.RESET.toString()+ChatColor.RESET.toString()+ChatColor.GRAY+"----------");
                placeholder5.setScore(17);

                Score currentStatePref = objt.getScore(ChatColor.DARK_AQUA+""+ChatColor.BOLD+"Current Game State: ");
                currentStatePref.setScore(16);

                Score currentState = objt.getScore(messageStandards.getGameStateColor()+""+GameState.getState());
                currentState.setScore(15);

                Score placeholder1 = objt.getScore(ChatColor.RESET.toString()+ChatColor.RESET.toString()+ChatColor.GRAY+"----------");
                placeholder1.setScore(14);

                Score winningTeamPref = objt.getScore(ChatColor.DARK_AQUA+""+ChatColor.BOLD+"Winning Team: ");
                winningTeamPref.setScore(13);

                Score winningTeam = objt.getScore(ChatColor.BOLD+""+ returnWinners.getWinningTeam());
                winningTeam.setScore(12);

                /*
                Score placeholder4 = objt.getScore(ChatColor.RESET.toString()+ChatColor.RESET.toString()+ChatColor.RESET.toString()+ChatColor.GRAY+"----------");
                placeholder4.setScore(11);

                Score timeUntilRestartPref = objt.getScore(ChatColor.BOLD+""+ChatColor.DARK_AQUA+"Time until Restart: ");
                timeUntilRestartPref.setScore(10);

                Score timeUntilRestart = objt.getScore(ChatColor.GOLD+""+Main.endgameTicks+" seconds");
                timeUntilRestart.setScore(9);
                */

            }

            if(!sb.getEntries().equals(pl.getScoreboard().getEntries())) { // To reduce flickering.
                pl.setScoreboard(sb);
            }
        }
    }
}
