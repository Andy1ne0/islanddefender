package me.theyellowcreepz.coredefender;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import java.util.Map;

/**
 * All code inside this plugin is the
 * property of Andrew Petersen.
 * Decompiling, copying, or claiming any
 * code from this plugin as your own
 * voids any legal rights you have to
 * this plugin, unless express permission
 * is given.
 */
public class messageStandards {

    // TODO need more consistency throughout plugin messages.

    private static ChatColor ReturningColor;

    public static ChatColor getGameStateColor(){
        switch (GameState.getState()){
            case LOBBY:
                ReturningColor = ChatColor.GOLD;
                break;
            case WARMUP:
                ReturningColor = ChatColor.DARK_AQUA;
                break;
            case INGAME:
                ReturningColor = ChatColor.GREEN;
                break;
            case ENDGAME:
                ReturningColor = ChatColor.GRAY;
                break;
            case RESTART:
                ReturningColor = null;
            break;
            default:
                ReturningColor = null;
                break;
        }
    return ReturningColor;
    }

    public static final String pluginPrefixMessage(){

        return (ChatColor.DARK_GRAY + "[" + ChatColor.RESET + ChatColor.DARK_AQUA + ChatColor.BOLD+ "Core Defender" + ChatColor.RESET+ChatColor.DARK_GRAY+"] ");

    }

    public static final String pluginPlayerInputPrefix(){
        return (ChatColor.DARK_GREEN+""+ChatColor.BOLD+">>> "+ChatColor.GREEN);
    }
    public static final String rightClickText(){
        return (ChatColor.RESET+""+ChatColor.GRAY+ChatColor.ITALIC+"(Right-click)");
    }
    public static final String alertPrefix(){
        return (ChatColor.DARK_RED+""+ChatColor.BOLD+">>> "+ChatColor.RED);
    }
    public static final String grayArrows(){
        return (ChatColor.GRAY+""+ChatColor.BOLD+">>> ");
    }
    public static final String kitCooldownMsg(Player pl, Map map){
        return (alertPrefix()+ChatColor.RESET+ChatColor.RED+ChatColor.ITALIC+"You can't do that for another "+ChatColor.GOLD+map.get(pl)+ChatColor.RED+ChatColor.ITALIC+" seconds!");
    }
    public static final String questionPref(){
        return (ChatColor.DARK_GRAY+""+ChatColor.BOLD+"["+ChatColor.DARK_AQUA+"???"+ChatColor.DARK_GRAY+"] "+ChatColor.RESET+ChatColor.LIGHT_PURPLE);
    }
    public static final String kitUsableAgain(){

        return (messageStandards.grayArrows()+ChatColor.RESET+ChatColor.DARK_AQUA+"You can now use your kit's ability again!");
    }
    public static final String opOpsPref(){
        return (ChatColor.BOLD+""+ChatColor.LIGHT_PURPLE+">>> ");
    }

}
