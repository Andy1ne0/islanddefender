package me.theyellowcreepz.coredefender.util;

import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.entity.FallingBlock;

import java.util.List;

/**
 * All code inside this plugin is the
 * property of Andrew Petersen.
 * Decompiling, copying, or claiming any
 * code from this plugin as your own
 * voids any legal rights you have to
 * this plugin, unless express permission
 * is given.
 */
public class entitiesWithinRadius {

    public static boolean isInBorder(Location center, Location notCenter, int range) {
        int x = center.getBlockX(), z = center.getBlockZ();
        int x1 = notCenter.getBlockX(), z1 = notCenter.getBlockZ();

        if (x1 >= (x + range) || z1 >= (z + range) || x1 <= (x - range) || z1 <= (z - range)) {
            return false;
        }
        return true;
    }


    public static List<Entity> getNearbyEntities(Location loc, double x, double y, double z) {
        FallingBlock ent = loc.getWorld().spawnFallingBlock(loc, 132, (byte) 0);
        List<Entity> out = ent.getNearbyEntities(x, y, z);
        ent.remove();

        return out;
    }



}
