package me.theyellowcreepz.coredefender.util;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.events.PacketContainer;
import com.comphenix.protocol.wrappers.EnumWrappers;
import com.comphenix.protocol.wrappers.WrappedChatComponent;
import me.theyellowcreepz.coredefender.Main;
import me.theyellowcreepz.coredefender.util.packetwrappers.titleWrapper;
import org.bukkit.entity.Player;

import java.lang.reflect.InvocationTargetException;

/**
 * All code inside this plugin is the
 * property of Andrew Petersen.
 * Decompiling, copying, or claiming any
 * code from this plugin as your own
 * voids any legal rights you have to
 * this plugin, unless express permission
 * is given.
 */
public class sendTitle {

    public static void sendTitle(Player pl, int fadeIn, int timeShown, int fadeOut, String mainMessage, String subtitleMessage){

        if(mainMessage == null){
            mainMessage = " ";
        }

        if(subtitleMessage == null){
            subtitleMessage = " ";
        }
        // START experimental time packet

        PacketContainer titleTimerPacket = new PacketContainer(PacketType.Play.Server.TITLE);
        titleWrapper timerWrapped = new titleWrapper(titleTimerPacket);
        timerWrapped.setAction(EnumWrappers.TitleAction.TIMES);
        timerWrapped.setFadeIn(fadeIn);
        timerWrapped.setStay(timeShown);
        timerWrapped.setFadeOut(fadeOut);
        try {
            Main.protocolManager.sendServerPacket(pl, titleTimerPacket);
        } catch (InvocationTargetException e){
            e.printStackTrace();
        }

        // END experimental time packet

        PacketContainer title = new PacketContainer(PacketType.Play.Server.TITLE);
        titleWrapper wrapped = new titleWrapper(title);
        wrapped.setFadeIn(fadeIn);
        wrapped.setStay(timeShown);
        wrapped.setFadeOut(fadeOut);
        wrapped.setTitle(WrappedChatComponent.fromText(mainMessage));
        wrapped.setAction(EnumWrappers.TitleAction.TITLE);

        try {
            Main.protocolManager.sendServerPacket(pl, title);
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }

        PacketContainer subtitle = new PacketContainer(PacketType.Play.Server.TITLE);
        titleWrapper wrappedSub = new titleWrapper(subtitle);
        wrappedSub.setFadeIn(fadeIn);
        wrappedSub.setStay(timeShown);
        wrappedSub.setFadeOut(fadeOut);
        wrappedSub.setTitle(WrappedChatComponent.fromText(subtitleMessage));
        wrappedSub.setAction(EnumWrappers.TitleAction.SUBTITLE);

        try {
            Main.protocolManager.sendServerPacket(pl, subtitle);
        } catch (InvocationTargetException e){
            e.printStackTrace();
        }

    }

    public static void resetTitle(Player pl){
        PacketContainer resetPacket = new PacketContainer(PacketType.Play.Server.TITLE);
        titleWrapper wrappedPack = new titleWrapper(resetPacket);
        wrappedPack.setAction(EnumWrappers.TitleAction.CLEAR);
        try {
            Main.protocolManager.sendServerPacket(pl, resetPacket);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
