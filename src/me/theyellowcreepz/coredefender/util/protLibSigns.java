package me.theyellowcreepz.coredefender.util;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.events.ListenerPriority;
import com.comphenix.protocol.events.PacketAdapter;
import com.comphenix.protocol.events.PacketEvent;
import com.comphenix.protocol.wrappers.BlockPosition;
import me.theyellowcreepz.coredefender.Main;
import me.theyellowcreepz.coredefender.util.packetwrappers.signUpdatePacketWrapper;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitScheduler;

/**
 * All code inside this plugin is the
 * property of Andrew Petersen.
 * Decompiling, copying, or claiming any
 * code from this plugin as your own
 * voids any legal rights you have to
 * this plugin, unless express permission
 * is given.
 */
public class protLibSigns {



    public static void registerSignIntercepts() {

        Main.protocolManager.addPacketListener(new PacketAdapter(Main.getInstance(), ListenerPriority.NORMAL, PacketType.Play.Server.UPDATE_SIGN) {
            @Override
            public void onPacketSending(PacketEvent event) {
                        event.setCancelled(true);
                signUpdatePacketWrapper sign = new signUpdatePacketWrapper(event.getPacket());
                BlockPosition signLoc = sign.getLocation();
                final Location loc = new Location(Main.mainWorld, signLoc.getX(), signLoc.getY(), signLoc.getZ());
                final Player pl = event.getPlayer();
                event.getPlayer().sendBlockChange(loc, Material.AIR, (byte) 0);
                BukkitScheduler sched = Bukkit.getScheduler();
                sched.runTaskLater(Main.getInstance(), new Runnable() {
                    @Override
                    public void run() {
                        pl.sendBlockChange(loc, Material.STONE, (byte) 0);
                        pl.sendBlockChange(loc, Material.AIR, (byte) 0);
                    }
                }, 20l);
            }
        });
    }
}