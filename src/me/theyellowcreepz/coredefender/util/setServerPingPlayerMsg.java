package me.theyellowcreepz.coredefender.util;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.events.PacketAdapter;
import com.comphenix.protocol.events.PacketEvent;
import com.comphenix.protocol.wrappers.WrappedServerPing;
import me.theyellowcreepz.coredefender.Main;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;

/**
 * All code inside this plugin is the
 * property of Andrew Petersen.
 * Decompiling, copying, or claiming any
 * code from this plugin as your own
 * voids any legal rights you have to
 * this plugin, unless express permission
 * is given.
 */
public class setServerPingPlayerMsg {

    public static void registerPingIntercept(){
        Main.protocolManager.addPacketListener(new PacketAdapter(PacketAdapter.params(Main.getInstance(), PacketType.Status.Server.OUT_SERVER_INFO).optionAsync()) {
            @Override
            public void onPacketSending(PacketEvent event) {
                WrappedServerPing ping = event.getPacket().getServerPings().read(0);
                
                ping.setVersionName(ChatColor.GOLD+"Players online: "+ ChatColor.AQUA+Bukkit.getOnlinePlayers().size()+ChatColor.GOLD+" / "+ChatColor.AQUA+Main.maximumPlayers + " "+ChatColor.DARK_GREEN+"["+ChatColor.GREEN+ChatColor.BOLD+"1.8"+ ChatColor.RESET+ ChatColor.DARK_GREEN+"]");
                ping.setPlayersVisible(false);
                ping.setVersionProtocol(1337);
            }
        });
    }

}
