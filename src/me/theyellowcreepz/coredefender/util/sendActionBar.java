package me.theyellowcreepz.coredefender.util;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.events.PacketContainer;
import com.comphenix.protocol.wrappers.WrappedChatComponent;
import me.theyellowcreepz.coredefender.Main;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.lang.reflect.InvocationTargetException;

/**
 * All code inside this plugin is the
 * property of Andrew Petersen.
 * Decompiling, copying, or claiming any
 * code from this plugin as your own
 * voids any legal rights you have to
 * this plugin, unless express permission
 * is given.
 */
public class sendActionBar {

    public static void sendTitle(Player pl, String message){

        PacketContainer chat = new PacketContainer(PacketType.Play.Server.CHAT);
        chat.getBytes().write(0, (byte)2);
        chat.getChatComponents().write(0, WrappedChatComponent.fromText(message));

        try {
            Main.protocolManager.sendServerPacket(pl, chat);
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (Exception e){
            e.printStackTrace();
        }

    }

}
