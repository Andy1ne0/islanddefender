package me.theyellowcreepz.coredefender.commands;

import me.theyellowcreepz.coredefender.GameState;
import me.theyellowcreepz.coredefender.Main;
import me.theyellowcreepz.coredefender.messageStandards;
import me.theyellowcreepz.coredefender.returnWinners;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

/**
 * All code inside this plugin is the
 * property of Andrew Petersen.
 * Decompiling, copying, or claiming any
 * code from this plugin as your own
 * voids any legal rights you have to
 * this plugin, unless express permission
 * is given.
 */
public class nextState implements CommandExecutor {


    @Override
    public boolean onCommand(CommandSender cs, Command cmd, String lbl, String[] args) {
    cmd.setPermissionMessage(messageStandards.alertPrefix()+"You aren't allowed to do that!");
        if(cs instanceof Player || cs instanceof ConsoleCommandSender) {
            if(cs instanceof  ConsoleCommandSender || cs.hasPermission("CoreDefender.NextState")|| cs.isOp()) {
                cs.sendMessage(messageStandards.opOpsPref() + "Changing the GameState... ");
                switch (GameState.getState()) {
                    case LOBBY:
                        cs.sendMessage(messageStandards.alertPrefix()+ ChatColor.RESET+ChatColor.RED+"Core Defender may be unstable with any less than "+Main.minimumPlayers+" players!");
                        Main.lobbyRunning = false;
                        if(Main.getInstance().getConfig().getBoolean("use_map_voting")){
                            Main.loadingmapRunning = true;
                        } else {
                            Main.warmupRunning = true;
                        }
                        Main.lobbyTicks = 0;
                        break;

                    case WARMUP:
                        Main.warmupRunning = false;
                        Main.ingameRunning = true;
                        Main.warmupTicks = 0;
                        break;

                    case INGAME:
                        returnWinners.winners = null;
                        GameState.setState(GameState.ENDGAME);
                        Main.ingameTicks = 0;
                        Main.ingameRunning = false;
                        Main.endgameRunning = true;
                }
                // cs.sendMessage(messageStandards.opOpsPref() + "Set the GameState to " + messageStandards.getGameStateColor() + GameState.getState() + ChatColor.LIGHT_PURPLE+ "!");

            } else {
                cs.sendMessage(messageStandards.alertPrefix() + "You aren't allowed to do that!");
            }
        }

        return true;

    }
}
