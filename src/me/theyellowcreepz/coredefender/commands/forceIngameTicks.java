package me.theyellowcreepz.coredefender.commands;

import me.theyellowcreepz.coredefender.GameState;
import me.theyellowcreepz.coredefender.Main;
import me.theyellowcreepz.coredefender.messageStandards;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;

/**
 * All code inside this plugin is the
 * property of Andrew Petersen.
 * Decompiling, copying, or claiming any
 * code from this plugin as your own
 * voids any legal rights you have to
 * this plugin, unless express permission
 * is given.
 */
public class forceIngameTicks implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender cs, Command cmd, String lbl, String[] args) {
    if(( cs.hasPermission("CoreDefender.forceTicks") || cs.isOp() ) || cs instanceof ConsoleCommandSender)
        if(args.length == 1) {
                // int newTicks = Integer.parseInt(args[1]);
                if(GameState.getState().equals(GameState.INGAME)){
                    try {
                        int newT = Integer.parseInt(args[0]);
                        Main.ingameTicks = newT;
                        cs.sendMessage(messageStandards.pluginPlayerInputPrefix()+"Set the timer to "+ ChatColor.GOLD+newT+" seconds!");
                        Bukkit.getServer().getLogger().info("[CoreDefenderTimer] "+cs.getName()+" has set the ingame timer to "+newT+" seconds. ");
                    } catch (NumberFormatException nFE) {
                        cs.sendMessage(messageStandards.alertPrefix()+"Only numbers are allowed!");
                    }
                } else {
                    cs.sendMessage(messageStandards.alertPrefix()+"You can't do that now!");
                }
            } else {
                cs.sendMessage(messageStandards.alertPrefix()+"Please specify a number!");
            }
        return true;
    }
}
