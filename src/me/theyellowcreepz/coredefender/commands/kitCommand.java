package me.theyellowcreepz.coredefender.commands;

import me.theyellowcreepz.coredefender.GameState;
import me.theyellowcreepz.coredefender.inventories.kitManager;
import me.theyellowcreepz.coredefender.messageStandards;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * All code inside this plugin is the
 * property of Andrew Petersen.
 * Decompiling, copying, or claiming any
 * code from this plugin as your own
 * voids any legal rights you have to
 * this plugin, unless express permission
 * is given.
 */
public class kitCommand implements CommandExecutor {


    @Override
    public boolean onCommand(CommandSender cs, Command cmd, String lbl, String[] args) {
        if (cs instanceof Player) {
            if (GameState.getState() == GameState.WARMUP || GameState.getState().equals(GameState.INGAME)) {
                Player pl = (Player) cs;
                pl.openInventory(kitManager.kitSelMenu);
                pl.sendMessage(messageStandards.pluginPlayerInputPrefix() + "Select a kit!");

            } else if(!(GameState.getState() == GameState.WARMUP)){
                cs.sendMessage(messageStandards.alertPrefix() + ChatColor.RESET+ChatColor.GRAY+"You can't change kits now!");
            }

        } else {
            cs.sendMessage("[ !!! ] You must be a player to run this command! ");
        }

        return true;
    }
}
