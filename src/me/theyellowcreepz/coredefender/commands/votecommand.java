package me.theyellowcreepz.coredefender.commands;

import me.theyellowcreepz.coredefender.GameState;
import me.theyellowcreepz.coredefender.Main;
import me.theyellowcreepz.coredefender.messageStandards;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * All code inside this plugin is the
 * property of Andrew Petersen.
 * Decompiling, copying, or claiming any
 * code from this plugin as your own
 * voids any legal rights you have to
 * this plugin, unless express permission
 * is given.
 */
public class votecommand implements CommandExecutor {

    public boolean isNumber(String s){
        try {
            Integer.parseInt(s);
            return true;
        } catch (NumberFormatException e){
            return false;
        }
    }

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        if(!(commandSender instanceof Player)) {
            commandSender.sendMessage("[ !!! ] Please run this as a player. ");
            return true;
        }

        Player pl = (Player)commandSender;

        if(!GameState.getState().equals(GameState.LOBBY)){
            pl.sendMessage(messageStandards.alertPrefix()+ChatColor.RED+"You can't do that right now! ");
            return true;
        }

        switch (strings.length){

            case 0:

                Main.sendMapVotingInfo(pl);

                break;

            case 1:

                if(isNumber(strings[0])){
                    int playerVoteNum = Integer.parseInt(strings[0]);
                    if(playerVoteNum < 1 || playerVoteNum > Main.mapsToVoteOn.size()){
                        pl.sendMessage(messageStandards.alertPrefix()+ChatColor.RED+"Invalid voting number! ");
                        return true;
                    }
                    int actualVote = playerVoteNum - 1;
                    if(Main.mapsToVoteOn.keySet().contains(actualVote)){
                        if(!(Main.mapVotes.containsKey(pl))){
                            Main.mapVotes.put(pl, actualVote);
                            int mapVotes = 0;
                            for(Player player : Main.mapVotes.keySet()){
                                int thatPlayersVote = Main.mapVotes.get(player);
                                if(thatPlayersVote == actualVote){
                                    mapVotes++;
                                }
                            }
                            pl.sendMessage(messageStandards.pluginPlayerInputPrefix()+"You voted for "+ChatColor.GOLD+Main.mapsToVoteOn.get(actualVote).getPublicTitle()+ChatColor.GREEN+"! ");
                            pl.sendMessage(messageStandards.grayArrows()+ChatColor.DARK_AQUA+"New voting count: "+ChatColor.GOLD+mapVotes+ChatColor.DARK_AQUA+" votes. ");
                        } else {
                            pl.sendMessage(messageStandards.alertPrefix()+ChatColor.RED+"You've already voted! ");
                            return true;
                        }

                    }
                } else {
                    pl.sendMessage(messageStandards.alertPrefix()+ChatColor.RED+"That isn't a number! ");
                }

                break;

        }

        return true;
    }
}
