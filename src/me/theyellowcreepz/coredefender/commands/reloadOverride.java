package me.theyellowcreepz.coredefender.commands;

import me.theyellowcreepz.coredefender.messageStandards;
import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;

/**
 * All code inside this plugin is the
 * property of Andrew Petersen.
 * Decompiling, copying, or claiming any
 * code from this plugin as your own
 * voids any legal rights you have to
 * this plugin, unless express permission
 * is given.
 */
public class reloadOverride implements Listener {
    @EventHandler
    public void onReload(PlayerCommandPreprocessEvent evt){
        if(evt.getMessage().toLowerCase().contains("/reload")){
            evt.setCancelled(true);
            evt.getPlayer().sendMessage(messageStandards.pluginPrefixMessage() + ChatColor.RED +"Executing this command is not supported with Core Defender!");
            evt.getPlayer().sendMessage(messageStandards.pluginPrefixMessage()+ChatColor.RED+"It breaks the maps, kits, and teams. ");
            evt.getPlayer().sendMessage(messageStandards.pluginPrefixMessage()+ChatColor.GRAY+"Please reboot the server fully instead. ");
        }
    }
}
