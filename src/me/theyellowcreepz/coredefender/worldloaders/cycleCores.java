package me.theyellowcreepz.coredefender.worldloaders;

import me.theyellowcreepz.coredefender.Main;
import me.theyellowcreepz.coredefender.MapInstance;
import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.BlockState;
import org.bukkit.block.Sign;

/**
 * All code inside this plugin is the
 * property of Andrew Petersen.
 * Decompiling, copying, or claiming any
 * code from this plugin as your own
 * voids any legal rights you have to
 * this plugin, unless express permission
 * is given.
 */
public class cycleCores {

    public static Location cycleCores(String team) {
        World worldName = Main.mainWorld;
        MapInstance map = Main.mainWorldMapInstance;
        if (team.equalsIgnoreCase("red")) {
            Chunk redCoreChunk = worldName.getChunkAt(Main.redCoreLoc);
                for (BlockState blocks : redCoreChunk.getTileEntities()) {
                    if (blocks instanceof Sign) {
                        Sign sign = (Sign) blocks;
                        if (sign.getLine(0).equalsIgnoreCase("[core:Red]")) {
                            Location SpawnPoint = sign.getLocation();
                            return SpawnPoint;
                        }
                    }
                }

        }
        else if(team.equalsIgnoreCase("blue")){
            Chunk blueCoreChunk = worldName.getChunkAt(Main.blueCoreLoc);
                for (BlockState blocks : blueCoreChunk.getTileEntities()) {
                    if (blocks instanceof Sign) {
                        Sign sign = (Sign) blocks;
                        if (sign.getLine(0).equalsIgnoreCase("[core:Blue]")) {
                            Location SpawnPoint = sign.getLocation();
                            return SpawnPoint;
                        }
                    }
                }
        }
        return null;
    }
}
