package me.theyellowcreepz.coredefender.worldloaders;

import me.theyellowcreepz.coredefender.Main;
import org.bukkit.Bukkit;
import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.BlockState;
import org.bukkit.block.Sign;

/**
 * All code inside this plugin is the
 * property of Andrew Petersen.
 * Decompiling, copying, or claiming any
 * code from this plugin as your own
 * voids any legal rights you have to
 * this plugin, unless express permission
 * is given.
 */
public class cycleSpawns {

    static World worldName = Main.mainWorld;
    static Location spawnChunk = worldName.getSpawnLocation();



    public static Location setSpawns(String team) {
        if (team.equalsIgnoreCase("red")) {
            Chunk redSpawnChunk = worldName.getChunkAt(Main.redSpawnApprox);

                for (BlockState blocks : redSpawnChunk.getTileEntities()) {
                    if (blocks instanceof Sign) {
                        Sign sign = (Sign) blocks;
                        if (sign.getLine(0).equalsIgnoreCase("[spawn:Red]")) {
                            Bukkit.getServer().getLogger().info("[ Island Defender ] Red spawn location was set! ");
                            Location SpawnPoint = sign.getLocation();
                            return SpawnPoint;
                        }
                    }
                }
        }
        else if(team.equalsIgnoreCase("blue")){
            Chunk blueSpawnChunk = worldName.getChunkAt(Main.blueSpawnApprox);
                for (BlockState blocks : blueSpawnChunk.getTileEntities()) {
                    if (blocks instanceof Sign) {
                        Sign sign = (Sign) blocks;
                        if (sign.getLine(0).equalsIgnoreCase("[spawn:Blue]")) {
                            Location SpawnPoint = sign.getLocation();
                            Bukkit.getServer().getLogger().info("[ Island Defender ] Blue spawn location was set! ");
                            return SpawnPoint;
                        }
                    }
                }
        } else if(team.equalsIgnoreCase("General")){
            Chunk spawnChunk = worldName.getChunkAt(Main.generalSpawnApprox);
            for(BlockState blocks : spawnChunk.getTileEntities()){
                if (blocks instanceof Sign){
                    Sign sign = (Sign) blocks;
                    if(sign.getLine(0).equalsIgnoreCase("[spawn:Gen]")){
                        Location SpawnPoint = sign.getLocation();
                        return SpawnPoint;
                    }
                }
            }
        }
        return null;
    }
}
