package me.theyellowcreepz.coredefender.inventories;

import me.theyellowcreepz.coredefender.GameState;
import me.theyellowcreepz.coredefender.messageStandards;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

/**
 * All code inside this plugin is the
 * property of Andrew Petersen.
 * Decompiling, copying, or claiming any
 * code from this plugin as your own
 * voids any legal rights you have to
 * this plugin, unless express permission
 * is given.
 */
public class kitCooldowns extends BukkitRunnable {
    @Override
    public void run() {
        if(GameState.getState() == GameState.INGAME){
            for(Player pl: Bukkit.getOnlinePlayers()) {
                if(kitManager.playerKits.containsKey(pl)) {
                    switch (kitManager.playerKits.get(pl)) {

                        /* Kit Cooldown Template:
                        case <Class ID>:
                            if (kitManager.<Kit Cooldown Variable - declared in kitManager>.containsKey(pl)) {
                                if (kitManager.< Kit Cooldown Variable >.get(pl).equals(1)) {
                                    kitManager.< Kit Cooldown Variable >.remove(pl);
                                    pl.sendMessage(messageStandards.kitUsableAgain());
                                } else {
                                    int newValue = kitManager.< Kit Cooldown Variable >.get(pl) - 1;
                                    kitManager.< Kit Cooldown Variable >.replace(pl, newValue);
                                }
                            }
                            break;
                         */

                        case 1:
                            if (kitManager.kit0Cooldown.containsKey(pl)) {
                                if (kitManager.kit0Cooldown.get(pl).equals(1)) {
                                    kitManager.kit0Cooldown.remove(pl);
                                    pl.sendMessage(messageStandards.kitUsableAgain());
                                } else {
                                    int newValue = kitManager.kit0Cooldown.get(pl) - 1;
                                    kitManager.kit0Cooldown.replace(pl, newValue);
                                }
                            }
                            break;
                        case 2:
                            if (kitManager.kit1Cooldown.containsKey(pl)) {
                                if (kitManager.kit1Cooldown.get(pl).equals(1)) {
                                    kitManager.kit1Cooldown.remove(pl);
                                    pl.sendMessage(messageStandards.kitUsableAgain());
                                } else {
                                    int newValue = kitManager.kit1Cooldown.get(pl) - 1;
                                    kitManager.kit1Cooldown.replace(pl, newValue);
                                }
                            }
                            break;
                        case 3:
                            if (kitManager.kit2Cooldown.containsKey(pl)) {
                                if (kitManager.kit2Cooldown.get(pl).equals(1)) {
                                    kitManager.kit2Cooldown.remove(pl);
                                    pl.sendMessage(messageStandards.kitUsableAgain());
                                } else {
                                    int newValue = kitManager.kit2Cooldown.get(pl) - 1;
                                    kitManager.kit2Cooldown.replace(pl, newValue);
                                }
                            }
                            break;
                        case 4:
                            if (kitManager.kit3Cooldown.containsKey(pl)) {
                                if (kitManager.kit3Cooldown.get(pl).equals(1)) {
                                    kitManager.kit3Cooldown.remove(pl);
                                    pl.sendMessage(messageStandards.kitUsableAgain());
                                } else {
                                    int newValue = kitManager.kit3Cooldown.get(pl) - 1;
                                    kitManager.kit3Cooldown.replace(pl, newValue);
                                }
                            }
                            break;
                        case 5:
                            if (kitManager.kit4Cooldown.containsKey(pl)) {
                                if (kitManager.kit4Cooldown.get(pl).equals(1)) {
                                    kitManager.kit4Cooldown.remove(pl);
                                    pl.sendMessage(messageStandards.kitUsableAgain());
                                } else {
                                    int newValue = kitManager.kit4Cooldown.get(pl) - 1;
                                    kitManager.kit4Cooldown.replace(pl, newValue);
                                }
                            }
                            break;
                        case 9:
                            if(kitManager.kit9Cooldown.containsKey(pl)){
                                if(kitManager.kit9Cooldown.get(pl).equals(1)){
                                    kitManager.kit9Cooldown.remove(pl);
                                    pl.sendMessage(messageStandards.kitUsableAgain());
                                } else {
                                    int newValue = kitManager.kit9Cooldown.get(pl) - 1;
                                    kitManager.kit9Cooldown.replace(pl, newValue);
                                }
                            }
                            break;
                        case 10:
                        if (kitManager.kit10Cooldown.containsKey(pl)) {
                            if (kitManager.kit10Cooldown.get(pl).equals(1)) {
                                kitManager.kit10Cooldown.remove(pl);
                                pl.sendMessage(messageStandards.kitUsableAgain());
                            } else {
                                int newValue = kitManager.kit10Cooldown.get(pl) - 1;
                                kitManager.kit10Cooldown.replace(pl, newValue);
                            }
                        }
                        break;
                    }
                }
            }
        }

    }
}
