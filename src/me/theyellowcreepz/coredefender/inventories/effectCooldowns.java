package me.theyellowcreepz.coredefender.inventories;

import me.theyellowcreepz.coredefender.messageStandards;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

/**
 * All code inside this plugin is the
 * property of Andrew Petersen.
 * Decompiling, copying, or claiming any
 * code from this plugin as your own
 * voids any legal rights you have to
 * this plugin, unless express permission
 * is given.
 */
public class effectCooldowns extends BukkitRunnable {
    @Override
    public void run() {
        for(Player pl: Bukkit.getOnlinePlayers()){
            if(kitManager.kit1Freeze.containsKey(pl)){
                if(kitManager.kit1Freeze.get(pl).equals(1)){
                    kitManager.kit1Freeze.remove(pl);
                    pl.sendMessage(messageStandards.grayArrows()+"You can now move. ");
                } else {
                    int newVal = kitManager.kit1Freeze.get(pl) - 1;
                    kitManager.kit1Freeze.remove(pl);
                    kitManager.kit1Freeze.put(pl, newVal);
                }
            }
        }
    }
}
