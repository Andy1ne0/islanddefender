package me.theyellowcreepz.coredefender.inventories;

import me.theyellowcreepz.coredefender.GameState;
import me.theyellowcreepz.coredefender.Main;
import me.theyellowcreepz.coredefender.interfaces.kitInterface;
import me.theyellowcreepz.coredefender.inventories.kitfiles.*;
import me.theyellowcreepz.coredefender.messageStandards;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

/**
 * All code inside this plugin is the
 * property of Andrew Petersen.
 * Decompiling, copying, or claiming any
 * code from this plugin as your own
 * voids any legal rights you have to
 * this plugin, unless express permission
 * is given.
 */
public class kitManager implements Listener {

    // Array with all kit classes.
    public static ArrayList<kitInterface> kitClasses = new ArrayList<>();

    // Player kits storage.
    public static HashMap<Player, Integer> playerKits = new HashMap<>();

    // Kit ID reference Array.
    public static HashMap<Integer, kitInterface> kitIdReference = new HashMap<>();

    // Kit Selector declaration.
    public static Inventory kitSelMenu;

    // Kit variables: Usage Cooldowns
    public static Map<Player, Integer> kit0Cooldown = new HashMap<Player, Integer>();
    public static Map<Player, Integer> kit1Cooldown = new HashMap<Player, Integer>();
    public static Map<Player, Integer> kit2Cooldown = new HashMap<Player, Integer>();
    public static Map<Player, Integer> kit3Cooldown = new HashMap<Player, Integer>();
    public static Map<Player, Integer> kit4Cooldown = new HashMap<Player, Integer>();
    public static Map<Player, Integer> kit9Cooldown = new HashMap<Player, Integer>();
    public static Map<Player, Integer> kit10Cooldown = new HashMap<Player, Integer>();

    // Kit variables: Effect cooldowns
    public static Map<Player, Integer> kit1Freeze = new HashMap<Player, Integer>();

    public void instantiateManager(){
        Bukkit.getServer().getLogger().info("[ Core Defender ] Kit manager instantiated. ");
    }

    public static void instantiateKits(){

        // Register all kit files here.
        
        kitClasses.add(new kit1());
        kitClasses.add(new kit2());
        kitClasses.add(new kit3());
        kitClasses.add(new kit4());
        kitClasses.add(new kit5());
        kitClasses.add(new kit6());
        kitClasses.add(new kit7());
        kitClasses.add(new kit8());
        kitClasses.add(new kit9());
        kitClasses.add(new kit10());

        for(int i = 0; i < kitClasses.size(); i++){
            kitInterface kitInt = kitClasses.get(i);
            if(kitInt.hasListeners()){
                Bukkit.getServer().getPluginManager().registerEvents((Listener) kitClasses.get(i), Main.getInstance());
            }
            if(kitInt.getKitID() == 0){
                Bukkit.getServer().getLogger().severe("[ Core Defender ] Kit Error (SEND TO YELLOW): Kit "+kitInt.getKitTitle()+" has no ID. Deleted for plugin safety. ");
                kitClasses.remove(i);
            } else {
                kitIdReference.put(kitInt.getKitID(), kitInt);
            }
        }

        Bukkit.getServer().getLogger().info("[ Core Defender ] Kits registered: "+kitClasses.size());
        Bukkit.getServer().getLogger().info(" ");
        Bukkit.getServer().getLogger().info("[ Core Defender ] The following kits were registered: ");
        Bukkit.getServer().getLogger().info(" ");
        ConsoleCommandSender receiver = Bukkit.getConsoleSender();
        for (int i = 0; i < kitClasses.size(); i++) {
            String isListener = (kitClasses.get(i).hasListeners()) ? (ChatColor.GRAY+" (Plus Listener)") : "";
            receiver.sendMessage(kitClasses.get(i).getKitTitle() + isListener);
        }

    }

    public static void configureKitMenu(){
        ArrayList<ItemStack> inventoryItems = new ArrayList<>();
        for(int i = 0; i < kitClasses.size(); i++){ // ++i
            kitInterface kitClass = kitClasses.get(i);
            if(kitClass != null){
                inventoryItems.add(kitClass.getDisplayItem());
            }
        }
        float invSize;
        invSize = (float)Math.ceil((inventoryItems.size()) / 9.0) * 9;

        kitSelMenu = Bukkit.createInventory(null, (int) invSize, ChatColor.BOLD+""+ChatColor.GRAY+">> "+ChatColor.GREEN+"Select a kit!");
        for(int i = 0; i < inventoryItems.size(); i++){
            ItemStack item = inventoryItems.get(i);
            if(item != null) {
                kitSelMenu.addItem(item);
            }
        }
        for (int i = (int)kitClasses.size(); i < invSize; i++) {
            ItemStack placeholder = new ItemStack(Material.STAINED_GLASS_PANE, 1);
            ItemMeta placeholderMeta = placeholder.getItemMeta();
            placeholderMeta.setDisplayName(" ");
            placeholder.setItemMeta(placeholderMeta);
            kitSelMenu.setItem(i, placeholder);
        }
        Bukkit.getServer().getLogger().info("");
        Bukkit.getServer().getLogger().info("[ Core Defender ] Kit inventory size: "+invSize);
        Bukkit.getServer().getLogger().info("[ Core Defender ] Kits GUI configured successfully. ");
        Bukkit.getServer().getLogger().info("");
    }

    public static void givePlayerKit(Player pl){
        pl.setGameMode(GameMode.SURVIVAL);
        if(!playerKits.containsKey(pl)){
            Random rand = new Random();
            int random = rand.nextInt(kitIdReference.size());
            playerKits.put(pl, random);
        }
        kitInterface playerKit = kitIdReference.get(playerKits.get(pl));

        pl.getInventory().setHelmet(playerKit.getHelmet());

        pl.getInventory().setChestplate(playerKit.getChestplate());

        pl.getInventory().setLeggings(playerKit.getLeggings());

        pl.getInventory().setBoots(playerKit.getBoots());

        if(playerKit.getKitItems() != null){
            for (int i = 0; i < playerKit.getKitItems().size(); i++) {
                pl.getInventory().addItem(playerKit.getKitItems().get(i));
            }
            pl.getInventory().addItem(new ItemStack(Material.LEAVES, 16));
        }

        if(playerKit.getPlayerMaxHealth() != 0){
            pl.setMaxHealth(playerKit.getPlayerMaxHealth());
        }

        if(playerKit.getPotionEffects() != null){
            for (int i = 0; i < playerKit.getPotionEffects().size(); i++) {
                pl.addPotionEffect(playerKit.getPotionEffects().get(0));
            }
        }
        pl.sendMessage(messageStandards.pluginPlayerInputPrefix()+ChatColor.RESET+ChatColor.BOLD+""+ChatColor.DARK_GREEN+"You were given your kit: "+playerKit.getKitTitle()+ChatColor.RESET+ChatColor.BOLD+""+ChatColor.DARK_GREEN+"!");
        Bukkit.getServer().getLogger().info("[ Core Defender ] Player "+pl.getName()+" was given kit "+playerKit.getKitTitle());
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerClick(InventoryClickEvent evt){
        if(GameState.getState().equals(GameState.WARMUP) || GameState.getState().equals(GameState.INGAME)){
            Player pl = (Player) evt.getWhoClicked();
            Inventory inv = evt.getInventory();
            if(inv != null && inv.equals(kitSelMenu) && evt.getCursor() != null){
                for(int i = 0; i < kitClasses.size(); i++){
                    kitInterface kitClass = kitClasses.get(i);
                    if(evt.getCurrentItem().equals(kitClass.getDisplayItem())){
                        playerKits.remove(pl);
                        playerKits.put(pl, kitClass.getKitID());
                        pl.closeInventory();
                        pl.sendMessage(ChatColor.DARK_GREEN+""+ChatColor.BOLD+">>> "+ChatColor.GREEN
                                +"You selected kit "+ChatColor.LIGHT_PURPLE+kitClass.getKitTitle()+ChatColor.GREEN+"!");
                        i = kitClasses.size() + 1;
                        continue;
                    }
                }
            }
        } else if(evt.getInventory().equals(kitSelMenu)){
            ((Player)evt.getWhoClicked()).sendMessage(messageStandards.alertPrefix()+ChatColor.RESET+ChatColor.RED+"You can't do that right now! ");
            ((Player)evt.getWhoClicked()).closeInventory();
        }
    }
}
