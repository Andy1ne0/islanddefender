package me.theyellowcreepz.coredefender.inventories.kitfiles;

import com.google.common.collect.Lists;
import me.theyellowcreepz.coredefender.GameState;
import me.theyellowcreepz.coredefender.Main;
import me.theyellowcreepz.coredefender.interfaces.kitInterface;
import me.theyellowcreepz.coredefender.inventories.kitManager;
import me.theyellowcreepz.coredefender.messageStandards;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;

import java.util.ArrayList;

/**
 * All code inside this plugin is the
 * property of Andrew Petersen.
 * Decompiling, copying, or claiming any
 * code from this plugin as your own
 * voids any legal rights you have to
 * this plugin, unless express permission
 * is given.
 */
public class kit1 implements kitInterface, Listener {
    @Override
    public ItemStack getDisplayItem() {
        ItemStack kit1 = new ItemStack(org.bukkit.Material.GOLD_HELMET, 1);
        ItemMeta kit1meta = kit1.getItemMeta();
        kit1meta.setDisplayName(ChatColor.GOLD + "" + ChatColor.BOLD + "Thor's Hammer");
        kit1meta.setLore(Lists.newArrayList(ChatColor.AQUA + "Strike opponents with lightning!", ChatColor.RED+""+ChatColor.BOLD+"You only have 9 hearts. ",ChatColor.GRAY+"(Cooldown: 5 Seconds)"));
        kit1.setItemMeta(kit1meta);
        return kit1;
    }

    @Override
    public String getKitTitle() {
        return (ChatColor.GOLD + "" + ChatColor.BOLD + "Thor's Hammer");
    }

    @Override
    public boolean hasListeners() {
        return true;
    }

    @Override
    public int getKitID() {
        return 1;
    }

    @Override
    public ArrayList<ItemStack> getKitItems() {

        ArrayList<ItemStack> kitItems = new ArrayList<ItemStack>();

        ItemStack ThorBow = new ItemStack(Material.GOLD_HOE, 1);
        ItemMeta ThorBowMeta = ThorBow.getItemMeta();
        ThorBowMeta.setDisplayName(ChatColor.GOLD + "Thor's Hammer " + messageStandards.rightClickText());
        ThorBowMeta.setLore(Lists.newArrayList(ChatColor.AQUA + "" + ChatColor.ITALIC + "Right-click to shoot lightning!"));
        ThorBow.setItemMeta(ThorBowMeta);
        ThorBow.addUnsafeEnchantment(Enchantment.ARROW_INFINITE, 1);
        ThorBow.addUnsafeEnchantment(Enchantment.DURABILITY, 3);

        kitItems.add(ThorBow);

        ItemStack ThorStdBow = new ItemStack(Material.BOW, 1);
        ItemMeta ThorStdBowMeta = ThorStdBow.getItemMeta();
        ThorStdBowMeta.setDisplayName(ChatColor.GOLD + "Thor's Bow");
        ThorStdBowMeta.setLore(Lists.newArrayList(ChatColor.AQUA + "" + ChatColor.ITALIC));
        ThorStdBow.setItemMeta(ThorStdBowMeta);
        ThorStdBow.addUnsafeEnchantment(Enchantment.ARROW_KNOCKBACK, 1);
        ThorStdBow.addUnsafeEnchantment(Enchantment.ARROW_INFINITE, 1);

        kitItems.add(ThorStdBow);

        ItemStack ThorSword = new ItemStack(Material.WOOD_SWORD, 1);
        ItemMeta ThorSwordMeta = ThorSword.getItemMeta();
        ThorSwordMeta.setDisplayName(ChatColor.DARK_GRAY+"Thor's Trustee Sword");
        ThorSword.setItemMeta(ThorSwordMeta);
        ThorSword.addUnsafeEnchantment(Enchantment.DURABILITY, 10);

        kitItems.add(ThorSword);

        ItemStack Arrows = new ItemStack(Material.ARROW, 1);

        kitItems.add(Arrows);

        return kitItems;
    }

    @Override
    public ItemStack getHelmet() {
        ItemStack Helmet = new ItemStack(Material.GOLD_BLOCK);
        ItemMeta HelmetMeta = Helmet.getItemMeta();
        HelmetMeta.setDisplayName(ChatColor.GOLD + "Thor's Helmet");
        Helmet.setItemMeta(HelmetMeta);

        return Helmet;
    }

    @Override
    public ItemStack getChestplate() {
        ItemStack Chestplate = new ItemStack(Material.GOLD_CHESTPLATE);
        ItemMeta ChestplateMeta = Chestplate.getItemMeta();
        ChestplateMeta.setDisplayName(ChatColor.GOLD + "Thor's T-shirt");
        Chestplate.setItemMeta(ChestplateMeta);
        Chestplate.addUnsafeEnchantment(Enchantment.PROTECTION_EXPLOSIONS, 1);

        return Chestplate;
    }

    @Override
    public ItemStack getLeggings() {
        ItemStack Leggings = new ItemStack(Material.GOLD_LEGGINGS);
        ItemMeta LeggingsMeta = Leggings.getItemMeta();
        LeggingsMeta.setDisplayName(ChatColor.GOLD + "Thor's Jeans");
        Leggings.setItemMeta(LeggingsMeta);

        return Leggings;
    }

    @Override
    public ItemStack getBoots() {
        ItemStack Boots = new ItemStack(Material.GOLD_BOOTS);
        ItemMeta BootsMeta = Boots.getItemMeta();
        BootsMeta.setDisplayName(ChatColor.GOLD + "Thor's Boots");
        Boots.setItemMeta(BootsMeta);
        Boots.addUnsafeEnchantment(Enchantment.DEPTH_STRIDER, 1);

        return Boots;
    }

    @Override
    public ArrayList<PotionEffect> getPotionEffects() {
        return null;
    }

    @Override
    public int getPlayerMaxHealth() {
        return 18;
    }

    @EventHandler
    public void onRightClick(PlayerInteractEvent evt){
        if(evt.getPlayer() != null && GameState.getState() == GameState.INGAME && !(Main.spectatorTeam.contains(evt.getPlayer())) && kitManager.playerKits.get(evt.getPlayer()) == 1){
            if(((evt.getAction() ==  (Action.RIGHT_CLICK_AIR))
                    || (evt.getAction() ==  Action.RIGHT_CLICK_BLOCK))
                    && (evt.getItem() != null && evt.getPlayer().getItemInHand().getType() == Material.GOLD_HOE)) {
                if(!(kitManager.kit0Cooldown.containsKey(evt.getPlayer()))){
                    Player pl = evt.getPlayer();
                    @SuppressWarnings("deprecation")
                    Block targetBlock = pl.getTargetBlock(null, 50);
                    if (targetBlock.getType() == Material.AIR) {
                        evt.setCancelled(true);
                        pl.sendMessage(messageStandards.alertPrefix() + ChatColor.RED + "The target block must be within 50 blocks!");
                    } else {
                        World w = Main.mainWorld;
                        w.strikeLightning(targetBlock.getLocation());
                        kitManager.kit0Cooldown.put(pl, 5);
                        pl.sendMessage(messageStandards.grayArrows() + ChatColor.GOLD + "Lightning has struck!");
                        Bukkit.getServer().getLogger().info("> Player " + pl.getName()
                                + " has struck lightning using the Thor kit at co-ordinates (" + targetBlock.getLocation() + "). ");
                    }
                }else if(kitManager.kit0Cooldown.containsKey(evt.getPlayer())){
                    evt.getPlayer().sendMessage(messageStandards.kitCooldownMsg(evt.getPlayer(), kitManager.kit0Cooldown));
                }
            }
        }
    }
}
