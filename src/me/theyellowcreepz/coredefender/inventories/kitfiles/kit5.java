package me.theyellowcreepz.coredefender.inventories.kitfiles;

import com.google.common.collect.Lists;
import me.theyellowcreepz.coredefender.GameState;
import me.theyellowcreepz.coredefender.Main;
import me.theyellowcreepz.coredefender.interfaces.kitInterface;
import me.theyellowcreepz.coredefender.inventories.kitManager;
import me.theyellowcreepz.coredefender.messageStandards;
import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.Snowball;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.ArrayList;

/**
 * All code inside this plugin is the
 * property of Andrew Petersen.
 * Decompiling, copying, or claiming any
 * code from this plugin as your own
 * voids any legal rights you have to
 * this plugin, unless express permission
 * is given.
 */
public class kit5 implements kitInterface, Listener {
    @Override
    public ItemStack getDisplayItem() {

        ItemStack kit5 = new ItemStack(Material.INK_SACK, 1);
        ItemMeta kit5meta = kit5.getItemMeta();
        kit5meta.setDisplayName(ChatColor.DARK_BLUE+""+ChatColor.BOLD+"The Squid");
        kit5meta.setLore(Lists.newArrayList(ChatColor.AQUA+"Blind your opponents for 10 seconds!", ChatColor.GRAY+"(Cooldown: 15 Seconds)", ChatColor.RED+""+ChatColor.BOLD+"You only have 9 hearts. "));
        kit5.setItemMeta(kit5meta);

        return kit5;
    }

    @Override
    public String getKitTitle() {
        return (ChatColor.DARK_BLUE+""+ChatColor.BOLD+"The Squid");
    }

    @Override
    public boolean hasListeners() {
        return true;
    }

    @Override
    public int getKitID() {
        return 5;
    }

    @Override
    public ArrayList<ItemStack> getKitItems() {

        ArrayList<ItemStack> kitItems = new ArrayList<>();

        ItemStack InkGun = new ItemStack(Material.INK_SACK, 1);
        ItemMeta InkGunMeta = InkGun.getItemMeta();
        InkGunMeta.setDisplayName(ChatColor.DARK_AQUA+"Ink Gun "+ messageStandards.rightClickText());
        InkGunMeta.setLore(Lists.newArrayList(ChatColor.AQUA+""+ChatColor.ITALIC+"Shoot blinding ink at your opponents in the form of a snowball!"));
        InkGun.setItemMeta(InkGunMeta);

        kitItems.add(InkGun);

        ItemStack InkSword = new ItemStack(Material.STONE_SWORD, 1);
        ItemMeta InkSwordMeta = InkSword.getItemMeta();
        InkSwordMeta.setDisplayName(ChatColor.DARK_AQUA+"Inky Sword");
        InkSword.setItemMeta(InkSwordMeta);
        InkSword.addEnchantment(Enchantment.DAMAGE_ALL, 1);

        kitItems.add(InkSword);

        return kitItems;
    }

    @Override
    public ItemStack getHelmet() {

        ItemStack Helmet = new ItemStack(Material.LEATHER_HELMET, 1);
        LeatherArmorMeta HelmetMeta = (LeatherArmorMeta) Helmet.getItemMeta();
        HelmetMeta.setDisplayName(ChatColor.DARK_AQUA+"Squid Hat");
        HelmetMeta.setColor(Color.BLUE);
        Helmet.setItemMeta(HelmetMeta);
        Helmet.addEnchantment(Enchantment.OXYGEN, 3);

        return Helmet;
    }

    @Override
    public ItemStack getChestplate() {

        ItemStack Chestplate = new ItemStack(Material.LEATHER_CHESTPLATE, 1);
        LeatherArmorMeta ChestplateMeta = (LeatherArmorMeta) Chestplate.getItemMeta();
        ChestplateMeta.setColor(Color.BLUE);
        ChestplateMeta.setDisplayName(ChatColor.DARK_AQUA+"Squid Shirt");
        Chestplate.setItemMeta(ChestplateMeta);

        return Chestplate;
    }

    @Override
    public ItemStack getLeggings() {

        ItemStack Leggings = new ItemStack(Material.LEATHER_LEGGINGS, 1);
        LeatherArmorMeta LeggingsMeta = (LeatherArmorMeta) Leggings.getItemMeta();
        LeggingsMeta.setDisplayName(ChatColor.DARK_AQUA+"Squid Tentacles");
        LeggingsMeta.setColor(Color.BLUE);
        Leggings.setItemMeta(LeggingsMeta);
        Leggings.addEnchantment(Enchantment.PROTECTION_PROJECTILE, 1);

        return Leggings;
    }

    @Override
    public ItemStack getBoots() {

        ItemStack Boots = new ItemStack(Material.LEATHER_BOOTS, 1);
        LeatherArmorMeta BootsMeta = (LeatherArmorMeta) Boots.getItemMeta();
        BootsMeta.setColor(Color.BLUE);
        BootsMeta.setDisplayName(ChatColor.DARK_AQUA+"Squid Feet");
        Boots.setItemMeta(BootsMeta);

        return Boots;
    }

    @Override
    public ArrayList<PotionEffect> getPotionEffects() {
        return null;
    }

    @Override
    public int getPlayerMaxHealth() {
        return 18;
    }

    @EventHandler
    public void onInteract(PlayerInteractEvent evt){
        if(evt.getPlayer() != null && GameState.getState() == GameState.INGAME){
            Player pl = evt.getPlayer();
            if(!(Main.spectatorTeam.contains(pl)) && kitManager.playerKits.get(pl) == 5) {
                if (evt.getItem() != null && evt.getAction() == Action.RIGHT_CLICK_BLOCK || evt.getAction() == Action.RIGHT_CLICK_AIR) {

                    if (pl.getItemInHand().getType() == Material.INK_SACK) {
                        if (!kitManager.kit4Cooldown.containsKey(pl)) {
                            Entity s = pl.launchProjectile(Snowball.class);
                            s.setCustomName("kit4sb");
                            s.setVelocity(s.getVelocity().setY(0));
                            s.setVelocity(s.getVelocity().multiply(3.0D));
                            pl.sendMessage(messageStandards.grayArrows() + "You've shot an ink-filled snowball!");
                            kitManager.kit4Cooldown.put(pl, 15);
                        } else if (kitManager.kit4Cooldown.containsKey(pl)) {
                            pl.sendMessage(messageStandards.kitCooldownMsg(pl, kitManager.kit4Cooldown));
                        }
                    }
                }
            }
        }
    }
    @EventHandler
    public void onHit(EntityDamageByEntityEvent evt){

        if (evt.getDamager().getType() == EntityType.SNOWBALL){

            Snowball sb = (Snowball) evt.getDamager();
            if (sb.getCustomName() != null && sb.getCustomName().equalsIgnoreCase("kit4sb")){
                if (evt.getEntity() instanceof Player){
                    Player pl = (Player) evt.getEntity();
                    pl.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, 100, 2, true));
                    pl.addPotionEffect(new PotionEffect(PotionEffectType.NIGHT_VISION, 100, 2, true));
                    pl.sendMessage(messageStandards.grayArrows()+ ChatColor.RESET+ChatColor.DARK_GRAY+ChatColor.ITALIC
                            +"You were blinded for 5 seconds!");
                }
            }
        }
    }
}
