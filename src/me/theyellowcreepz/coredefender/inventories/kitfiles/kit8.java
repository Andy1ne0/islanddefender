package me.theyellowcreepz.coredefender.inventories.kitfiles;

import com.google.common.collect.Lists;
import me.theyellowcreepz.coredefender.interfaces.kitInterface;
import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.ArrayList;

/**
 * All code inside this plugin is the
 * property of Andrew Petersen.
 * Decompiling, copying, or claiming any
 * code from this plugin as your own
 * voids any legal rights you have to
 * this plugin, unless express permission
 * is given.
 */
public class kit8 implements kitInterface {
    @Override
    public ItemStack getDisplayItem() {
        ItemStack dispItem = new ItemStack(Material.CACTUS, 1);
        ItemMeta dispItemMeta = dispItem.getItemMeta();
        dispItemMeta.setDisplayName(ChatColor.GREEN+""+ChatColor.BOLD+"Walking Cactus");
        dispItemMeta.setLore(Lists.newArrayList(ChatColor.AQUA+"You're a walking cactus! Go spike your enemies with pricks!", ChatColor.RED+""+ChatColor.BOLD+"You only have 5 hearts. ", ChatColor.RED+""+ChatColor.BOLD+"You have slowness 2. "));
        dispItem.setItemMeta(dispItemMeta);
        dispItem.addUnsafeEnchantment(Enchantment.THORNS, 2);
        return dispItem;
    }

    @Override
    public String getKitTitle() {
        return (ChatColor.GREEN+""+ChatColor.BOLD+"Walking Cactus");
    }

    @Override
    public boolean hasListeners() {
        return false;
    }

    @Override
    public int getKitID() {
        return 8;
    }

    @Override
    public ArrayList<ItemStack> getKitItems() {

        ArrayList<ItemStack> kitItems = new ArrayList<>();

        ItemStack cactusSword = new ItemStack(Material.STONE_SWORD, 1);
        ItemMeta cactusSwordMeta = cactusSword.getItemMeta();
        cactusSwordMeta.setDisplayName(ChatColor.GREEN+""+ChatColor.BOLD+"Cactus Sword");
        cactusSword.setItemMeta(cactusSwordMeta);
        cactusSword.addEnchantment(Enchantment.DAMAGE_ALL, 1);
        cactusSword.addUnsafeEnchantment(Enchantment.THORNS, 2);

        kitItems.add(cactusSword);

        return kitItems;
    }

    @Override
    public ItemStack getHelmet() {
        ItemStack Helmet = new ItemStack(Material.LEATHER_HELMET, 1);
        LeatherArmorMeta HelmetMeta = (LeatherArmorMeta) Helmet.getItemMeta();
        HelmetMeta.setDisplayName(ChatColor.GREEN+""+ChatColor.BOLD+"Cactus Head");
        HelmetMeta.setColor(Color.GREEN);
        Helmet.setItemMeta(HelmetMeta);
        Helmet.addEnchantment(Enchantment.THORNS, 1);
        return Helmet;
    }

    @Override
    public ItemStack getChestplate() {
        ItemStack Chestplate = new ItemStack(Material.LEATHER_CHESTPLATE, 1);
        LeatherArmorMeta ChestplateMeta = (LeatherArmorMeta) Chestplate.getItemMeta();
        ChestplateMeta.setDisplayName(ChatColor.GREEN+""+ChatColor.BOLD+"Cactus Chestplate");
        ChestplateMeta.setColor(Color.GREEN);
        Chestplate.setItemMeta(ChestplateMeta);
        Chestplate.addEnchantment(Enchantment.THORNS, 2);
        Chestplate.addEnchantment(Enchantment.DURABILITY, 2);
        Chestplate.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 1);
        return Chestplate;
    }

    @Override
    public ItemStack getLeggings() {
        ItemStack Leggings = new ItemStack(Material.LEATHER_LEGGINGS, 1);
        LeatherArmorMeta LeggingsMeta = (LeatherArmorMeta) Leggings.getItemMeta();
        LeggingsMeta.setDisplayName(ChatColor.GREEN+""+ChatColor.BOLD+"Cactus Legs");
        LeggingsMeta.setColor(Color.GREEN);
        Leggings.setItemMeta(LeggingsMeta);
        Leggings.addEnchantment(Enchantment.THORNS, 2);
        Leggings.addEnchantment(Enchantment.DURABILITY, 2);
        Leggings.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 1);
        return Leggings;
    }

    @Override
    public ItemStack getBoots() {
        ItemStack Boots = new ItemStack(Material.LEATHER_BOOTS, 1);
        LeatherArmorMeta BootsMeta = (LeatherArmorMeta) Boots.getItemMeta();
        BootsMeta.setDisplayName(ChatColor.GREEN+""+ChatColor.BOLD+"Cactus Boots");
        BootsMeta.setColor(Color.GREEN);
        Boots.setItemMeta(BootsMeta);
        Boots.addEnchantment(Enchantment.THORNS, 1);
        return Boots;
    }

    @Override
    public ArrayList<PotionEffect> getPotionEffects() {
        ArrayList<PotionEffect> effects = new ArrayList<>();
        effects.add(new PotionEffect(PotionEffectType.SLOW, 10000000, 1));
        return effects;
    }

    @Override
    public int getPlayerMaxHealth() {
        return 10;
    }
}
