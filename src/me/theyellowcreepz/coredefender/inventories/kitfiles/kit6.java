package me.theyellowcreepz.coredefender.inventories.kitfiles;

import com.google.common.collect.Lists;
import me.theyellowcreepz.coredefender.interfaces.kitInterface;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.ArrayList;

/**
 * All code inside this plugin is the
 * property of Andrew Petersen.
 * Decompiling, copying, or claiming any
 * code from this plugin as your own
 * voids any legal rights you have to
 * this plugin, unless express permission
 * is given.
 */
public class kit6 implements kitInterface {
    @Override
    public ItemStack getDisplayItem() {

        ItemStack kit6 = new ItemStack(Material.PAPER, 1);
        ItemMeta kit6meta = kit6.getItemMeta();
        kit6meta.setDisplayName(ChatColor.GRAY+""+ChatColor.BOLD+"Spooky Axe");
        kit6meta.setLore(Lists.newArrayList(ChatColor.AQUA+"You're semi-invisible to all players, including your teammates!", ChatColor.RED+""+ChatColor.BOLD+"You only have 3 hearts. "));
        kit6.setItemMeta(kit6meta);

        return kit6;
    }

    @Override
    public String getKitTitle() {
        return (ChatColor.GRAY+""+ChatColor.BOLD+"Spooky Axe");
    }

    @Override
    public boolean hasListeners() {
        return false;
    }

    @Override
    public int getKitID() {
        return 6;
    }

    @Override
    public ArrayList<ItemStack> getKitItems() {

        ArrayList<ItemStack> kitItems = new ArrayList<>();

        ItemStack ghostAxe = new ItemStack(Material.IRON_AXE, 1);
        ItemMeta axeMeta = ghostAxe.getItemMeta();
        axeMeta.setDisplayName(ChatColor.GRAY+""+ChatColor.BOLD+"Spooky Axe");
        axeMeta.setLore(Lists.newArrayList(ChatColor.AQUA+"Your spooky axe! You're semi-invisible, use it wisely!"));
        ghostAxe.setItemMeta(axeMeta);
        ghostAxe.addEnchantment(Enchantment.DAMAGE_ALL, 3);

        kitItems.add(ghostAxe);

        return kitItems;
    }

    @Override
    public ItemStack getHelmet() {
        return null;
    }

    @Override
    public ItemStack getChestplate() {
        return null;
    }

    @Override
    public ItemStack getLeggings() {
        ItemStack Leggings = new ItemStack(Material.CHAINMAIL_LEGGINGS, 1);
        ItemMeta LeggingsMeta = Leggings.getItemMeta();
        LeggingsMeta.setDisplayName(ChatColor.GRAY+""+ChatColor.BOLD+"Spooky Legs");
        LeggingsMeta.setLore(Lists.newArrayList(ChatColor.AQUA+"Your spooky legs! ooOOooOoOoOooOo"));
        Leggings.setItemMeta(LeggingsMeta);

        return Leggings;
    }

    @Override
    public ItemStack getBoots() {
        return null;
    }

    @Override
    public ArrayList<PotionEffect> getPotionEffects() {

        ArrayList<PotionEffect> potionEffects = new ArrayList<>();

        potionEffects.add(new PotionEffect(PotionEffectType.INVISIBILITY, 1000000, 0));

        return potionEffects;
    }

    @Override
    public int getPlayerMaxHealth() {
        return 6;
    }
}
