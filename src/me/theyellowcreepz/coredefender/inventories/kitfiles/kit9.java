package me.theyellowcreepz.coredefender.inventories.kitfiles;

import com.google.common.collect.Lists;
import me.theyellowcreepz.coredefender.GameState;
import me.theyellowcreepz.coredefender.Main;
import me.theyellowcreepz.coredefender.interfaces.kitInterface;
import me.theyellowcreepz.coredefender.inventories.kitManager;
import me.theyellowcreepz.coredefender.messageStandards;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.util.Vector;

import java.util.ArrayList;

/**
 * All code inside this plugin is the
 * property of Andrew Petersen.
 * Decompiling, copying, or claiming any
 * code from this plugin as your own
 * voids any legal rights you have to
 * this plugin, unless express permission
 * is given.
 */
public class kit9 implements kitInterface, Listener {
    @Override
    public ItemStack getDisplayItem() {
        ItemStack dispItem = new ItemStack(Material.STONE, 1);
        ItemMeta dispItemMeta = dispItem.getItemMeta();
        dispItemMeta.setDisplayName(ChatColor.GRAY+""+ ChatColor.BOLD+"Rhinoceros Thump");
        dispItemMeta.setLore(Lists.newArrayList(ChatColor.AQUA+"Thump nearby enemies! ", ChatColor.RED+""+ChatColor.BOLD+"You only have 7 hearts. ", ChatColor.GRAY+"(Cooldown: 10 Seconds)"));
        dispItem.setItemMeta(dispItemMeta);
        dispItem.addUnsafeEnchantment(Enchantment.DAMAGE_ALL, 10);
        return dispItem;
    }

    @Override
    public String getKitTitle() {
        return (ChatColor.GRAY+""+ ChatColor.BOLD+"Rhinoceros Thump");
    }

    @Override
    public boolean hasListeners() {
        return true;
    }

    @Override
    public int getKitID() {
        return 9;
    }

    @Override
    public ArrayList<ItemStack> getKitItems() {
        ArrayList<ItemStack> kitItems = new ArrayList<>();

        ItemStack rhinoAxe = new ItemStack(Material.STONE_AXE, 5);
        ItemMeta rhinoAxeMeta = rhinoAxe.getItemMeta();
        rhinoAxeMeta.setDisplayName(ChatColor.GRAY+""+ChatColor.BOLD+"Rhino Axe "+ messageStandards.rightClickText());
        rhinoAxeMeta.setLore(Lists.newArrayList(ChatColor.AQUA+"Right-Click to thump anyone within 8 blocks of you!", ChatColor.GRAY+"(Cooldown: 10 Seconds)"));
        rhinoAxe.setItemMeta(rhinoAxeMeta);
        rhinoAxe.addUnsafeEnchantment(Enchantment.DAMAGE_ALL, 1);

        kitItems.add(rhinoAxe);

        return kitItems;
    }

    @Override
    public ItemStack getHelmet() {
        ItemStack Helmet = new ItemStack(Material.STONE, 1);
        ItemMeta HelmetMeta = Helmet.getItemMeta();
        HelmetMeta.setDisplayName(ChatColor.GRAY+""+ ChatColor.BOLD+"Rhino Horns");
        Helmet.setItemMeta(HelmetMeta);
        Helmet.addUnsafeEnchantment(Enchantment.THORNS, 1);
        return Helmet;
    }

    @Override
    public ItemStack getChestplate() {
        ItemStack Chestplate = new ItemStack(Material.IRON_CHESTPLATE, 1);
        ItemMeta ChestplateMeta = Chestplate.getItemMeta();
        ChestplateMeta.setDisplayName(ChatColor.GRAY+""+ChatColor.BOLD+"Rhino Body");
        Chestplate.setItemMeta(ChestplateMeta);
        Chestplate.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 1);
        return Chestplate;
    }

    @Override
    public ItemStack getLeggings() {
        ItemStack Leggings = new ItemStack(Material.IRON_LEGGINGS, 1);
        ItemMeta LeggingsMeta = Leggings.getItemMeta();
        LeggingsMeta.setDisplayName(ChatColor.GRAY+""+ChatColor.BOLD+"Rhino Legs");
        Leggings.setItemMeta(LeggingsMeta);
        return Leggings;
    }

    @Override
    public ItemStack getBoots() {
        ItemStack Boots = new ItemStack(Material.IRON_BOOTS, 1);
        ItemMeta BootsMeta = Boots.getItemMeta();
        BootsMeta.setDisplayName(ChatColor.GRAY+""+ChatColor.BOLD+"Rhino Feet");
        BootsMeta.setLore(Lists.newArrayList(ChatColor.GRAY+"Thump III"));
        Boots.setItemMeta(BootsMeta);
        return Boots;
    }

    @Override
    public ArrayList<PotionEffect> getPotionEffects() {
        return null;
    }

    @Override
    public int getPlayerMaxHealth() {
        return 14;
    }

    @EventHandler
    public void onClick(PlayerInteractEvent evt){
        if(GameState.getState().equals(GameState.INGAME) && kitManager.playerKits.get(evt.getPlayer()) == 9){
            Player pl = evt.getPlayer();
            if(pl.getItemInHand().getType().equals(Material.STONE_AXE)){
                if(evt.getAction().equals(Action.RIGHT_CLICK_AIR) || evt.getAction().equals(Action.RIGHT_CLICK_BLOCK)){
                    if(!(kitManager.kit9Cooldown.containsKey(pl))) {
                        pl.sendMessage(messageStandards.grayArrows() + ChatColor.RESET + ChatColor.GRAY + "You thumped the nearby enemies!");
                        kitManager.kit9Cooldown.put(pl, 10);
                        ArrayList<Player> playersToSendEffects = new ArrayList<>();
                        playersToSendEffects.add(pl);
                        for (Player e : Bukkit.getOnlinePlayers()) {
                            if (e.getLocation().distance(pl.getLocation()) <= 8) {
                                Player plToAffect = (Player) e;
                                String damagerTeam = null;
                                String damagedTeam = null;
                                if (Main.redTeam.contains(pl)) {
                                    damagerTeam = "red";
                                } else if (Main.blueTeam.contains(pl)) {
                                    damagerTeam = "blue";
                                }
                                if (Main.redTeam.contains(plToAffect)) {
                                    damagedTeam = "red";
                                } else if (Main.blueTeam.contains(plToAffect)) {
                                    damagedTeam = "blue";
                                }

                                if (damagedTeam != null && damagerTeam != null) {
                                    if (!damagerTeam.equals(damagedTeam)) {
                                        plToAffect.damage(5);
                                        plToAffect.setVelocity(plToAffect.getVelocity().add(new Vector(0, 0.8, 0)));
                                        playersToSendEffects.add(plToAffect);
                                        plToAffect.sendMessage(messageStandards.alertPrefix() + ChatColor.DARK_GRAY + "You were " + ChatColor.BOLD + "THUMPED" + ChatColor.RESET + ChatColor.DARK_GRAY + "!");
                                    }
                                }
                            }
                        }

                        for (Player p : playersToSendEffects) {
                            p.playSound(pl.getLocation(), Sound.SUCCESSFUL_HIT, 5, 5);
                        }
                    } else if(kitManager.kit9Cooldown.containsKey(pl)){
                        pl.sendMessage(messageStandards.kitCooldownMsg(pl, kitManager.kit9Cooldown));
                }
                }
            }
        }
    }
}
