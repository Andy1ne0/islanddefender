package me.theyellowcreepz.coredefender.inventories.kitfiles;

import com.google.common.collect.Lists;
import me.theyellowcreepz.coredefender.GameState;
import me.theyellowcreepz.coredefender.Main;
import me.theyellowcreepz.coredefender.interfaces.kitInterface;
import me.theyellowcreepz.coredefender.inventories.kitManager;
import me.theyellowcreepz.coredefender.messageStandards;
import org.bukkit.*;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import java.util.ArrayList;

/**
 * All code inside this plugin is the
 * property of Andrew Petersen.
 * Decompiling, copying, or claiming any
 * code from this plugin as your own
 * voids any legal rights you have to
 * this plugin, unless express permission
 * is given.
 */
public class kit10 implements kitInterface, Listener {
    @Override
    public ItemStack getDisplayItem() {
        ItemStack dispItem = new ItemStack(Material.BLAZE_POWDER, 1);
        ItemMeta dispItemMeta = dispItem.getItemMeta();
        dispItemMeta.setDisplayName(ChatColor.RED+""+ChatColor.BOLD+"Blazing Hot");
        dispItemMeta.setLore(Lists.newArrayList(ChatColor.AQUA+"You're a blaze! Shoot those hot balls of fieryness! ", ChatColor.RED+""+ChatColor.BOLD+"You have 10 hearts. ", ChatColor.GRAY+"(Cooldown: 15 Seconds)"));
        dispItem.setItemMeta(dispItemMeta);
        return dispItem;
    }

    @Override
    public String getKitTitle() {
        return (ChatColor.RED+""+ChatColor.BOLD+"Blazing Hot");
    }

    @Override
    public boolean hasListeners() {
        return true;
    }

    @Override
    public int getKitID() {
        return 10;
    }

    @Override
    public ArrayList<ItemStack> getKitItems() {
        ArrayList<ItemStack> kitItems = new ArrayList<>();

        ItemStack blazeSword = new ItemStack(Material.STONE_SWORD, 1);
        ItemMeta blazeSwordMeta = blazeSword.getItemMeta();
        blazeSwordMeta.setDisplayName(ChatColor.RED+""+ChatColor.BOLD+"Flaming Sword");
        blazeSword.setItemMeta(blazeSwordMeta);
        blazeSword.addEnchantment(Enchantment.FIRE_ASPECT, 1);
        kitItems.add(blazeSword);

        ItemStack blazeRod = new ItemStack(Material.BLAZE_ROD, 1);
        ItemMeta blazeRodMeta = blazeRod.getItemMeta();
        blazeRodMeta.setDisplayName(ChatColor.RED+""+ChatColor.BOLD+"Blaze Rod "+ messageStandards.rightClickText());
        blazeRodMeta.setLore(Lists.newArrayList(ChatColor.AQUA+"Shoot fire balls at any enemies within 20 blocks! ", ChatColor.GRAY+"(Cooldown: 15 Seconds)"));
        blazeRod.setItemMeta(blazeRodMeta);
        blazeRod.addUnsafeEnchantment(Enchantment.FIRE_ASPECT, 2);
        kitItems.add(blazeRod);

        return kitItems;
    }

    @Override
    public ItemStack getHelmet() {
        ItemStack Helmet = new ItemStack(Material.LEATHER_HELMET, 1);
        LeatherArmorMeta HelmetMeta = (LeatherArmorMeta) Helmet.getItemMeta();
        HelmetMeta.setColor(Color.ORANGE);
        HelmetMeta.setDisplayName(ChatColor.RED+""+ChatColor.BOLD+"Blaze Head");
        Helmet.setItemMeta(HelmetMeta);
        return Helmet;
    }

    @Override
    public ItemStack getChestplate() {
        ItemStack Chestplate = new ItemStack(Material.LEATHER_CHESTPLATE, 1);
        LeatherArmorMeta ChestplateMeta = (LeatherArmorMeta) Chestplate.getItemMeta();
        ChestplateMeta.setColor(Color.ORANGE);
        ChestplateMeta.setDisplayName(ChatColor.RED+""+ChatColor.BOLD+"Blaze Chest");
        Chestplate.setItemMeta(ChestplateMeta);
        return Chestplate;
    }

    @Override
    public ItemStack getLeggings() {
        ItemStack Leggings = new ItemStack(Material.LEATHER_LEGGINGS, 1);
        LeatherArmorMeta LeggingsMeta = (LeatherArmorMeta) Leggings.getItemMeta();
        LeggingsMeta.setColor(Color.ORANGE);
        LeggingsMeta.setDisplayName(ChatColor.RED+""+ChatColor.BOLD+"Blaze Legs");
        Leggings.setItemMeta(LeggingsMeta);
        return Leggings;
    }

    @Override
    public ItemStack getBoots() {
        ItemStack Boots = new ItemStack(Material.LEATHER_BOOTS, 1);
        LeatherArmorMeta BootsMeta = (LeatherArmorMeta) Boots.getItemMeta();
        BootsMeta.setColor(Color.ORANGE);
        BootsMeta.setDisplayName(ChatColor.RED+""+ChatColor.BOLD+"Blaze Feet");
        Boots.setItemMeta(BootsMeta);
        return Boots;
    }

    @Override
    public ArrayList<PotionEffect> getPotionEffects() {
        ArrayList<PotionEffect> potionEffects = new ArrayList<>();

        potionEffects.add(new PotionEffect(PotionEffectType.FIRE_RESISTANCE, 100000, 1));

        return potionEffects;
    }

    @Override
    public int getPlayerMaxHealth() {
        return 0;
    }

    @EventHandler
    public void onClick(PlayerInteractEvent evt){
        if(GameState.getState().equals(GameState.INGAME)){
            if(kitManager.playerKits.get(evt.getPlayer()) == 10){
                Player pl = evt.getPlayer();
                    if(evt.getItem() != null && evt.getItem().getType().equals(Material.BLAZE_ROD)){
                        if(evt.getAction().equals(Action.RIGHT_CLICK_AIR) || evt.getAction().equals(Action.RIGHT_CLICK_BLOCK)){
                            if(!(kitManager.kit10Cooldown.containsKey(pl))){
                                ArrayList<Entity> entitiesToAttackPrim = new ArrayList<>();
                                ArrayList<Entity> entitiesToAttack = new ArrayList<>();
                                for(Entity e : Main.mainWorld.getEntities()){
                                    if(e instanceof LivingEntity) {
                                        if (e.getLocation().distance(pl.getLocation()) <= 20) {
                                            entitiesToAttackPrim.add(e);
                                        }
                                    }
                                }

                                if(entitiesToAttackPrim != null) {
                                    ArrayList<Entity> entsToRemove = new ArrayList<>();

                                    entitiesToAttack = entitiesToAttackPrim;

                                    for (Entity e : entitiesToAttackPrim) {
                                        if (e instanceof Player) {

                                            if (e == pl) {
                                                entsToRemove.add(e);
                                                continue;
                                            }

                                            Player target = (Player) e;
                                            String plTeam = null;
                                            String targetTeam = null;

                                            if (Main.redTeam.contains(pl)) {
                                                plTeam = "red";
                                            } else if (Main.blueTeam.contains(pl)) {
                                                plTeam = "blue";
                                            } else {
                                                entsToRemove.add(e);
                                                continue;
                                            }

                                            if (Main.redTeam.contains(target)) {
                                                targetTeam = "red";
                                            } else if (Main.blueTeam.contains(target)) {
                                                targetTeam = "blue";
                                            } else {
                                                entsToRemove.add(e);
                                                continue;
                                            }

                                            if (plTeam == targetTeam) {
                                                entsToRemove.add(e);
                                                continue;
                                            }
                                        } else {
                                            if (!(e instanceof Monster)) {
                                                entsToRemove.add(e);
                                            }
                                        }
                                    }
                                    entitiesToAttack.removeAll(entsToRemove);

                                }

                                if(entitiesToAttack != null){
                                    int ticksToDelay = 1;
                                    for(Entity e : entitiesToAttack){
                                        final int finalTicksToDelay = ticksToDelay;

                                            new BukkitRunnable() {
                                                @Override
                                                public void run() {
                                                    Vector plLoc = new Vector(pl.getLocation().getX(), pl.getLocation().getY(), pl.getLocation().getZ());
                                                    Vector eLoc = new Vector(e.getLocation().getX(), e.getLocation().getY(), e.getLocation().getZ());
                                                    Vector dir = plLoc.subtract(eLoc).multiply(-0.1);

                                                    Location entSpLoc = pl.getLocation();
                                                    Fireball fb = pl.launchProjectile(Fireball.class);
                                                    fb.setShooter(pl);
                                                    fb.setVelocity(dir);
                                                    final double[] vectorMultiplier = {-0.05};
                                                    fb.setCustomName("kit10");
                                                    new BukkitRunnable() {
                                                        @Override
                                                        public void run() {
                                                            if(e.isDead()){
                                                                this.cancel();
                                                            }

                                                            if(!Bukkit.getOnlinePlayers().contains(pl)){
                                                                e.remove();
                                                                this.cancel();
                                                            }

                                                            if(e.getLocation().distance(fb.getLocation()) <= 1){
                                                                fb.teleport(e.getLocation());
                                                                return;
                                                            }
                                                            Vector origLoc = new Vector(fb.getLocation().getX(), fb.getLocation().getY(), fb.getLocation().getZ());
                                                            Vector newLoc = new Vector(e.getLocation().getX(), e.getLocation().getY(), e.getLocation().getZ());

                                                            Vector dir = origLoc.subtract(newLoc).multiply(vectorMultiplier[0]);
                                                            /*
                                                            if(vectorMultiplier[0] == -0.05){
                                                                vectorMultiplier[0] = 0.05;
                                                            } else if(vectorMultiplier[0] == 0.05){
                                                                vectorMultiplier[0] = -0.05;
                                                            }
                                                            */
                                                            // fb.setVelocity(dir);
                                                            fb.setDirection(dir);

                                                        }
                                                    }.runTaskTimer(Main.getInstance(), 3l, 3l);
                                                }
                                            }.runTaskLater(Main.getInstance(), finalTicksToDelay);
                                        ticksToDelay = ticksToDelay + 2;
                                        }
                                    }
                                    pl.sendMessage(messageStandards.pluginPlayerInputPrefix()+ChatColor.RESET+ChatColor.GRAY+"You shot flaming fireballs!");
                                    kitManager.kit10Cooldown.put(pl, 15);
                                } else if(kitManager.kit10Cooldown.containsKey(pl)){
                                    pl.sendMessage(messageStandards.kitCooldownMsg(pl, kitManager.kit10Cooldown));
                                }
                            }
                        }
                    }
                }
            }


    @EventHandler
    public void onExplode(EntityExplodeEvent evt){
        if(evt.getEntity() != null && evt.getEntity().getCustomName() != null && evt.getEntity().getCustomName().equals("kit10")){
            evt.setCancelled(true);
        }
    }

    @EventHandler
    public void onDamage(EntityDamageByEntityEvent evt){
        if(evt.getDamager() instanceof Fireball){
            Fireball fb = (Fireball) evt.getDamager();
            if(fb.getCustomName() != null && fb.getCustomName().equals("kit10")){
                if(evt.getEntity() instanceof Player){
                    Player pl = (Player) fb.getShooter();
                    Player target = (Player) evt.getEntity();
                    String plTeam = null;
                    String targetTeam = null;
                    if (Main.redTeam.contains(pl)) {
                        plTeam = "red";
                    } else if (Main.blueTeam.contains(pl)) {
                        plTeam = "blue";
                    } else {
                        return;
                    }

                    if (Main.redTeam.contains(target)) {
                        targetTeam = "red";
                    } else if (Main.blueTeam.contains(target)) {
                        targetTeam = "blue";
                    } else {
                        return;
                    }

                    if(plTeam.equals(targetTeam)){
                        evt.setCancelled(true);
                    } else {
                        Main.mainWorld.createExplosion(target.getLocation(), 0f);
                        pl.setVelocity(new Vector(0, 0.8, 0));
                        pl.setFireTicks(80);
                        return;
                    }
                } else if(evt.getEntity() instanceof Monster){
                    Main.mainWorld.createExplosion(evt.getEntity().getLocation(), 0f);
                    evt.getEntity().setVelocity(new Vector(0, 0.1, 0));
                    evt.getEntity().setFireTicks(80);
                    return;
                }
            }
        } else {
            return;
        }
    }
}
