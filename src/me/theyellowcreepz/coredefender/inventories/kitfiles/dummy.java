package me.theyellowcreepz.coredefender.inventories.kitfiles;

import me.theyellowcreepz.coredefender.interfaces.kitInterface;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;

import java.util.ArrayList;

/**
 * All code inside this plugin is the
 * property of Andrew Petersen.
 * Decompiling, copying, or claiming any
 * code from this plugin as your own
 * voids any legal rights you have to
 * this plugin, unless express permission
 * is given.
 */
public class dummy implements kitInterface {
    @Override
    public ItemStack getDisplayItem() {
        return new ItemStack(Material.OBSIDIAN, 5);
    }

    @Override
    public String getKitTitle() {
        return "dummy kit";
    }

    @Override
    public boolean hasListeners() {
        return false;
    }

    @Override
    public int getKitID() {
        return 699;
    }

    @Override
    public ArrayList<ItemStack> getKitItems() {
        return null;
    }

    @Override
    public ItemStack getHelmet() {
        return null;
    }

    @Override
    public ItemStack getChestplate() {
        return null;
    }

    @Override
    public ItemStack getLeggings() {
        return null;
    }

    @Override
    public ItemStack getBoots() {
        return null;
    }

    @Override
    public ArrayList<PotionEffect> getPotionEffects() {
        return null;
    }

    @Override
    public int getPlayerMaxHealth() {
        return 0;
    }
}
