package me.theyellowcreepz.coredefender.inventories.kitfiles;

import com.google.common.collect.Lists;
import me.theyellowcreepz.coredefender.GameState;
import me.theyellowcreepz.coredefender.Main;
import me.theyellowcreepz.coredefender.interfaces.kitInterface;
import me.theyellowcreepz.coredefender.inventories.kitManager;
import me.theyellowcreepz.coredefender.messageStandards;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.entity.TNTPrimed;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;

import java.util.ArrayList;

/**
 * All code inside this plugin is the
 * property of Andrew Petersen.
 * Decompiling, copying, or claiming any
 * code from this plugin as your own
 * voids any legal rights you have to
 * this plugin, unless express permission
 * is given.
 */
public class kit3 implements kitInterface, Listener {
    @Override
    public ItemStack getDisplayItem() {

        ItemStack kit3 = new ItemStack(org.bukkit.Material.DIAMOND_HOE, 1);
        ItemMeta kit3meta = kit3.getItemMeta();
        kit3meta.setDisplayName(ChatColor.DARK_PURPLE + "" + ChatColor.BOLD + "Diamond Bomber");
        kit3meta.setLore(Lists.newArrayList(ChatColor.AQUA + "Shoot diamond-filled explosives at other players!", ChatColor.RED+""+ChatColor.BOLD+"You have 2 extra hearts.", (ChatColor.GRAY+"(Cooldown: 8 Seconds)")));
        kit3.setItemMeta(kit3meta);

        return kit3;
    }

    @Override
    public String getKitTitle() {
        return (ChatColor.DARK_PURPLE + "" + ChatColor.BOLD + "Diamond Bomber");
    }

    @Override
    public boolean hasListeners() {
        return true;
    }

    @Override
    public int getKitID() {
        return 3;
    }

    @Override
    public ArrayList<ItemStack> getKitItems() {

        ArrayList<ItemStack> kitItems = new ArrayList<>();

        ItemStack DiamondGun = new ItemStack(Material.DIAMOND_HOE, 1);
        ItemMeta DiamondGunMeta = DiamondGun.getItemMeta();
        DiamondGunMeta.setDisplayName(ChatColor.AQUA+"Diamond-Encrusted Gun "+ messageStandards.rightClickText());
        DiamondGunMeta.setLore(Lists.newArrayList(ChatColor.AQUA+""+ChatColor.ITALIC+"Right-click to shoot TNT with diamond fragments inside!"));
        DiamondGun.setItemMeta(DiamondGunMeta);

        kitItems.add(DiamondGun);

        ItemStack RichSword = new ItemStack(Material.STONE_SWORD, 1);
        ItemMeta RichSwordMeta = RichSword.getItemMeta();
        RichSwordMeta.setDisplayName(ChatColor.AQUA+"Expensive Sword");
        RichSword.setItemMeta(RichSwordMeta);

        kitItems.add(RichSword);

        return kitItems;
    }

    @Override
    public ItemStack getHelmet() {

        ItemStack Helmet = new ItemStack(Material.LEATHER_HELMET, 1);
        ItemMeta HelmetMeta = Helmet.getItemMeta();
        HelmetMeta.setDisplayName(ChatColor.AQUA+"The Rich Hat");
        Helmet.setItemMeta(HelmetMeta);
        Helmet.addEnchantment(Enchantment.OXYGEN, 1);

        return Helmet;
    }

    @Override
    public ItemStack getChestplate() {

        ItemStack Chestplate = new ItemStack(Material.LEATHER_CHESTPLATE, 1);
        ItemMeta ChestplateMeta = Chestplate.getItemMeta();
        ChestplateMeta.setDisplayName(ChatColor.AQUA+"Expensive Jacket");
        Chestplate.setItemMeta(ChestplateMeta);
        Chestplate.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 3);

        return Chestplate;
    }

    @Override
    public ItemStack getLeggings() {

        ItemStack Leggings = new ItemStack(Material.LEATHER_LEGGINGS, 1);
        ItemMeta LeggingsMeta = Leggings.getItemMeta();
        LeggingsMeta.setDisplayName(ChatColor.AQUA+"Expensive Jeans");
        Leggings.setItemMeta(LeggingsMeta);

        return Leggings;
    }

    @Override
    public ItemStack getBoots() {

        ItemStack Boots = new ItemStack(Material.LEATHER_BOOTS, 1);
        ItemMeta BootsMeta = Boots.getItemMeta();
        BootsMeta.setDisplayName(ChatColor.AQUA+"Air Jordans");
        Boots.setItemMeta(BootsMeta);

        return Boots;
    }

    @Override
    public ArrayList<PotionEffect> getPotionEffects() {
        return null;
    }

    @Override
    public int getPlayerMaxHealth() {
        return 24;
    }

    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent evt){

        if (GameState.getState() == GameState.INGAME){
            Player pl = evt.getPlayer();
            if(evt.getItem() != null && kitManager.playerKits.get(pl) == 3 && (evt.getAction() == Action.RIGHT_CLICK_BLOCK || evt.getAction() == Action.RIGHT_CLICK_AIR)) {
                if (pl.getItemInHand().getType() == Material.DIAMOND_HOE) {
                    if (!(kitManager.kit2Cooldown.containsKey(pl))) {
                        Entity TNT = pl.getWorld().spawn(pl.getLocation(), TNTPrimed.class);
                        ((TNTPrimed) TNT).setFuseTicks(40);
                        TNT.setCustomName("kit2tnt");
                        TNT.setVelocity(pl.getLocation().getDirection().multiply(2.0D));
                        pl.sendMessage(messageStandards.grayArrows() + "You've shot diamond-filled TNT.");
                        kitManager.kit2Cooldown.put(pl, 8);
                    } else if (kitManager.kit2Cooldown.containsKey(pl)) {
                        pl.sendMessage(messageStandards.kitCooldownMsg(pl, kitManager.kit2Cooldown));
                    }
                }
            }
        }
    }

    @EventHandler
    public void onExplode(EntityExplodeEvent evt){
        if(evt.getEntity() != null && evt.getEntity() instanceof TNTPrimed){
            TNTPrimed Explosive = (TNTPrimed) evt.getEntity();
            if(evt.getEntity().getCustomName()!=null && evt.getEntity().getCustomName().equalsIgnoreCase("kit2tnt")){
                Location loc = Explosive.getLocation();
                evt.setCancelled(true);
                Main.mainWorld.createExplosion(loc, 5f);
                ArrayList<Location> blocksForDMD = new ArrayList<Location>();
                blocksForDMD.add(Explosive.getLocation().add(0, 3, 0));
                blocksForDMD.add(Explosive.getLocation().add(1, 3, 0));
                blocksForDMD.add(Explosive.getLocation().add(1, 3, 1));
                blocksForDMD.add(Explosive.getLocation().add(1, 3, -1));
                blocksForDMD.add(Explosive.getLocation().add(-1, 3, 0));
                blocksForDMD.add(Explosive.getLocation().add(-1, 3, 1));
                blocksForDMD.add(Explosive.getLocation().add(-1, 3, -1));
                blocksForDMD.add(Explosive.getLocation().add(0, 3, 1));
                blocksForDMD.add(Explosive.getLocation().add(0, 3, -1));

                for(Location locBlockdmd : blocksForDMD){

                    ItemStack diamond = new ItemStack(Material.DIAMOND);
                    ItemMeta dmeta = diamond.getItemMeta();
                    dmeta.setDisplayName("kit2d");
                    diamond.setItemMeta(dmeta);
                    Main.mainWorld.dropItemNaturally(locBlockdmd, diamond);

                }
                blocksForDMD.clear();

            }

        }
    }
}
