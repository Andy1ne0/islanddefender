package me.theyellowcreepz.coredefender.inventories.kitfiles;

import com.google.common.collect.Lists;
import me.theyellowcreepz.coredefender.interfaces.kitInterface;
import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.ArrayList;

/**
 * All code inside this plugin is the
 * property of Andrew Petersen.
 * Decompiling, copying, or claiming any
 * code from this plugin as your own
 * voids any legal rights you have to
 * this plugin, unless express permission
 * is given.
 */
public class kit7 implements kitInterface {
    @Override
    public ItemStack getDisplayItem() {
        ItemStack kit7 = new ItemStack(Material.LEATHER_BOOTS, 1);
        ItemMeta kit7meta = kit7.getItemMeta();
        kit7meta.setDisplayName(ChatColor.DARK_PURPLE+""+ChatColor.BOLD+"Aussie Kangaroo");
        kit7meta.setLore(Lists.newArrayList(ChatColor.AQUA+"Jump like a kangaroo! ", ChatColor.RED+""+ChatColor.BOLD+"You only have 5 hearts. "));
        kit7.setItemMeta(kit7meta);
        return kit7;
    }

    @Override
    public String getKitTitle() {
        return (ChatColor.DARK_PURPLE+""+ChatColor.BOLD+"Aussie Kangaroo");
    }

    @Override
    public boolean hasListeners() {
        return false;
    }

    @Override
    public int getKitID() {
        return 7;
    }

    @Override
    public ArrayList<ItemStack> getKitItems() {

        ArrayList<ItemStack> kitItems = new ArrayList<>();

        ItemStack kangarooBoomerang = new ItemStack(Material.WOOD_HOE, 1);
        ItemMeta kangarooBoomerangMeta = kangarooBoomerang.getItemMeta();
        kangarooBoomerangMeta.setDisplayName(ChatColor.DARK_PURPLE+"Kangaroo Boomerang");
        kangarooBoomerang.setItemMeta(kangarooBoomerangMeta);
        kangarooBoomerang.addUnsafeEnchantment(Enchantment.DAMAGE_ALL, 4);
        kangarooBoomerang.addUnsafeEnchantment(Enchantment.KNOCKBACK, 2);

        kitItems.add(kangarooBoomerang);

        return kitItems;
    }

    @Override
    public ItemStack getHelmet() {

        ItemStack Helmet = new ItemStack(Material.LEATHER_HELMET, 1);
        LeatherArmorMeta HelmetMeta = (LeatherArmorMeta) Helmet.getItemMeta();
        HelmetMeta.setColor(Color.MAROON);
        HelmetMeta.setDisplayName(ChatColor.DARK_PURPLE+"Kangaroo Head");
        Helmet.setItemMeta(HelmetMeta);
        Helmet.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 1);

        return Helmet;
    }

    @Override
    public ItemStack getChestplate() {

        ItemStack Chestplate = new ItemStack(Material.LEATHER_CHESTPLATE, 1);
        LeatherArmorMeta ChestplateMeta = (LeatherArmorMeta) Chestplate.getItemMeta();
        ChestplateMeta.setColor(Color.MAROON);
        ChestplateMeta.setDisplayName(ChatColor.DARK_PURPLE+"Kangaroo Chest Fur");
        Chestplate.setItemMeta(ChestplateMeta);
        Chestplate.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 1);

        return Chestplate;
    }

    @Override
    public ItemStack getLeggings() {

        ItemStack Leggings = new ItemStack(Material.LEATHER_LEGGINGS, 1);
        LeatherArmorMeta LeggingsMeta = (LeatherArmorMeta) Leggings.getItemMeta();
        LeggingsMeta.setColor(Color.MAROON);
        LeggingsMeta.setDisplayName(ChatColor.DARK_PURPLE+"Kangaroo Legs");
        Leggings.setItemMeta(LeggingsMeta);
        Leggings.addUnsafeEnchantment(Enchantment.PROTECTION_EXPLOSIONS, 3);

        return Leggings;
    }

    @Override
    public ItemStack getBoots() {

        ItemStack Boots = new ItemStack(Material.LEATHER_BOOTS, 1);
        LeatherArmorMeta BootsMeta = (LeatherArmorMeta) Boots.getItemMeta();
        // BootsMeta.setColor(Color.MAROON);
        BootsMeta.setDisplayName(ChatColor.DARK_PURPLE + "Kangaroo Feet");
        BootsMeta.setLore(Lists.newArrayList(ChatColor.GRAY+"Jumpy Feet II"));
        Boots.setItemMeta(BootsMeta);
        Boots.addUnsafeEnchantment(Enchantment.PROTECTION_FALL, 3);

        return Boots;
    }

    @Override
    public ArrayList<PotionEffect> getPotionEffects() {

        ArrayList<PotionEffect> potionEffects = new ArrayList<>();

        potionEffects.add(new PotionEffect(PotionEffectType.JUMP, 1000000, 3));
        potionEffects.add(new PotionEffect(PotionEffectType.SPEED, 1000000, 1));

        return potionEffects;
    }

    @Override
    public int getPlayerMaxHealth() {
        return 10;
    }
}
