package me.theyellowcreepz.coredefender.inventories.kitfiles;

import com.google.common.collect.Lists;
import me.theyellowcreepz.coredefender.GameState;
import me.theyellowcreepz.coredefender.Main;
import me.theyellowcreepz.coredefender.interfaces.kitInterface;
import me.theyellowcreepz.coredefender.inventories.kitManager;
import me.theyellowcreepz.coredefender.messageStandards;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.entity.Snowball;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.ArrayList;

/**
 * All code inside this plugin is the
 * property of Andrew Petersen.
 * Decompiling, copying, or claiming any
 * code from this plugin as your own
 * voids any legal rights you have to
 * this plugin, unless express permission
 * is given.
 */
public class kit2 implements kitInterface, Listener {
    @Override
    public ItemStack getDisplayItem() {
        ItemStack kit2 = new ItemStack(org.bukkit.Material.SNOW_BALL, 1);
        ItemMeta kit2meta = kit2.getItemMeta();
        kit2meta.setDisplayName(ChatColor.AQUA + "" + ChatColor.BOLD + "Queen Elsa");
        kit2meta.setLore(Lists.newArrayList(ChatColor.AQUA + "Freeze your opponents in place for 5 seconds!", ChatColor.RED+""+ChatColor.BOLD+"You only have 8 hearts. ", ChatColor.GRAY+"(Cooldown: 15 Seconds)"));
        kit2.setItemMeta(kit2meta);
        return kit2;
    }

    @Override
    public String getKitTitle() {
        return (ChatColor.AQUA + "" + ChatColor.BOLD + "Queen Elsa");
    }

    @Override
    public boolean hasListeners() {
        return true;
    }

    @Override
    public int getKitID() {
        return 2;
    }

    @Override
    public ArrayList<ItemStack> getKitItems() {

        ArrayList<ItemStack> kitItems = new ArrayList<ItemStack>();

        ItemStack ElsaHoe = new ItemStack(Material.IRON_HOE, 1);
        ItemMeta ElsaHoeMeta = ElsaHoe.getItemMeta();
        ElsaHoeMeta.setDisplayName(ChatColor.DARK_GRAY+""+ChatColor.BOLD+"Elsa's Wand "+ messageStandards.rightClickText());
        ElsaHoeMeta.setLore(Lists.newArrayList(ChatColor.AQUA+""+ChatColor.ITALIC+"Right-click to freeze your target!"));
        ElsaHoe.setItemMeta(ElsaHoeMeta);

        kitItems.add(ElsaHoe);

        ItemStack ElsaSword = new ItemStack(Material.WOOD_SWORD, 1);
        ItemMeta ElsaSwordMeta = ElsaSword.getItemMeta();
        ElsaSwordMeta.setDisplayName(ChatColor.DARK_GRAY+"Elsa's Faulty Sword");
        ElsaSword.setItemMeta(ElsaSwordMeta);
        ElsaSword.addUnsafeEnchantment(Enchantment.DAMAGE_ALL, -1);

        kitItems.add(ElsaSword);

        return kitItems;
    }

    @Override
    public ItemStack getHelmet() {

        ItemStack Helmet = new ItemStack(Material.IRON_HELMET, 1);
        ItemMeta HelmetMeta = Helmet.getItemMeta();
        HelmetMeta.setDisplayName(ChatColor.DARK_GRAY+"Elsa's Helmet");
        Helmet.setItemMeta(HelmetMeta);
        Helmet.addEnchantment(Enchantment.OXYGEN, 2);

        return Helmet;
    }

    @Override
    public ItemStack getChestplate() {

        ItemStack Chestplate = new ItemStack(Material.IRON_CHESTPLATE, 1);
        ItemMeta ChestplateMeta = Chestplate.getItemMeta();
        ChestplateMeta.setDisplayName(ChatColor.DARK_GRAY+"Elsa's Shirt");
        Chestplate.setItemMeta(ChestplateMeta);

        return Chestplate;
    }

    @Override
    public ItemStack getLeggings() {

        ItemStack Leggings = new ItemStack(Material.IRON_LEGGINGS, 1);
        ItemMeta LeggingsMeta = Leggings.getItemMeta();
        LeggingsMeta.setDisplayName(ChatColor.DARK_GRAY+"Elsa's Pants");
        Leggings.setItemMeta(LeggingsMeta);

        return Leggings;
    }

    @Override
    public ItemStack getBoots() {

        ItemStack Boots = new ItemStack(Material.IRON_BOOTS, 1);
        ItemMeta BootsMeta = Boots.getItemMeta();
        BootsMeta.setDisplayName(ChatColor.DARK_GRAY+"Elsa's Boots");
        Boots.setItemMeta(BootsMeta);
        Boots.addEnchantment(Enchantment.DEPTH_STRIDER, 2);

        return Boots;
    }

    @Override
    public ArrayList<PotionEffect> getPotionEffects() {
        return null;
    }

    @Override
    public int getPlayerMaxHealth() {
        return 16;
    }

    @EventHandler
    public void onInteract(PlayerInteractEvent evt){
        if(evt.getPlayer() != null && GameState.getState() == GameState.INGAME && !(Main.spectatorTeam.contains(evt.getPlayer())) && kitManager.playerKits.get(evt.getPlayer()) == 2
                && evt.getItem() != null &&(evt.getAction() == Action.RIGHT_CLICK_BLOCK || evt.getAction() == Action.RIGHT_CLICK_AIR)
                &&  evt.getPlayer().getItemInHand().getType() == Material.IRON_HOE){
            if(!(kitManager.kit1Cooldown.containsKey(evt.getPlayer()))){
                Player pl = evt.getPlayer();
                Snowball snowball = pl.launchProjectile(Snowball.class);
                snowball.setVelocity(pl.getLocation().getDirection().multiply(6.0D));
                snowball.setCustomName("elsa");
                pl.sendMessage(messageStandards.grayArrows()+ ChatColor.AQUA+"You shot a snowball!");
                Bukkit.getServer().getLogger().info("> Player "+pl.getName()+" shot a snowball at location ("+pl.getLocation()+"). ");
                kitManager.kit1Cooldown.put(pl, 15);
            } else if(kitManager.kit1Cooldown.containsKey(evt.getPlayer())){
                evt.getPlayer().sendMessage(messageStandards.kitCooldownMsg(evt.getPlayer(), kitManager.kit1Cooldown));
            }
        }
    }

    @EventHandler
    public void onSnowballHit(EntityDamageByEntityEvent evt){
        if(GameState.getState() == GameState.INGAME){
            if(evt.getEntity() instanceof Player){
                Player pl = (Player) evt.getEntity();
                if(evt.getCause() == EntityDamageEvent.DamageCause.PROJECTILE){
                    if(evt.getDamager() instanceof Snowball){
                        Snowball sb = (Snowball) evt.getDamager();
                        if(sb.getCustomName() != null && sb.getCustomName().equalsIgnoreCase("elsa")){
                            PotionEffect freeze = new PotionEffect(PotionEffectType.SLOW, 200, 5);
                            pl.damage(2);
                            if (kitManager.kit1Freeze.containsKey(pl)){
                                kitManager.kit1Freeze.replace(pl, 5);
                                pl.addPotionEffect(freeze);
                                pl.sendMessage(messageStandards.grayArrows()+ChatColor.RESET+""+ChatColor.DARK_GRAY+ChatColor.ITALIC+"You were frozen for 5 seconds!");
                            } else {
                                kitManager.kit1Freeze.put(pl, 5);
                                pl.sendMessage(messageStandards.grayArrows()+ChatColor.RESET+""+ChatColor.DARK_GRAY+ChatColor.ITALIC+"You were frozen for 5 seconds!");
                            }
                        }
                    }
                }
            }
        }
    }
    @SuppressWarnings("deprecation")
    @EventHandler
    public void onPlayerMove(PlayerMoveEvent evt){
        if(kitManager.kit1Freeze.containsKey(evt.getPlayer())){
            if(evt.getPlayer().isOnGround()) {
                evt.setCancelled(true);}
        }
    }
}
