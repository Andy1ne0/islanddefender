package me.theyellowcreepz.coredefender.inventories.kitfiles;

import com.google.common.collect.Lists;
import me.theyellowcreepz.coredefender.GameState;
import me.theyellowcreepz.coredefender.Main;
import me.theyellowcreepz.coredefender.interfaces.kitInterface;
import me.theyellowcreepz.coredefender.inventories.kitManager;
import me.theyellowcreepz.coredefender.messageStandards;
import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.TNTPrimed;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.potion.PotionEffect;

import java.util.ArrayList;

/**
 * All code inside this plugin is the
 * property of Andrew Petersen.
 * Decompiling, copying, or claiming any
 * code from this plugin as your own
 * voids any legal rights you have to
 * this plugin, unless express permission
 * is given.
 */
public class kit4 implements kitInterface, Listener {
    @Override
    public ItemStack getDisplayItem() {

        ItemStack kit4 = new ItemStack(org.bukkit.Material.TNT, 1);
        ItemMeta kit4meta = kit4.getItemMeta();
        kit4meta.setDisplayName(ChatColor.DARK_RED + "" + ChatColor.BOLD + "The Nuker");
        kit4meta.setLore(Lists.newArrayList(ChatColor.AQUA + "Drop bombs on your opponents!", ChatColor.RED+""+ChatColor.BOLD+"You only have 9 hearts. ",ChatColor.GRAY+"(Cooldown: 15 Seconds)"));
        kit4.setItemMeta(kit4meta);

        return kit4;
    }

    @Override
    public String getKitTitle() {
        return (ChatColor.DARK_RED + "" + ChatColor.BOLD + "The Nuker");
    }

    @Override
    public boolean hasListeners() {
        return true;
    }

    @Override
    public int getKitID() {
        return 4;
    }

    @Override
    public ArrayList<ItemStack> getKitItems() {

        ArrayList<ItemStack> kitItems = new ArrayList<>();

        ItemStack BombDropper = new ItemStack(Material.IRON_AXE, 1);
        ItemMeta BombDropperMeta = BombDropper.getItemMeta();
        BombDropperMeta.setDisplayName(ChatColor.DARK_RED+"Bomber Tool "+ messageStandards.rightClickText());
        BombDropperMeta.setLore(Lists.newArrayList(ChatColor.AQUA+""+ChatColor.ITALIC+"Right-click to shoot airborne bombs at your opponents!"));
        BombDropper.setItemMeta(BombDropperMeta);
        BombDropper.addUnsafeEnchantment(Enchantment.DAMAGE_ALL, 1);

        kitItems.add(BombDropper);

        return kitItems;
    }

    @Override
    public ItemStack getHelmet() {
        ItemStack Helmet = new ItemStack(Material.TNT, 1);
        ItemMeta HelmetMeta = Helmet.getItemMeta();
        HelmetMeta.setDisplayName(ChatColor.DARK_RED+"Explosive Hat");
        Helmet.setItemMeta(HelmetMeta);
        Helmet.addUnsafeEnchantment(Enchantment.PROTECTION_EXPLOSIONS, 3);

        return Helmet;
    }

    @Override
    public ItemStack getChestplate() {

        ItemStack Chestplate = new ItemStack(Material.LEATHER_CHESTPLATE, 1);
        LeatherArmorMeta ChestplateMeta = (LeatherArmorMeta) Chestplate.getItemMeta();
        ChestplateMeta.setDisplayName(ChatColor.DARK_RED+"Explosive T-shirt");
        ChestplateMeta.setColor(Color.RED);
        Chestplate.setItemMeta(ChestplateMeta);

        return Chestplate;
    }

    @Override
    public ItemStack getLeggings() {

        ItemStack Leggings = new ItemStack(Material.LEATHER_LEGGINGS, 1);
        LeatherArmorMeta LeggingsMeta = (LeatherArmorMeta) Leggings.getItemMeta();
        LeggingsMeta.setColor(Color.RED);
        LeggingsMeta.setDisplayName(ChatColor.DARK_RED+"Explosive Leggings");
        Leggings.setItemMeta(LeggingsMeta);

        return Leggings;
    }

    @Override
    public ItemStack getBoots() {

        ItemStack Boots = new ItemStack(Material.LEATHER_BOOTS, 1);
        LeatherArmorMeta BootsMeta = (LeatherArmorMeta) Boots.getItemMeta();
        BootsMeta.setDisplayName(ChatColor.DARK_RED+"Explosive Boots");
        BootsMeta.setColor(Color.RED);
        Boots.setItemMeta(BootsMeta);
        Boots.addUnsafeEnchantment(Enchantment.DEPTH_STRIDER, 2);
        Boots.addUnsafeEnchantment(Enchantment.PROTECTION_EXPLOSIONS, 5);

        return Boots;
    }

    @Override
    public ArrayList<PotionEffect> getPotionEffects() {
        return null;
    }

    @Override
    public int getPlayerMaxHealth() {
        return 18;
    }

    @EventHandler
    public void onInteractEvent(PlayerInteractEvent evt){
        Player pl = evt.getPlayer();
        if(GameState.getState() == GameState.INGAME && evt.getItem() != null){
            if (kitManager.playerKits.get(pl) == 4) {
                if (evt.getAction() == Action.RIGHT_CLICK_BLOCK
                        || evt.getAction() == Action.RIGHT_CLICK_AIR) {
                    if (pl.getItemInHand().getType() == Material.IRON_AXE) {
                        if (!(kitManager.kit3Cooldown.containsKey(pl))) {

                            Entity PrimedTNT = Main.mainWorld.spawnEntity
                                    (pl.getLocation(), EntityType.PRIMED_TNT);
                            ((TNTPrimed) PrimedTNT).setFuseTicks(20);
                            PrimedTNT.setVelocity(pl.getLocation().getDirection().multiply(2.0D));
                            pl.sendMessage(messageStandards.grayArrows() + "You've shot TNT!");
                            kitManager.kit3Cooldown.put(pl, 15);
                        } else if(kitManager.kit3Cooldown.containsKey(pl)){
                            pl.sendMessage(messageStandards.kitCooldownMsg(pl, kitManager.kit3Cooldown));
                        }
                    }
                }
            }
        }
    }
}
