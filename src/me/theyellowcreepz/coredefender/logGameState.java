package me.theyellowcreepz.coredefender;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;

/**
 * All code inside this plugin is the
 * property of Andrew Petersen.
 * Decompiling, copying, or claiming any
 * code from this plugin as your own
 * voids any legal rights you have to
 * this plugin, unless express permission
 * is given.
 */
public class logGameState {

    public static void logState(){
        Bukkit.getServer().getLogger().info(" ");
        Bukkit.getServer().getConsoleSender().sendMessage(ChatColor.GOLD+"[ Core Defender ] GameState set to: "+ GameState.getState());
        Bukkit.getServer().getLogger().info(" ");
    }

}
