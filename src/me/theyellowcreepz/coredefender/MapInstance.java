package me.theyellowcreepz.coredefender;

import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.WorldCreator;
import org.bukkit.configuration.ConfigurationSection;

/**
 * All code inside this plugin is the
 * property of Andrew Petersen.
 * Decompiling, copying, or claiming any
 * code from this plugin as your own
 * voids any legal rights you have to
 * this plugin, unless express permission
 * is given.
 */
public class MapInstance {

    private String WorldFolderName = null;
    private World world = null;
    private String publicTitle = null;
    private ConfigurationSection config = null;

    public MapInstance(String worldFolderName, String publicWorldName, ConfigurationSection configSection){
        this.WorldFolderName = worldFolderName;
        this.publicTitle = publicWorldName;
        this.config = configSection;
    }

    public World getWorld() {
        world = Bukkit.createWorld(new WorldCreator(WorldFolderName));
        return world;
    }

    public String getPublicTitle(){
        return publicTitle;
    }

    public ConfigurationSection getConfig(){
        return config;
    }
}
